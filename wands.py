from itemCommon import *

#Buster Wand, Dark Star
# dmg = (min dmg, max dmg, num hits)

low = 4
mid = 4
high = 4

allWands =  [
#------------------------------------------------------------- 0 ---------------------------------------------------------------
{
	'name' : 'Lad\'s Wand',
	'dmg' : (0, 2, 1),
	'crit' : 0,
	'price' : 400,
	'level' : 0,
	'descr' : 'Not much to look at, but I can offer this secondhand wand at a steep discount.',
	'mods' : {
	}
},
#------------------------------------------------------------- 1 ---------------------------------------------------------------
{
	'name' : 'The Needler',
	'dmg' : (0, 1, 5),
	'crit' : 2,
	'price' : low,
	'level' : 1,
	'descr' : 'An unconventional wand that relies on multi-hits to do damage.',
	'mods' : {
	}
},
{
	'name' : 'Ol\' Reliable',
	'dmg' : (3, 3, 1),
	'crit' : 1,
	'price' : mid,
	'level' : 1,
	'descr' : 'It may be ordinary, but you\'ll appreciate the consistent damage output.',
	'mods' : {
	}
},
{
	'name' : 'Horker\'s Wand',
	'dmg' : (0, 6, 1),
	'crit' : 0,
	'price' : low,
	'level' : 1,
	'descr' : 'Inconsistent damage output and no crit ability, but high damage potential.',
	'mods' : {
	}
},
#------------------------------------------------------------- 2 ---------------------------------------------------------------
{
	'name' : 'Venom',
	'dmg' : (3, 7, 1),
	'crit' : 5,
	'price' : high,
	'level' : 2,
	'descr' : 'Chance to poison on crit. Poison fanatics may enjoy using this wand.',
	'mods' : {
		'fx-on-crit' : ('status-fx', StatusEffect.Poison, 50)
	}
},
{
	'name' : 'Little Star',
	'dmg' : (3, 4, 1),
	'crit' : 18,
	'price' : low,
	'level' : 2,
	'descr' : 'Sub-par base damage, but very high crit chance that restores mp on success.',
	'mods' : {
		'fx-on-crit' : ('mp-plus', 2)
	}
},
{
	'name' : 'Cudgel',
	'dmg' : (7, 9, 1),
	'crit' : 1,
	'price' : mid,
	'level' : 2,
	'descr' : 'No frills wand tuned for base damage. Low crit chance.',
	'mods' : {
	}
},
#------------------------------------------------------------- 3 ---------------------------------------------------------------
{
	'name' : 'Firebrand',
	'dmg' : (8, 12, 1),
	'crit' : 5,
	'price' : mid,
	'level' : 3,
	'descr' : 'A spicy wand that does additional damage on fire attacks',
	'mods' : {
		'element-boost' : [(Element.Fire, 4)]
	}
},
{
	'name' : 'Dual Wands',
	'dmg' : (4, 10, 2),
	'crit' : 7,
	'price' : high,
	'level' : 3,
	'descr' : 'What\'s cooler than dual wielding wands? (Chance to crit on both wands)',
	'mods' : {
	}
},
{
	'name' : 'Mingus Master',
	'dmg' : (9, 14, 1),
	'crit' : 9,
	'price' : low,
	'level' : 3,
	'descr' : 'A good all-around wand with nice crit chance, once used by famouse Mage Charles Mingus',
	'mods' : {
	}
},
#------------------------------------------------------------- 3 ---------------------------------------------------------------
{
	'name' : 'Moonshot',
	'dmg' : (0, 25, 1),
	'crit' : 11,
	'price' : low,
	'level' : 4,
	'descr' : 'A wand with a pathetic minimum damage and astounding max damage. Good crit chance gives it deadly potential.',
	'mods' : {
	}
},
{
	'name' : 'Squeaker Wand',
	'dmg' : (12, 15, 1),
	'crit' : 4,
	'price' : high,
	'level' : 4,
	'descr' : 'A wand for sly fellows. Increases the chance of inflicting status effects.',
	'mods' : {
		'status-up': 12
	}
},
{
	'name' : 'Sparkplug',
	'dmg' : (12, 19, 1),
	'crit' : 5,
	'price' : mid,
	'level' : 4,
	'descr' : 'Increases damage output on Electric attacks.',
	'mods' : {
		'element-boost' : [(Element.Electric, 6)]
	}
},
#------------------------------------------------------------- 4 ---------------------------------------------------------------
{
	'name' : 'September',
	'dmg' : (12, 15, 1),
	'crit' : 5,
	'price' : mid,
	'level' : 5,
	'descr' : 'A dull wand under normal circumstances, but with a massive damage increase on Earth, Wind, or Fire attacks.',
	'mods' : {
		'element-boost' : [(Element.Earth, 10), (Element.Wind, 10), (Element.Fire, 10)]
	}
},
{
	'name' : 'Medic\'s Wand',
	'dmg' : (17, 23, 1),
	'crit' : 6,
	'price' : high,
	'level' : 5,
	'descr' : 'A wand which heals HP on crits. Good if you like healing HP on crits.',
	'mods' : {
		'fx-on-crit' : ('hp-plus', 10)
	}
},
{
	'name' : 'Husky Dusker',
	'dmg' : (20, 20, 1),
	'crit' : 2,
	'price' : low,
	'level' : 5,
	'descr' : 'Mediocre Crit but consistent damage output. A Husky Dusker for Musty Buskers',
	'mods' : {
	}
},
#------------------------------------------------------------- 4 ---------------------------------------------------------------
{
	'name' : 'Elsa\'s Wand',
	'dmg' : (23, 25, 1),
	'crit' : 4,
	'price' : mid,
	'level' : 6,
	'descr' : 'A wand that increases Ice damage and Freeze chance. Sponsored by Disney\'s™ Frozen®',
	'mods' : {
		'element-boost' : [(Element.Ice, 8)],
		'status-up-on': [(StatusEffect.Freeze, 12)]
	}
},
{
	'name' : 'Stinky Dinker',
	'dmg' : (21, 26, 1),
	'crit' : 3,
	'price' : low,
	'level' : 6,
	'descr' : 'A wand forged by Swamp Imps that has a small chance to poison after every single attack.',
	'mods' : {
		'inflict-chance' : [(StatusEffect.Poison, 2, 6)]
	}
},
{
	'name' : 'Pearl Diver',
	'dmg' : (20, 24, 1),
	'crit' : 6,
	'price' : high,
	'level' : 6,
	'descr' : 'A wand with a very special ability: A chance to completely ignore the enemy\'s resistances when you cast any spell.',
	'mods' : {
		'ignore-resist' : 20
	}
},
#------------------------------------------------------------- 4 ---------------------------------------------------------------
{
	'name' : 'Dark Star',
	'dmg' : (24, 27, 1),
	'crit' : 7,
	'price' : low,
	'level' : 7,
	'descr' : 'Discovered in the top floor of the Felled God\'s Tower. Doubles Void Damage (4X with crits!)',
	'mods' : {
		'element-boost' : [(Element.Void, 24)]
	}
},
{
	'name' : 'Thresher',
	'dmg' : (0, 35, 1),
	'crit' : 4,
	'price' : low,
	'level' : 7,
	'descr' : 'Powered by the latest advances in Magical Science. Min dmg has a chance to increase by 1 every time it\'s used.',
	'mods' : {
		'atk-up' : (1, 50)
	}
},
{
	'name' : 'Buster Wand',
	'dmg' : (20, 30, 1),
	'crit' : 25,
	'price' : low,
	'level' : 7,
	'descr' : 'The Buster Wand. The biggest, heaviest, longest wand on the market. The ultimate crit machine.',
	'mods' : {
	}
}

]