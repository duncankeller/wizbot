from itemCommon import *
from enemies import * 

 
dungeonList = [
{
	'name' : 'Kobold Gutter',
	'sequence': [
		'',
		'',
		'Kobold',
		'',
		'Stinking Kobold', 
		'',
		'chest',
		'',
		'Kobold', 
		'',
		'Stinking Kobold', 
		'',
		'Kobold',
		'',
		'Hulking Kobold'
	],
	'item-rewards' : [
		'Lesser Health Potion'
	],
	'chest-wizcoin' : [
		50,
		25,
		25,
		25,
	],
	'enemy-wizcoin' : (5,15),
	'boss-reward-wizcoin' : 100
},




]