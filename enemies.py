from itemCommon import * 

enemyTemplate = {
	'name' : 'name',
	'hp' : 0,
	'mp' : 0,
	'maxHp' : 0,
	'maxMp' : 0,
	'icon' : 'x',
	'resist' : [],
	'weak' : [],
	'attacks' :  [],

	'status' : StatusEffect.None_,
	'status-time' : 0

}

def makeEnemyFromTemplate(enemy):
	newEnemy = enemyTemplate
	newEnemy['name'] = enemy['name']
	newEnemy['hp'] = enemy['hp']
	newEnemy['maxHp'] = enemy['hp']
	newEnemy['mp'] = enemy['mp']
	newEnemy['maxMp'] = enemy['mp']
	newEnemy['attacks'] = enemy['attacks']
	newEnemy['icon'] = enemy['icon']
	newEnemy['weak'] = enemy['weak']
	newEnemy['resist'] = enemy['resist']


fullEnemyList = [ 
{
#------------------------------------------------------------- 0 ---------------------------------------------------------------
	'name' : 'Kobold',

	'hp' : 4,
	'mp' : 0,
	'icon' : '👹',
	'exp' : 1,

	'attacks' :  [
	{
		'descr' : 'swings his dagger',
		'dmg' : (1, 2, 1),
		'chance' : 85
	},
	{
		'descr' : 'trips over his feet',
		'chance' : 15
	}
	]
},
{
	'name' : 'Stinking Kobold',
	'hp' : 3,
	'mp' : 0,
	'icon' : '👹',
	'exp' : 1,

	'attacks' :  [
	{
		'descr' : 'uses his claws',
		'dmg' : (0, 1, 1),
		'chance' : 40
	},
	{
		'descr' : 'looks bored',
		'chance' : 4
	},
	{
		'descr' : 'throws a poison wad',
		'effect' : (StatusEffect.Poison, 30),
		'chance' : 15
	}
	]
},
{
	'name' : 'Hulking Kobold',
	'hp' : 10,
	'mp' : 0,
	'icon' : '👹',
	'exp' : 3,

	'attacks' :  [
	{
		'descr' : 'charges forward',
		'dmg' : (1, 3, 1),
		'chance' : 50
	},
	{
		'descr' : 'is lost in thought',
		'chance' : 5
	},
	{
		'descr' : 'throws a sloppy punch',
		'dmg' : (1, 1, 1),
		'chance' : 80
	}
	]
},
#------------------------------------------------------------- 1 ---------------------------------------------------------------
{
	'name' : 'Venomous Snake',
	'hp' : 6,
	'mp' : 0,
	'icon' : '🐍',
	'exp' : 2,

	'attacks' :  [
	{
		'descr' : 'lets out a warning hiss',
		'chance' : 5
	},
	{
		'descr' : 'uses its fangs',
		'dmg' : (0, 2, 1),
		'effect' : (StatusEffect.Poison, 40),
		'chance' : 80
	}
	]
},
{
	'name' : 'Giant Rat',
	'hp' : 7,
	'mp' : 0,
	'icon' : '🐀',
	'exp' : 2,

	'attacks' :  [
	{
		'descr' : 'uses its greasy claws',
		'dmg' : (1, 2, 1),
		'chance' : 1
	}
	]
},
{
	'name' : 'Cave Bat',
	'hp' : 4,
	'mp' : 0,
	'icon' : '🦇',
	'exp' : 2,

	'attacks' :  [
	{
		'descr' : 'uses its fangs',
		'dmg' : (2, 3, 1),
		'chance' : 1
	}
	]
},
{
	'name' : 'Massive Toad',
	'hp' : 26,
	'mp' : 0,
	'icon' : '🐸',
	'resist' : [Element.Fire, Element.Water, StatusEffect.Burn],
	'weak' : [Element.Ice],
	'exp' : 5,

	'attacks' :  [
	{
		'descr' : 'uses its tongue as a whip',
		'dmg' : (1, 2, 1),
		'chance' : 30
	},
	{
		'descr' : 'knocks you over with its sheer girth',
		'dmg' : (0, 1, 1),
		'chance' : 30
	},
	{
		'descr' : 'belches',
		'effect' : (StatusEffect.Poison, 100),
		'chance' : 5
	},
	{
		'descr' : 'swallows you up and spits you out',
		'dmg' : (4, 4, 1),
		'element' : Element.Water,
		'chance' : 20
	}
	]
},
#------------------------------------------------------------- 2 ---------------------------------------------------------------
{
	'name' : 'Frost Gnome',
	'hp' : 16,
	'mp' : 8,
	'icon' : '⛄️',
	'resist' : [Element.Ice, StatusEffect.Freeze],
	'weak' : [Element.Fire],
	'exp' : 4,

	'attacks' : [
	{
		'descr' : 'slashes with an ice dagger',
		'dmg' : (4, 6, 1),
		'element' : Element.Ice,
		'chance' : 50
	},
	{
		'descr' : 'casts Freeze',
		'effect' : (StatusEffect.Freeze, 30),
		'element' : Element.Ice,
		'mp-cost': 4,
		'dmg' : (2, 3, 1),
		'chance' : 30
	}
	]
},
{
	'name' : 'Ice Jelly',
	'hp' : 18,
	'mp' : 0,
	'icon' : '🧊',
	'resist' : [Element.Ice, StatusEffect.Freeze],
	'weak' : [Element.Fire],
	'exp' : 4,

	'attacks' :  [
	{
		'descr' : 'lunges',
		'dmg' : (3, 4, 1),
		'effect' : (StatusEffect.Freeze, 10),
		'element' : Element.Ice,
		'chance' : 50
	}
	]
},
{
	'name' : 'Storm Lord',
	'hp' : 58,
	'mp' : 20,
	'icon' : '🌬',
	'resist' : [Element.Ice, StatusEffect.Freeze],
	'weak' : [Element.Fire],
	'exp' : 9,

	'attacks' :  [
	{
		'descr' : 'bellows',
		'dmg' : (4, 6, 1),
		'effect' : (StatusEffect.Freeze, 10),
		'element' : Element.Ice,
		'chance' : 30
	},
	{
		'descr' : 'calls upon the great storm',
		'dmg' : (10, 10, 1),
		'effect' : (StatusEffect.Freeze, 20),
		'element' : Element.Ice,
		'mp-cost': 4,
		'chance' : 10
	},
	{
		'descr' : 'brings down icy rain',
		'dmg' : (1, 3, 3),
		'effect' : (StatusEffect.Freeze, 8),
		'element' : Element.Ice,
		'chance' : 20
	},
	{
		'descr' : 'makes snow begin to fall',
		'effect' : (StatusEffect.Freeze, 40),
		'element' : Element.Ice,
		'mp-cost': 5,
		'chance' : 7
	}
	]
},
#------------------------------------------------------------- 3 ---------------------------------------------------------------
{
	'name' : 'Skeleton Warrior',
	'hp' : 20,
	'mp' : 0,
	'icon' : '💀',
	'resist' : [StatusEffect.Sleep],
	'weak' : [Element.Light],
	'exp' : 6,

	'attacks' :  [
	{
		'descr' : 'slashes its bone-sword',
		'dmg' : (2, 8, 1),
		'chance' : 80
	},
	{
		'descr' : 'raises its shield',
		'self-effect' : (StatusEffect.Protect, 100),
		'mp-cost': 4,
		'chance' : 10
	},
	{
		'descr' : 'does a useless skeleton dance',
		'chance' : 5
	}
	]
},
{
	'name' : 'Skeleton Mage',
	'hp' : 18,
	'mp' : 14,
	'icon' : '💀',
	'resist' : [StatusEffect.Sleep],
	'weak' : [Element.Light],
	'exp' : 6,

	'attacks' :  [
	{
		'descr' : 'casts bone-crunch',
		'dmg' : (1, 5, 2),
		'element' : Element.Void,
		'mp-cost': 2,
		'chance' : 80
	},
	{
		'descr' : 'casts weaken',
		'effect' : (StatusEffect.Weaken, 50),
		'element' : Element.Void,
		'mp-cost': 4,
		'chance' : 30
	},
	{
		'descr' : 'slaps with its bony hand',
		'dmg' : (0, 2, 1),
		'chance' : 10
	},
	{
		'descr' : 'plays its ribcage like a xylophone',
		'chance' : 5
	}
	]
},
{
	'name' : 'Undead Soldier',
	'hp' : 24,
	'mp' : 0,
	'icon' : '🧟',
	'resist' : [StatusEffect.Sleep],
	'weak' : [Element.Fire],
	'exp' : 5,

	'attacks' :  [
	{
		'descr' : 'limps towards you',
		'dmg' : (1, 3, 1),
		'chance' : 80
	}
	]
},
{
	'name' : 'Beholder King',
	'hp' : 76,
	'mp' : 15,
	'icon' : '🧿',
	'weak' : [StatusEffect.Confusion],
	'exp' : 12,

	'attacks' :  [
	{
		'descr' : 'fires a laser',
		'dmg' : (0, 3, 4),
		'element' : Element.Fire,
		'chance' : 80
	},
	{
		'descr' : 'fires a laser',
		'dmg' : (1, 3, 2),
		'element' : Element.Fire,
		'chance' : 60
	},
	{
		'descr' : 'sends a piercing gaze',
		'effect' : (StatusEffect.Paralisis, 40),
		'chance' : 10
	},
	{
		'descr' : 'rolls slowly towards you',
		'chance' : 2
	}
	]
},
#------------------------------------------------------------- 4 ---------------------------------------------------------------
{
	'name' : 'Bengal Tiger',
	'hp' : 30,
	'mp' : 0,
	'icon' : '🐯',
	'resist' : [StatusEffect.Weaken],
	'weak' : [Element.Fire],
	'exp' : 8,

	'attacks' :  [
	{
		'descr' : 'bites',
		'dmg' : (6, 8, 2),
		'chance' : 80
	}
	]
},
{
	'name' : 'Enraged Bear',
	'hp' : 38,
	'mp' : 0,
	'icon' : '🐻',
	'resist' : [Element.Earth, StatusEffect.Freeze],
	'weak' : [StatusEffect.Sleep, StatusEffect.Poison],
	'exp' : 8,

	'attacks' :  [
	{
		'descr' : 'swipes with its claws',
		'dmg' : (5, 6, 2),
		'chance' : 80
	},
	{
		'descr' : 'takes a power nap',
		'hp-up' : 8,
		'chance' : 15
	}
	]
},
{
	'name' : 'Mad Hen',
	'hp' : 20,
	'mp' : 0,
	'icon' : '🐔',
	'weak' : [Element.Electric, Element.Ice],
	'exp' : 9,

	'attacks' :  [
	{
		'descr' : 'pecks at your eyes',
		'dmg' : (2, 3, 1),
		'effect' : (StatusEffect.Confusion, 66),
		'chance' : 40
	},
	{
		'descr' : 'uses its razor-sharp talons',
		'dmg' : (0, 12, 1),
		'chance' : 70
	}
	]
},
{
	'name' : 'Hell Hog',
	'hp' : 94,
	'mp' : 0,
	'icon' : '🐗',
	'resist' : [Element.Electric, Element.Ice, Element.Fire],
	'exp' : 19,

	'attacks' :  [
	{
		'descr' : 'lunges',
		'dmg' : (6, 10, 1),
		'chance' : 40
	},
	{
		'descr' : 'rages',
		'dmg' : (1, 5, 3),
		'chance' : 40
	},
	{
		'descr' : 'ravages',
		'dmg' : (14, 14, 1),
		'effect' : (StatusEffect.Confusion, 20),
		'chance' : 10
	}
	]
},
#------------------------------------------------------------- 5 ---------------------------------------------------------------
{
	'name' : 'Shark',
	'hp' : 36,
	'mp' : 0,
	'icon' : '🦈',
	'resist' : [Element.Water, Element.Fire],
	'weak' : [Element.Ice],
	'exp' : 10,

	'attacks' :  [
	{
		'descr' : 'bites',
		'dmg' : (8, 8, 1),
		'element' : Element.Water,
		'chance' : 60
	},
	{
		'descr' : 'smells blood',
		'chance' : 20
	}
	]
},
{
	'name' : 'Killer Whale',
	'hp' : 46,
	'mp' : 0,
	'icon' : '🐋',
	'resist' : [Element.Water, Element.Fire],
	'weak' : [Element.Ice],
	'exp' : 10,

	'attacks' :  [
	{
		'descr' : 'uses body slam',
		'dmg' : (6, 8, 1),
		'element' : Element.Water,
		'chance' : 60
	},
	{
		'descr' : 'sings a whale song',
		'effect' : (StatusEffect.Sleep, 30),
		'chance' : 20
	}
	]
},
{
	'name' : 'Lobster',
	'hp' : 26,
	'mp' : 0,
	'icon' : '🦞',
	'resist' : [Element.Water, Element.Fire],
	'weak' : [Element.Ice],
	'exp' : 10,

	'attacks' :  [
	{
		'descr' : 'snaps with its poison claws',
		'dmg' : (2, 4, 1),
		'element' : Element.Water,
		'effect' : (StatusEffect.Poison, 50),
		'chance' : 60
	}
	]
},
{
	'name' : 'Mer Folk',
	'hp' : 38,
	'mp' : 20,
	'icon' : '🧜‍',
	'resist' : [Element.Water, Element.Fire],
	'weak' : [Element.Ice],
	'exp' : 10,

	'attacks' :  [
	{
		'descr' : 'casts whirlpool',
		'dmg' : (10, 10, 1),
		'element' : Element.Water,
		'mp-cost': 5,
		'chance' : 20
	},
	{
		'descr' : 'casts siren song',
		'mp-cost': 4,
		'element' : Element.Water,
		'effect' : (StatusEffect.Sleep, 50),
		'chance' : 20
	},
	{
		'descr' : 'stabs with its trident',
		'dmg' : (4, 9, 1),
		'chance' : 40
	}
	]
},
{
	'name' : 'Giant Enemy Crab',
	'hp' : 124,
	'mp' : 0,
	'icon' : '🦀',
	'resist' : [Element.Water, Element.Fire],
	'weak' : [Element.Ice],
	'exp' : 27,
	'armor' : 0,

	'attacks' :  [
	{
		'descr' : 'hunkers down',
		'armor-up' : 1,
		'chance' : 40
	},
	{
		'descr' : 'uses its meaty claws',
		'dmg' : (5, 7, 1),
		'chance' : 40
	},
	{
		'descr' : 'hardens',
		'self-effect' : (StatusEffect.Protect, 70),
		'chance' : 20
	}
	]
},
#------------------------------------------------------------- 6 ---------------------------------------------------------------
{
	'name' : 'Dracula',
	'hp' : 46,
	'mp' : 22,
	'icon' : '🧛🏻',
	'resist' : [Element.Void],
	'weak' : [Element.Light, Element.Fire],
	'exp' : 13,

	'attacks' :  [
	{
		'descr' : 'vants to suck your blooood',
		'dmg' : (5, 6, 1),
		'element' : Element.Void,
		'drain' : 'hp',
		'mp-cost': 5,
		'chance' : 20
	},
	{
		'descr' : 'swoops down',
		'dmg' : (8, 12, 1),
		'chance' : 20
	}
	]
},
{
	'name' : 'Spooky Ghost',
	'hp' : 38,
	'mp' : 0,
	'icon' : '👻',
	'resist' : [Element.Void],
	'weak' : [Element.Light],
	'exp' : 13,
	'evade' : 40,

	'attacks' :  [
	{
		'descr' : 'makes a scary face',
		'effect' : (StatusEffect.Paralisis, 70),
		'chance' : 20
	},
	{
		'descr' : 'spewed slime',
		'dmg' : (6, 9, 1),
		'element' : Element.Void,
		'chance' : 80
	}
	]
},
{
	'name' : 'Creepy Crawly Spider',
	'hp' : 50,
	'mp' : 0,
	'icon' : '🕷',
	'resist' : [],
	'weak' : [],
	'exp' : 12,

	'attacks' :  [
	{
		'descr' : 'bites',
		'dmg' : (10, 12, 1),
		'effect' : (StatusEffect.Poison, 70),
		'chance' : 20
	},
	{
		'descr' : 'crawls around. It\'s totally gross.',
		'chance' : 1
	}
	]
},
{
	'name' : 'Jack-O-Man',
	'hp' : 190,
	'mp' : 66,
	'icon' : '🎃',
	'resist' : [Element.Void, Element.Water, Element.Fire, Element.Ice, Element.Electric],
	'weak' : [Element.Wind],
	'exp' : 32,

	'attacks' :  [
	{
		'descr' : 'dances with a burning passion',
		'dmg' : (7, 10, 1),
		'element' : Element.Fire,
		'effect' : (StatusEffect.Burn, 30),
		'mp-cost': 4,
		'chance' : 20
	},
	{
		'descr' : 'dances with a cool flair',
		'dmg' : (8, 8, 1),
		'element' : Element.Water,
		'mp-cost': 2,
		'chance' : 20
	},
	{
		'descr' : 'dances with a chilling style',
		'dmg' : (6, 9, 1),
		'element' : Element.Ice,
		'effect' : (StatusEffect.Freeze, 30),
		'mp-cost': 4,
		'chance' : 20
	},
	{
		'descr' : 'dances with an electric energy',
		'dmg' : (5, 12, 1),
		'element' : Element.Electric,
		'effect' : (StatusEffect.Paralisis, 30),
		'mp-cost': 4,
		'chance' : 20
	}
	]
},
#------------------------------------------------------------- 7 ---------------------------------------------------------------
{
	'name' : 'Smiling Clown',
	'hp' : 60,
	'mp' : 0,
	'icon' : '🤡',
	'resist' : [Element.Water, StatusEffect.Confusion],
	'weak' : [],
	'exp' : 14,

	'attacks' :  [
	{
		'descr' : 'sprays seltzer water',
		'element' : Element.Water,
		'dmg' : (0, 20, 1),
		'chance' : 20
	},
	{
		'descr' : 'throws a bowling pin',
		'dmg' : (5, 15, 1),
		'chance' : 20
	},
	{
		'descr' : 'starts laughing',
		'effect' : (StatusEffect.Confusion, 80),
		'chance' : 10
	}
	]
},
{
	'name' : 'Juggleman',
	'hp' : 60,
	'mp' : 0,
	'icon' : '🤹',
	'resist' : [Element.Fire, StatusEffect.Paralisis, Element.Electric, StatusEffect.Burn],
	'weak' : [StatusEffect.Confusion],
	'exp' : 14,
	'evade' : 10,

	'attacks' :  [
	{
		'descr' : 'tosses a lead ball',
		'dmg' : (0, 18, 1),
		'chance' : 20
	},
	{
		'descr' : 'tosses a flaming ball',
		'dmg' : (0, 15, 1),
		'effect' : (StatusEffect.Burn, 15),
		'element' : Element.Fire,
		'chance' : 20
	},
	{
		'descr' : 'tosses an electrically charged ball',
		'dmg' : (0, 12, 1),
		'effect' : (StatusEffect.Paralisis, 15),
		'element' : Element.Electric,
		'chance' : 20
	}
	]
},
{
	'name' : 'Strongman',
	'hp' : 58,
	'mp' : 0,
	'icon' : '🏋🏻',
	'resist' : [],
	'weak' : [StatusEffect.Paralisis, StatusEffect.Sleep, Element.Water],
	'exp' : 14,

	'attacks' :  [
	{
		'descr' : 'sends forth a supersonic punch',
		'dmg' : (0, 32, 1),
		'chance' : 20
	},
	{
		'descr' : 'pumps iron',
		'chance' : 20
	}
	]
},
{
	'name' : 'Big Top Tent',
	'hp' : 240,
	'mp' : 220,
	'icon' : '🎪',
	'resist' : [StatusEffect.Burn, StatusEffect.Freeze, StatusEffect.Paralisis, StatusEffect.Confusion, StatusEffect.Poison, StatusEffect.Weaken, StatusEffect.Sleep],
	'weak' : [Element.Wind, Element.Fire],
	'exp' : 42,

	'attacks' :  [
	{
		'descr' : 'opens its entrance, releasing pestilence',
		'dmg' : (3, 5, 1),
		'effect' : (StatusEffect.Poison, 30),
		'mp-cost': 3,
		'chance' : 20
	},
	{
		'descr' : 'opens its entrance, releasing icy winds',
		'dmg' : (3, 5, 1),
		'effect' : (StatusEffect.Freeze, 30),
		'mp-cost': 3,
		'chance' : 20
	},
	{
		'descr' : 'opens its entrance, releasing embers',
		'dmg' : (3, 5, 1),
		'effect' : (StatusEffect.Burn, 30),
		'mp-cost': 3,
		'chance' : 20
	},
	{
		'descr' : 'opens its entrance, releasing a horde of rats',
		'dmg' : (1, 3, 5),
		'chance' : 20
	}
	]
},
#------------------------------------------------------------- 8 ---------------------------------------------------------------
{
	'name' : 'Alien Wiz',
	'hp' : 68,
	'mp' : 40,
	'icon' : '👽',
	'resist' : [StatusEffect.Paralisis, Element.Electric],
	'weak' : [Element.Ice],
	'exp' : 17,

	'attacks' :  [
	{
		'descr' : 'casts an otherworldly hex',
		'dmg' : (1, 6, 1),
		'mp-cost': 5,
		'effect' : (StatusEffect.Paralisis, 90),
		'chance' : 2
	},
	{
		'descr' : 'casts a whirling bolt of darkness',
		'dmg' : (12, 15, 1),
		'mp-cost': 5,
		'effect' : (StatusEffect.Paralisis, 5),
		'element' : Element.Electric,
		'chance' : 5
	}
	]
},
{
	'name' : 'Alien Junkboy',
	'hp' : 72,
	'mp' : 0,
	'icon' : '🛸',
	'resist' : [StatusEffect.Paralisis, Element.Electric],
	'weak' : [Element.Ice],
	'exp' : 17,

	'attacks' :  [
	{
		'descr' : 'fires a laser',
		'dmg' : (2, 8, 4),
		'chance' : 10
	}
	]
},
{
	'name' : 'Cyber Freak',
	'hp' : 65,
	'mp' : 0,
	'icon' : '🤖',
	'resist' : [StatusEffect.Paralisis, Element.Electric, StatusEffect.Sleep],
	'weak' : [StatusEffect.Confusion],
	'exp' : 17,

	'attacks' :  [
	{
		'descr' : 'uses a cattle prod',
		'dmg' : (12, 18, 1),
		'element' : Element.Electric,
		'effect' : (StatusEffect.Paralisis, 30),
		'chance' : 10
	},
	{
		'descr' : 'shoots an electric bolt',
		'dmg' : (14, 18, 1),
		'element' : Element.Electric,
		'chance' : 20
	}
	]
},
{
	'name' : 'Seer',
	'hp' : 185,
	'mp' : 240,
	'icon' : '🧠',
	'resist' : [StatusEffect.Paralisis, Element.Electric, StatusEffect.Silence],
	'weak' : [Element.Ice, StatusEffect.Sleep],
	'exp' : 30,

	'attacks' :  [
	{
		'descr' : 'feasts',
		'dmg' : (10, 14, 1),
		'chance' : 20
	},
	{
		'descr' : 'makes your mind quiet as the grave',
		'effect' : (StatusEffect.Silence, 60),
		'element' : Element.Electric,
		'mp-cost': 5,
		'chance' : 10
	},
	{
		'descr' : 'glances into your fears',
		'effect' : (StatusEffect.Weaken, 40),
		'element' : Element.Electric,
		'mp-cost': 6,
		'chance' : 10
	},
	{
		'descr' : 'drinks from your consciousness',
		'drain-mp' : (0, 8),
		'mp-cost': 2,
		'element' : Element.Electric,
		'chance' : 8
	}
	]
},
#------------------------------------------------------------- 9 ---------------------------------------------------------------
{
	'name' : 'Rook',
	'hp' : 86,
	'mp' : 12,
	'icon' : '♜',
	'resist' : [StatusEffect.Sleep, StatusEffect.Paralisis, StatusEffect.Confusion],
	'weak' : [StatusEffect.Silence],
	'exp' : 20,

	'attacks' :  [
	{
		'descr' : 'moves to B7',
		'dmg' : (17, 18, 1),
		'chance' : 5
	},
	{
		'descr' : 'moves to E1',
		'dmg' : (17, 18, 1),
		'chance' : 5
	},
	{
		'descr' : 'moves to F4',
		'dmg' : (17, 18, 1),
		'chance' : 5
	},
	{
		'descr' : 'moves to D7',
		'dmg' : (17, 18, 1),
		'chance' : 5
	},
	{
		'descr' : 'moves to A3',
		'self-effect' : (StatusEffect.Protect, 100),
		'mp-cost': 5,
		'chance' : 10
	}
	]
},
{
	'name' : 'Knight',
	'hp' : 82,
	'mp' : 0,
	'icon' : '♞',
	'resist' : [StatusEffect.Confusion],
	'weak' : [StatusEffect.Weaken],
	'exp' : 20,

	'attacks' :  [
	{
		'descr' : 'moves to A4',
		'dmg' : (20, 22, 1),
		'chance' : 1
	},
	{
		'descr' : 'moves to G2',
		'dmg' : (19, 21, 1),
		'chance' : 1
	},
	{
		'descr' : 'moves to F8',
		'dmg' : (21, 23, 1),
		'chance' : 1
	},
	{
		'descr' : 'moves to B6',
		'dmg' : (19, 24, 1),
		'chance' : 1
	}
	]
},
{
	'name' : 'Bishop',
	'hp' : 76,
	'mp' : 60,
	'icon' : '♝',
	'resist' : [StatusEffect.Silence],
	'weak' : [StatusEffect.Weaken],
	'exp' : 20,

	'attacks' :  [
	{
		'descr' : 'moves to C4',
		'dmg' : (10, 14, 1),
		'effect' : (StatusEffect.Freeze, 30),
		'element' : Element.Ice,
		'mp-cost': 5,
		'chance' : 1
	},
	{
		'descr' : 'moves to G7',
		'dmg' : (10, 14, 1),
		'effect' : (StatusEffect.Poison, 30),
		'mp-cost': 5,
		'chance' : 2
	},
	{
		'descr' : 'moves to D2',
		'effect' : (StatusEffect.Sleep, 30),
		'mp-cost': 6,
		'chance' : 1
	},
	{
		'descr' : 'moves to H3',
		'dmg' : (10, 14, 1),
		'element' : Element.Fire,
		'effect' : (StatusEffect.Burn, 30),
		'mp-cost': 5,
		'chance' : 1
	}
	]
},
{
	'name' : 'Queen',
	'hp' : 260,
	'mp' : 100,
	'icon' : '♛',
	'resist' : [StatusEffect.Sleep, StatusEffect.Paralisis, StatusEffect.Confusion],
	'weak' : [StatusEffect.Silence, StatusEffect.Poison, StatusEffect.Burn],
	'exp' : 36,

	'attacks' :  [
	{
		'descr' : 'moves to a defensive position',
		'raise-atk' : 5,
		'mp-cost': 6,
		'chance' : 5
	},
	{
		'descr' : 'puts you into check',
		'dmg' : (10, 10, 1),
		'chance' : 5
	}
	]
},
#------------------------------------------------------------- 10 ---------------------------------------------------------------
{
	'name' : 'Fire Caster',
	'hp' : 90,
	'mp' : 52,
	'icon' : '𓀎',
	'resist' : [Element.Fire, StatusEffect.Burn, StatusEffect.Weaken],
	'weak' : [StatusEffect.Silence, Element.Water],
	'exp' : 24,

	'attacks' :  [
	{
		'descr' : 'summons unholy flame',
		'dmg' : (24, 26, 1),
		'mp-cost': 7,
		'element' : Element.Fire,
		'effect' : (StatusEffect.Burn, 80),
		'chance' : 5
	}
	]
},
{
	'name' : 'Water Caster',
	'hp' : 90,
	'mp' : 52,
	'icon' : '𓀙',
	'resist' : [Element.Water, StatusEffect.Weaken],
	'weak' : [StatusEffect.Silence, Element.Ice],
	'exp' : 24,

	'attacks' :  [
	{
		'descr' : 'summons a tidal wave',
		'dmg' : (24, 26, 1),
		'mp-cost': 7,
		'element' : Element.Water,
		'chance' : 5
	}
	]
},
{
	'name' : 'Ice Caster',
	'hp' : 90,
	'mp' : 52,
	'icon' : '𓁫',
	'resist' : [Element.Ice, StatusEffect.Freeze, StatusEffect.Weaken],
	'weak' : [StatusEffect.Silence, Element.Fire],
	'exp' : 24,

	'attacks' :  [
	{
		'descr' : 'summons a ferocious blizzard',
		'dmg' : (24, 26, 1),
		'mp-cost': 7,
		'element' : Element.Ice,
		'effect' : (StatusEffect.Freeze, 80),
		'chance' : 5
	}
	]
},
{
	'name' : 'Wind Caster',
	'hp' : 90,
	'mp' : 52,
	'icon' : '𓁰',
	'resist' : [Element.Wind, StatusEffect.Weaken],
	'weak' : [StatusEffect.Silence, Element.Earth],
	'exp' : 24,

	'attacks' :  [
	{
		'descr' : 'summons a hellacious gust',
		'dmg' : (24, 26, 1),
		'mp-cost': 7,
		'element' : Element.Wind,
		'effect' : (StatusEffect.Confusion, 50),
		'chance' : 5
	}
	]
},
{
	'name' : 'Electric Caster',
	'hp' : 90,
	'mp' : 52,
	'icon' : '𓀀',
	'resist' : [Element.Electric, StatusEffect.Paralisis, StatusEffect.Weaken],
	'weak' : [StatusEffect.Silence, Element.Wind],
	'exp' : 24,

	'attacks' :  [
	{
		'descr' : 'summons a bolt of lightning',
		'dmg' : (24, 26, 1),
		'mp-cost': 7,
		'element' : Element.Electric,
		'effect' : (StatusEffect.Paralisis, 50),
		'chance' : 5
	}
	]
},
{
	'name' : 'Void Caster',
	'hp' : 280,
	'mp' : 240,
	'icon' : '𓂀',
	'resist' : [Element.Electric, Element.Wind, Element.Void, Element.Fire, Element.Ice, Element.Water, Element.Earth, StatusEffect.Weaken],
	'weak' : [Element.Light],
	'exp' : 44,

	'attacks' :  [
	{
		'descr' : 'conjures a swiling vortex',
		'dmg' : (14, 34, 2),
		'mp-cost': 8,
		'element' : Element.Void,
		'chance' : 10
	},
	{
		'descr' : 'conjures a swiling vortex',
		'dmg' : (14, 34, 1),
		'mp-cost': 8,
		'element' : Element.Void,
		'chance' : 10
	},
	{
		'descr' : 'summons darkness beyond darkness',
		'mp-cost': 7,
		'element' : Element.Void,
		'effect' : (StatusEffect.Silence, 60),
		'chance' : 4
	},
	{
		'descr' : 'leeches dark energy',
		'mp-cost': 9,
		'dmg' : (5, 15, 1),
		'element' : Element.Void,
		'drain' : 'hp',
		'chance' : 4
	},
	{
		'descr' : 'sings in an incomprehensible language',
		'mp-cost': 7,
		'element' : Element.Void,
		'effect' : (StatusEffect.Sleep, 60),
		'chance' : 4
	}
	]
},
#------------------------------------------------------------- 11 ---------------------------------------------------------------
{
	'name' : 'Kitchen Knife',
	'hp' : 90,
	'mp' : 0,
	'icon' : '🔪',
	'resist' : [StatusEffect.Sleep, StatusEffect.Paralisis, StatusEffect.Confusion, StatusEffect.Weaken],
	'weak' : [Element.Earth],
	'exp' : 28,

	'attacks' :  [
	{
		'descr' : 'slices downward',
		'dmg' : (30, 30, 1),
		'effect' : (StatusEffect.Paralisis, 10),
		'chance' : 5
	},
	{
		'descr' : 'cuts some undercooked chicken',
		'dmg' : (20, 20, 1),
		'effect' : (StatusEffect.Poison, 90),
		'chance' : 2
	}
	]
},
{
	'name' : 'Old Hammer',
	'hp' : 84,
	'mp' : 0,
	'icon' : '🔨',
	'resist' : [StatusEffect.Sleep, StatusEffect.Paralisis, StatusEffect.Confusion, StatusEffect.Weaken],
	'weak' : [Element.Wind],
	'exp' : 28,

	'attacks' :  [
	{
		'descr' : 'swings at you',
		'dmg' : (32, 34, 1),
		'effect' : (StatusEffect.Paralisis, 10),
		'chance' : 5
	},
	{
		'descr' : 'whacks your thumb',
		'dmg' : (23, 24, 1),
		'effect' : (StatusEffect.Confusion, 70),
		'chance' : 2
	}
	]
},
{
	'name' : 'Standard Plug',
	'hp' : 86,
	'mp' : 0,
	'icon' : '🔌',
	'resist' : [StatusEffect.Sleep, StatusEffect.Paralisis, StatusEffect.Confusion, StatusEffect.Weaken, Element.Electric],
	'weak' : [Element.Water],
	'exp' : 28,

	'attacks' :  [
	{
		'descr' : 'zaps you',
		'dmg' : (28, 30, 1),
		'effect' : (StatusEffect.Paralisis, 30),
		'element' : Element.Electric,
		'chance' : 5
	},
	{
		'descr' : 'charges up',
		'raise-atk' : 3,
		'chance' : 2
	}
	]
},
{
	'name' : 'Bath Boy',
	'hp' : 480,
	'mp' : 0,
	'icon' : '🛀',
	'resist' : [Element.Water, Element.Earth, Element.Fire, StatusEffect.Burn, StatusEffect.Weaken],
	'weak' : [Element.Electric, Element.Ice, StatusEffect.Freeze],
	'exp' : 50,

	'attacks' :  [
	{
		'descr' : 'splashes you',
		'dmg' : (36, 38, 2),
		'mp-cost': 8,
		'element' : Element.Water,
		'chance' : 20
	},
	{
		'descr' : 'splashes you with rancid water',
		'dmg' : (36, 38, 2),
		'mp-cost': 8,
		'effect' : (StatusEffect.Poison, 70),
		'element' : Element.Water,
		'chance' : 20
	},
	{
		'descr' : 'splashes you with stinky water',
		'dmg' : (36, 38, 2),
		'mp-cost': 8,
		'effect' : (StatusEffect.Weaken, 30),
		'element' : Element.Water,
		'chance' : 20
	}
	]
},
#------------------------------------------------------------- 12 ---------------------------------------------------------------
{
	'name' : 'Forest Sprite',
	'hp' : 104,
	'mp' : 82,
	'icon' : '🧚',
	'resist' : [Element.Light, StatusEffect.Silence],
	'weak' : [Element.Void],
	'exp' : 32,

	'attacks' :  [
	{
		'descr' : 'casts a small flash of light',
		'dmg' : (20, 35, 1),
		'mp-cost': 4,
		'chance' : 7
	},
	{
		'descr' : 'casts a massive flash of light',
		'dmg' : (30, 40, 1),
		'mp-cost': 8,
		'chance' : 7
	},
	{
		'descr' : 'sprinkles pixie dust',
		'mp-cost': 8,
		'effect' : (StatusEffect.Sleep, 50),
		'chance' : 3
	}
	]
},
{
	'name' : 'Wrathful Djinn',
	'hp' : 112,
	'mp' : 78,
	'icon' : '🧞',
	'resist' : [Element.Fire, StatusEffect.Burn, StatusEffect.Silence],
	'weak' : [Element.Void],
	'exp' : 32,

	'attacks' :  [
	{
		'descr' : 'causes flames to swell',
		'effect' : (StatusEffect.Burn, 80),
		'dmg' : (0, 25, 1),
		'mp-cost': 4,
		'chance' : 5
	},
	{
		'descr' : 'shoots a fireball',
		'effect' : (StatusEffect.Burn, 20),
		'dmg' : (15, 22, 2),
		'mp-cost': 7,
		'chance' : 7
	},
	{
		'descr' : 'summons a firey tornado',
		'effect' : (StatusEffect.Burn, 20),
		'dmg' : (30, 35, 1),
		'mp-cost': 7,
		'chance' : 6
	},
	]
},
{
	'name' : 'Sneaky Tengu',
	'hp' : 130,
	'mp' : 0,
	'icon' : '👺',
	'resist' : [Element.Water, Element.Earth, Element.Wind, Element.Fire, Element.Ice, StatusEffect.Sleep, StatusEffect.Burn, StatusEffect.Freeze, StatusEffect.Paralisis, StatusEffect.Weaken],
	'weak' : [Element.Void, StatusEffect.Confusion],
	'exp' : 32,

	'attacks' :  [
	{
		'descr' : 'unleashes a swift punch',
		'effect' : (StatusEffect.Paralisis, 5),
		'dmg' : (33, 37, 1),
		'chance' : 5
	},
	{
		'descr' : 'swings at you with a high kick',
		'effect' : (StatusEffect.Paralisis, 5),
		'dmg' : (35, 40, 1),
		'chance' : 5
	},
	{
		'descr' : 'throws an open-palmed jab',
		'effect' : (StatusEffect.Paralisis, 5),
		'dmg' : (5, 15, 3),
		'chance' : 5
	}
	]
},
{
	'name' : 'Drake of the Lake',
	'hp' : 510,
	'mp' : 100,
	'icon' : '🐉',
	'resist' : [Element.Water, Element.Light, StatusEffect.Weaken],
	'weak' : [Element.Void],
	'exp' : 60,

	'attacks' :  [
	{
		'descr' : 'fires a beam of bright, beautiful light',
		'dmg' : (40, 43, 1),
		'element' : Element.Light,
		'effect' : (StatusEffect.Confusion, 20),
		'chance' : 20
	},
	{
		'descr' : 'casts a protective aura of light',
		'dmg' : (40, 43, 1),
		'element' : Element.Light,
		'mp-cost': 10,
		'effect' : (StatusEffect.Protect, 70),
		'chance' : 5
	}
	]
},
#------------------------------------------------------------- 13 ---------------------------------------------------------------
{
	'name' : 'Dark Knight',
	'hp' : 150,
	'mp' : 0,
	'icon' : '🤺',
	'resist' : [Element.Void, StatusEffect.Weaken],
	'weak' : [Element.Light],
	'exp' : 36,

	'attacks' :  [
	{
		'descr' : 'swings his sword',
		'dmg' : (33, 37, 1),
		'element' : Element.Void,
		'chance' : 10
	},
	{
		'descr' : 'goes in for the kill',
		'dmg' : (80, 80, 1),
		'element' : Element.Void,
		'chance' : 1
	}
	]
},
{
	'name' : 'Stone Man',
	'hp' : 180,
	'mp' : 0,
	'icon' : '👤',
	'resist' : [Element.Void, StatusEffect.Weaken, StatusEffect.Confusion, StatusEffect.Burn, StatusEffect.Freeze, Element.Water, Element.Earth],
	'weak' : [Element.Light, Element.Wind],
	'exp' : 36,

	'attacks' :  [
	{
		'descr' : 'lumbers forward',
		'dmg' : (20, 35, 1),
		'element' : Element.Void,
		'chance' : 10
	},
	{
		'descr' : 'hunkers down',
		'self-effect' : (StatusEffect.Protect, 90),
		'element' : Element.Void,
		'chance' : 6
	},
	{
		'descr' : 'shoots a piercing glare',
		'effect' : (StatusEffect.Weaken, 75),
		'element' : Element.Void,
		'chance' : 3
	}
	]
},
{
	'name' : 'Twisted Elf Mage',
	'hp' : 140,
	'mp' : 350,
	'icon' : '🧝',
	'resist' : [Element.Void, StatusEffect.Silence, StatusEffect.Confusion],
	'weak' : [Element.Light],
	'exp' : 36,

	'attacks' :  [
	{
		'descr' : 'casts sparks of dark matter',
		'dmg' : (40, 40, 1),
		'element' : Element.Void,
		'mp-cost': 9,
		'chance' : 10
	},
	{
		'descr' : 'casts a beam of negative energy',
		'element' : Element.Void,
		'dmg' : (10, 30, 1),
		'drain-mp' : (5, 10),
		'mp-cost': 2,
		'drain' : 'hp',
		'chance' : 6
	}
	]
},
{
	'name' : 'Corrupted Dragon',
	'hp' : 240,
	'mp' : 0,
	'icon' : '🐲',
	'resist' : [Element.Void, Element.Fire],
	'weak' : [Element.Light],
	'exp' : 42,

	'attacks' :  [
	{
		'descr' : 'breathes unholy fire',
		'dmg' : (10, 15, 4),
		'element' : Element.Fire,
		'chance' : 10
	},
	{
		'descr' : 'breathes unholy fire',
		'dmg' : (10, 15, 3),
		'element' : Element.Fire,
		'chance' : 10
	},
	{
		'descr' : 'breathes unholy fire',
		'dmg' : (10, 15, 2),
		'element' : Element.Fire,
		'chance' : 10
	},
	{
		'descr' : 'breathes heinous black mist',
		'dmg' : (50, 60, 1),
		'element' : Element.Void,
		'effect' : (StatusEffect.Weaken, 15),
		'chance' : 39
	}
	]
},
{
	'name' : 'Mirror Mage',
	'hp' : 820,
	'mp' : 820,
	'icon' : '🧙',
	'resist' : [Element.Void, Element.Fire, Element.Earth, Element.Wind, Element.Electric, Element.Water, Element.Ice, StatusEffect.Confusion, StatusEffect.Silence, StatusEffect.Weaken],
	'weak' : [Element.Light],
	'exp' : 62,

	'attacks' :  [
	{
		'descr' : 'drinks from thine well',
		'drain-mp' : (10, 14),
		'mp-cost': 1,
		'chance' : 60
	},
	{
		'descr' : 'brings lightning down from the heavens',
		'element' : Element.Electric,
		'effect' : (StatusEffect.Paralisis, 20),
		'dmg' : (50, 65, 1),
		'mp-cost': 10,
		'chance' : 10
	},
	{
		'descr' : 'summons an ocean of flame',
		'element' : Element.Fire,
		'effect' : (StatusEffect.Burn, 20),
		'dmg' : (50, 65, 1),
		'mp-cost': 10,
		'chance' : 10
	},
	{
		'descr' : 'causes the room to become frigid',
		'element' : Element.Fire,
		'effect' : (StatusEffect.Freeze, 20),
		'dmg' : (50, 65, 1),
		'mp-cost': 10,
		'chance' : 10
	},
	{
		'descr' : 'unleashes a tsunami from his staff',
		'element' : Element.Water,
		'effect' : (StatusEffect.Weaken, 10),
		'dmg' : (50, 65, 1),
		'mp-cost': 10,
		'chance' : 10
	},
	{
		'descr' : 'causes the ground to sway',
		'element' : Element.Earth,
		'effect' : (StatusEffect.Sleep, 10),
		'dmg' : (50, 65, 1),
		'mp-cost': 10,
		'chance' : 10
	},
	{
		'descr' : 'sends forth a dizzying gale',
		'element' : Element.Wind,
		'effect' : (StatusEffect.Confusion, 10),
		'dmg' : (50, 65, 1),
		'mp-cost': 10,
		'chance' : 10
	},
	{
		'descr' : 'brings unholy darkness upon thee',
		'element' : Element.Void,
		'dmg' : (70, 90, 1),
		'mp-cost': 20,
		'chance' : 5
	}
	]
},
#------------------------------------------------------------- 14 ---------------------------------------------------------------
{
	'name' : 'Turkey',
	'hp' : 1000,
	'mp' : 0,
	'icon' : '🦃',
	'resist' : [Element.Void, Element.Fire, Element.Earth, Element.Wind, Element.Electric, Element.Water, Element.Ice, StatusEffect.Confusion, StatusEffect.Silence, StatusEffect.Weaken, StatusEffect.Freeze, Element.Void, Element.Fire, Element.Earth, Element.Wind, Element.Electric, Element.Water, Element.Ice, StatusEffect.Confusion, StatusEffect.Silence, StatusEffect.Burn, Element.Light, Element.Void, Element.Fire, Element.Earth, Element.Wind, Element.Electric, Element.Water, Element.Ice, StatusEffect.Confusion, StatusEffect.Silence, StatusEffect.Paralisis, Element.Void, Element.Fire, Element.Earth, Element.Wind, Element.Electric, Element.Water, Element.Ice, StatusEffect.Confusion, StatusEffect.Silence, StatusEffect.Freeze, Element.Void, Element.Fire, Element.Earth, Element.Wind, Element.Electric, Element.Water, Element.Ice, StatusEffect.Confusion, StatusEffect.Silence, StatusEffect.Burn, Element.Void, Element.Fire, Element.Earth, Element.Wind, Element.Electric, Element.Water, Element.Ice, StatusEffect.Confusion, StatusEffect.Silence, StatusEffect.Sleep],
	'weak' : [],
	'exp' : 1,

	'attacks' :  [
	{
		'descr' : 'gobbles',
		'dmg' : (70, 80, 1),
		'chance' : 1
	}
	]
},
{
	'name' : 'Lance Armstrong',
	'hp' : 10000,
	'mp' : 0,
	'icon' : '🚴',
	'resist' : [Element.Void, Element.Fire, Element.Earth, Element.Wind, Element.Electric, Element.Water, Element.Ice, StatusEffect.Confusion, StatusEffect.Silence, StatusEffect.Weaken, StatusEffect.Freeze, Element.Void, Element.Fire, Element.Earth, Element.Wind, Element.Electric, Element.Water, Element.Ice, StatusEffect.Confusion, StatusEffect.Silence, StatusEffect.Burn, Element.Light, Element.Void, Element.Fire, Element.Earth, Element.Wind, Element.Electric, Element.Water, Element.Ice, StatusEffect.Confusion, StatusEffect.Silence, StatusEffect.Paralisis, Element.Void, Element.Fire, Element.Earth, Element.Wind, Element.Electric, Element.Water, Element.Ice, StatusEffect.Confusion, StatusEffect.Silence, StatusEffect.Freeze, Element.Void, Element.Fire, Element.Earth, Element.Wind, Element.Electric, Element.Water, Element.Ice, StatusEffect.Confusion, StatusEffect.Silence, StatusEffect.Burn, Element.Void, Element.Fire, Element.Earth, Element.Wind, Element.Electric, Element.Water, Element.Ice, StatusEffect.Confusion, StatusEffect.Silence, StatusEffect.Sleep],
	'weak' : [],
	'exp' : 1000,

	'attacks' :  [
	{
		'descr' : 'juices up',
		'raise-atk' : 10,
		'chance' : 1
	},
	{
		'descr' : 'goes for gold',
		'dmg' : (5, 5, 1),
		'chance' : 1
	}
	]
}
]

#      🦃 ⚰      ⚉     
# ⚀⚁⚂⚃⚄⚅ ⛀             