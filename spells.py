from itemCommon import * 

# max level 6
allSpells = [
#------------------------------------------------------------- 0 ---------------------------------------------------------------
{
	'name' : 'Plonk',
	'attack' : True,
	'element' : Element.None_,
	'price' : 200,
	'mp-cost' : 0,
	'level' : 0,
	'does-dmg' : True,
	'descr' : 'A basic but essential spell that casts non-elemental damage. 0 MP cost makes it a must-have in any Wizard\'s spellbook',

},
#------------------------------------------------------------- 0 ---------------------------------------------------------------
{
	'name' : 'Minor Heal',
	'attack' : False,
	'element' : Element.Light,
	'price' : 3000,
	'mp-cost' : 5,
	'level' : 1,
	'does-dmg' : False,
	'descr' : 'A basic heal spell that restores 5 hp.',
	'heal-hp' : 5
},
{
	'name' : 'Heal',
	'attack' : False,
	'element' : Element.Light,
	'price' : 5000,
	'mp-cost' : 8,
	'level' : 4,
	'does-dmg' : False,
	'descr' : 'A basic heal spell that restores 15 hp.',
	'heal-hp' : 15
},
{
	'name' : 'Major Heal',
	'attack' : False,
	'element' : Element.Light,
	'price' : 8000,
	'mp-cost' : 14,
	'level' : 6,
	'does-dmg' : False,
	'descr' : 'A basic heal spell that restores 5 hp.',
	'heal-hp' : 30
},
{
	'name' : 'Fireball',
	'attack' : True,
	'element' : Element.Fire,
	'price' : 2000,
	'mp-cost' : 2,
	'level' : 1,
	'does-dmg' : True,
	'descr' : 'Adds Fire element to your next attack. Chance to inflict Burn',
	'inflict' : (StatusEffect.Burn, 30, 4) # effect, chance, effective turns
},
{
	'name' : 'Splash',
	'attack' : True,
	'element' : Element.Water,
	'price' : 2000,
	'mp-cost' : 2,
	'level' : 1,
	'does-dmg' : True,
	'descr' : 'Adds Water element to your next attack.',
},
{
	'name' : 'Zap',
	'attack' : True,
	'element' : Element.Electric,
	'price' : 2500,
	'mp-cost' : 3,
	'level' : 1,
	'does-dmg' : True,
	'descr' : 'Adds Electric element to your next attack. Chance to inflict Paralisis',
	'inflict' : (StatusEffect.Paralisis, 30, 3) # effect, chance, effective turns
},
{
	'name' : 'Blizard',
	'attack' : True,
	'element' : Element.Ice,
	'price' : 2500,
	'mp-cost' : 3,
	'level' : 1,
	'does-dmg' : True,
	'descr' : 'Adds Ice element to your next attack. Chance to inflict Freeze',
	'inflict' : (StatusEffect.Freeze, 30, 4) # effect, chance, effective turns
},
{
	'name' : 'Gust',
	'attack' : True,
	'element' : Element.Wind,
	'price' : 3000,
	'mp-cost' : 4,
	'level' : 1,
	'does-dmg' : True,
	'descr' : 'Adds Wind element to your next attack.'
},
{
	'name' : 'Rockslide',
	'attack' : True,
	'element' : Element.Earth,
	'price' : 3000,
	'mp-cost' : 4,
	'level' : 2,
	'does-dmg' : True,
	'descr' : 'Adds Earth element to your next attack.'
},
{
	'name' : 'Light Beam',
	'attack' : True,
	'element' : Element.Light,
	'price' : 9000,
	'mp-cost' : 6,
	'level' : 3,
	'does-dmg' : True,
	'descr' : 'Adds Light element to your next attack.'
},
{
	'name' : 'Void Ray',
	'attack' : True,
	'element' : Element.Void,
	'price' : 9500,
	'mp-cost' : 6,
	'level' : 3,
	'does-dmg' : True,
	'descr' : 'Adds Void element to your next attack.'
},
{
	'name' : 'Poison',
	'attack' : False,
	'element' : Element.None_,
	'price' : 1900,
	'mp-cost' : 5,
	'level' : 1,
	'does-dmg' : False,
	'descr' : 'Poisons the enemy, doing chip damage every turn.',
	'inflict' : (StatusEffect.Poison, 100, 6) # effect, chance, effective turns
},
{
	'name' : 'Sleep',
	'attack' : False,
	'element' : Element.None_,
	'price' : 2900,
	'mp-cost' : 7,
	'level' : 2,
	'does-dmg' : False,
	'descr' : '70% Chance to put the enemy to Sleep.',
	'inflict' : (StatusEffect.Sleep, 70, 3) # effect, chance, effective turns
},
{
	'name' : 'Confusion',
	'attack' : False,
	'element' : Element.None_,
	'price' : 3400,
	'mp-cost' : 8,
	'level' : 3,
	'does-dmg' : False,
	'descr' : '70% Chance to Confuse the enemy.',
	'inflict' : (StatusEffect.Confusion, 70, 3) # effect, chance, effective turns
},
{
	'name' : 'Weaken',
	'attack' : False,
	'element' : Element.Void,
	'price' : 4800,
	'mp-cost' : 10,
	'level' : 4,
	'does-dmg' : False,
	'descr' : '70% Chance to inflict enemy with Weak (takes 2X damage).',
	'inflict' : (StatusEffect.Weaken, 70, 4) # effect, chance, effective turns
},
{
	'name' : 'Protect',
	'attack' : False,
	'element' : Element.Light,
	'price' : 5200,
	'mp-cost' : 10,
	'level' : 4,
	'does-dmg' : False,
	'descr' : '70% Chance to put a protecting shield over you (-50\% damage).',
	'self-inflict' : (StatusEffect.Protect, 100, 4) # effect, chance, effective turns
},
{
	'name' : 'Silence',
	'attack' : False,
	'element' : Element.Light,
	'price' : 6400,
	'mp-cost' : 12,
	'level' : 5,
	'does-dmg' : False,
	'descr' : '70% Chance to inflict enemy with Silence (inability to cast magic).',
	'inflict' : (StatusEffect.Silence, 70, 4) # effect, chance, effective turns
},
{
	'name' : 'Dispell',
	'attack' : False,
	'element' : Element.Light,
	'price' : 6500,
	'mp-cost' : 12,
	'level' : 5,
	'does-dmg' : False,
	'descr' : 'Removes current status effect.',
	'dispell' : True
}


]