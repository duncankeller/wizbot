import json
import random
import copy
from enum import Enum

class Element(str, Enum):
	None_ = 'None',
	Fire = 'Fire',
	Water = 'Water',
	Ice = 'Ice',
	Electric = 'Electric',
	Earth = 'Earth',
	Wind = 'Wind',
	Void = 'Void',
	Light = 'Light'

class StatusEffect(str, Enum):
	None_ = 'None',
	Burn = 'Burn',
	Poison = 'Poison',
	Sleep = 'Sleep',
	Paralisis = 'Paralisis',
	Confusion = 'Confusion',
	Protect = 'Protect',
	Weaken = 'Weaken',
	Freeze = 'Freeze',
	Silence = 'Silence'

class PlayerPersonalitiy(str, Enum):
	Balanced = 'Balanced',
	Agressive = 'Agressive',
	Cautious = 'Cautious',
	Devious = 'Devious',
	Conservative = 'Conservative'

def requiredExp(lvl):
	return 8 + (lvl * 3)

class Enemy():
	name = 'Kobold'
	hp = 10
	mp = 0

	attacks = []


elementToIcon = {
	Element.None_ : '',
	Element.Fire : '🔥',
	Element.Water : '💦',
	Element.Ice : '🧊',
	Element.Electric : '⚡',
	Element.Earth : '🌎',
	Element.Wind : '💨',
	Element.Void : '🌌',
	Element.Light : '🎇'
}

elementStr = {
	Element.None_ : 'None',
	Element.Fire : 'Fire',
	Element.Water : 'Water',
	Element.Ice : 'Ice',
	Element.Electric : 'Electric',
	Element.Earth : 'Earth',
	Element.Wind : 'Wind',
	Element.Void : 'Void',
	Element.Light : 'Light'
}

statusEffectToIcon = {
	StatusEffect.None_ : '',
	StatusEffect.Burn : '🥵',
	StatusEffect.Poison : '🤢',
	StatusEffect.Sleep : '😴',
	StatusEffect.Paralisis : '🤐',
	StatusEffect.Confusion : '😵',
	StatusEffect.Protect : '👷',
	StatusEffect.Weaken : '😰',
	StatusEffect.Freeze : '🥶',
	StatusEffect.Silence : '🤐'
}

statusEffectPastTense = {
	StatusEffect.None_ : '',
	StatusEffect.Burn : 'Burned',
	StatusEffect.Poison : 'Poisoned',
	StatusEffect.Sleep : 'put to Sleep',
	StatusEffect.Paralisis : 'Paralized',
	StatusEffect.Confusion : 'Confused',
	StatusEffect.Protect : 'Protected by a magic force',
	StatusEffect.Weaken : 'Weakened',
	StatusEffect.Freeze : 'Frozen',
	StatusEffect.Silence : 'Silenced'
}

statusEffectStr = {
	StatusEffect.None_ : 'None',
	StatusEffect.Burn : 'Burn',
	StatusEffect.Poison : 'Poison',
	StatusEffect.Sleep : 'Sleep',
	StatusEffect.Paralisis : 'Paralisis',
	StatusEffect.Confusion : 'Confusion',
	StatusEffect.Protect : 'Protect',
	StatusEffect.Weaken : 'Weaken',
	StatusEffect.Freeze : 'Freeze',
	StatusEffect.Silence : 'Silence'
}

adventurerCardTemplate = {
	'name' : 'username',
	'hp' : 10,
	'maxHp' : 10,
	'mp' : 10,
	'maxMp' : 10,
	'exp' : 0,
	'lvl' : 0,
	'wand'  : {},
	'cloak' : {},
	'spells' : [],
	'item' : {},
	'inv' : [],
	'personality' : PlayerPersonalitiy.Balanced,

	'guildID' : '',
	'status' : StatusEffect.None_,
	'status-time' : 0,
	'coin' : 0,

	'seen-enemies' : [],
	'completed-dungeons' : [],
	'wizgems' : 0


}

def refreshAdventureCard(card):
	card['status'] = StatusEffect.None_
	card['status-time'] = 0
	return card

def healAdventureCard(card):
	card['hp'] = card['maxHp']
	card['mp'] = card['maxMp']
	return card
