from bs4 import BeautifulSoup
import urllib.request
import random
import json

trinketMap = {}

url = "https://shenmue.fandom.com/wiki/Shenmue/Collection_Datasheets"
file = urllib.request.urlopen(url)
soup = BeautifulSoup(file, 'html.parser')

results = soup.find_all('tr')

for result in results:
	img_soup = BeautifulSoup(str(result), 'html.parser')
	img_results = img_soup.find_all('a', href=True)


	img = ''
	for result in img_results:
		img = result['href']
		break


	name_results = img_soup.find_all('td')

	name = ''
	for result in name_results:
		if result.get_text() != '\n':
			name = result.get_text().replace('\n', '')
			break
		

	if img != '' and  name != '':
		entry = {}
		entry['img'] = img
		rarity = random.randrange(0, 100)
		if rarity < 5:
			rarity = 0
		elif rarity < 21:
			rarity = 1
		else:
			rarity = 2
		entry['rarity'] = rarity
		trinketMap[name] = entry

with open('entries.json', 'w') as file:
	json.dump(trinketMap, file)