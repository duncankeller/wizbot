
#https://discord.com/api/oauth2/authorize?client_id=903504658201452556&permissions=326417579072&scope=bot

import random
import datetime
import json
import os
import discord
import randfacts
import shutil
import copy
import sys

import igdbReader
from common import *
from nameGenerator import *
from trinketStocks import *
from dungeonExploration import *

from discord.ext import commands, tasks

didInit = False

token = 'NOTREAD'
with open('../bottoken.text', 'r') as file:
    token = file.read().replace('\n', '')

# constants
wizstateMapFilepath = 'wizstateMapData.json'
wizstateMapFilepathTmp = 'wizstateMapData-tmp.json'

state_guessingGame = 'state_guessingGame'
gamestate_notStarted = 'gamestate_notStarted'
gamestate_waiting = 'gamestate_waiting'
class state_guessingGame:
	state = gamestate_notStarted
	correctNum = 0

state_wizcoinCount = 'state_wizcoinCount'
state_wizcoinLastAward = 'state_wizcoinLastAward'
state_wizcoinStartedMiningTime = 'state_wizcoinStartedMiningTime'
state_wizcoinMiningAmountInPorgress = 'state_wizcoinMiningAmountInPorgress'
wizcoinSecondsToMinePerCoin = 1020
wizcoinGoblinMiningReductionSeconds = 0.01
state_wizcoinMiningState = 'state_wizcoinMiningState'
state_wizcoinMiningState_notStarted = 'state_wizcoinMiningState_notStarted'
state_wizcoinMiningState_waitingForAnswer = 'state_wizcoinMiningState_waitingForAnswer'
state_wizcoinMiningState_waitingForMining = 'state_wizcoinMiningState_waitingForMining'
state_wizcoinMiningState_waitingForGoblins = 'state_wizcoinMiningState_waitingForGoblins'
data_wicoinMiningGoblins = 'data_wicoinMiningGoblins'

# artifacts
data_artifacts = 'data_artifacts'
artifact_trinketKey = 'Trinket Key'

data_randomGameBookmarks = 'data_randomGameBookmarks'

# trinket buying state
state_buyingTrinket = 'state_buyingTrinket'
state_buyingTrinket_notStarted = 'state_buyingTrinket_notStarted'
state_buyingTrinket_waiting = 'state_buyingTrinket_waiting'
data_trinkets = 'data_trinkets'
minCapsuleToyCost = 20
maxCapsuleToyCost = 1000

data_aliasMap = 'data_aliasMap'
state_addingAlias = 'state_addingAlias'
state_addingAliasName = 'state_addingAliasName'

# auction state
data_auctionHouseItems = 'data_auctionHouseItems'
state_auctioningTrinket = 'state_auctioningTrinket'
state_auctioningTrinket_notStarted = 'state_auctioningTrinket_notStarted'
state_auctioningTrinket_selecting = 'state_auctioningTrinket_selecting'
state_auctioningTrinketName = 'state_auctioningTrinketName'
data_auctionItemUsername = 'data_auctionItemUsername'
data_auctionItemTrinketName = 'data_auctionItemTrinketName'
data_auctionItemCost = 'data_auctionItemCost'

data_gameCollection = 'data_gameCollection'

lastRandomGame = {}

# TODO: delete this after merging in the adventurer card changes
data_adventure = 'data_adventure'
data_adventureCard = 'data_adventureCard'
data_completedDungeons = 'data_completedDungeons'

# prefs
prefsIdxMap = []
prefsDescrMap = {}
currPref = 0
def addPref(prefName, prefDescr):
	global currPref
	prefsIdxMap.append(prefName)
	prefsDescrMap[currPref] = prefDescr
	currPref += 1
data_prefs = 'data_prefs'
prefs_imgIdgbOnly = 'prefs_imgIdgbOnly'
addPref(prefs_imgIdgbOnly, 'If set to true, \'random game\' command will only return results that contain screenshots')
prefs_idgbBlockMobile = 'prefs_idgbBlockMobile'
addPref(prefs_idgbBlockMobile, 'If set to true, \'random game\' command will not return mobile games')

# bot setup and loading previous server state
intents = discord.Intents.default()
intents.members = True
client = discord.Client(intents=intents)

stateMap = {}
def loadStatemap():
	global stateMap

	if os.path.isfile(wizstateMapFilepath):
		with open(wizstateMapFilepath) as file:
			print('correctly opened and read previous bot state')
			inMap = json.load(file)
			stateMap = inMap['stateMap']


def saveStateBackup():
	global stateMap

	if not os.path.exists('backup'):
		os.makedirs('backup')

	i = 0
	while os.path.exists('backup/wizStateBackup_%s.json' % i):
		i += 1

	with open('backup/wizStateBackup_%s.json' % i, 'w') as file:
		outState = {}
		outState['stateMap'] = stateMap
		json.dump(outState, file)

try:
	loadStatemap()
except:
	print('fatal error: failed to read state map')


# save hourly backups
@tasks.loop(hours=1)
async def saveStateBackupTask():
	try:
		saveStateBackup()
	except:
		print('warning: failed to save backup')
saveStateBackupTask.start()

def doWriteGamestop():
	filepath = 'gamestopSelection_0.json'
	selection = igdbReader.getGameSetData()

	#debug
	for platKey in selection:
			for game in selection[platKey]:
				if game['name'] == 'NHL Stanley Cup':
					print('FUCK tried to write out bad data!! from writeOutGamestopSelection')
					return

	with open(filepath, 'w') as file:
		json.dump(selection, file)
		print('wrote out gamestop data, done')

@tasks.loop(hours=1)
async def writeGamestopSelection():
	if datetime.datetime.now().hour == 0:
		print('Handling daily tasks...')
		updateMarketForTheDay()
		doWriteGamestop()
		print('Done handling daily tasks.')
		
writeGamestopSelection.start()


# util funcs
def registerUser(guild, name):
	stateMap[str(guild.id)][name] = {}
	stateMap[str(guild.id)][name][state_wizcoinCount] = 0
	stateMap[str(guild.id)][name][state_wizcoinLastAward] = 0

def resetState(message, author):
	stateMap[str(message.guild.id)][author][state_wizcoinMiningState] = state_wizcoinMiningState_notStarted
	stateMap[str(message.guild.id)][author][state_auctioningTrinket] = state_auctioningTrinket_notStarted
	stateMap[str(message.guild.id)][author][state_buyingTrinket] = state_buyingTrinket_notStarted
	stateMap[str(message.guild.id)][author][state_guessingGame] = gamestate_notStarted

async def sendMsgToThread(thread, msg):
	await thread.edit(content=msg)

def init(guild):
	if str(guild.id) not in stateMap:
		stateMap[str(guild.id)] = {}

	if data_aliasMap not in stateMap[str(guild.id)]:
		stateMap[str(guild.id)][data_aliasMap] = {}
		stateMap[str(guild.id)][data_aliasMap]['calvin'] = 'TheCalz0ne'
		stateMap[str(guild.id)][data_aliasMap]['nate'] = 'Nato'
		stateMap[str(guild.id)][data_aliasMap]['Darth Damaged'] = 'Nato'
		stateMap[str(guild.id)][data_aliasMap]['nadeem'] = 'deemy'
		stateMap[str(guild.id)][data_aliasMap]['kmo'] = 'kymorr'
		stateMap[str(guild.id)][data_aliasMap]['kyle'] = 'kymorr'

	if data_auctionHouseItems not in stateMap[str(guild.id)]:
		stateMap[str(guild.id)][data_auctionHouseItems] = {}

	for member in guild.members:
		if member.name not in stateMap[str(guild.id)]:
			registerUser(guild, member.name)

		# for new features, have to init those entries into the statemap
		if state_wizcoinStartedMiningTime not in stateMap[str(guild.id)][member.name]:
			stateMap[str(guild.id)][member.name][state_wizcoinStartedMiningTime] = 0
		if state_wizcoinMiningAmountInPorgress not in stateMap[str(guild.id)][member.name]:
			stateMap[str(guild.id)][member.name][state_wizcoinMiningAmountInPorgress] = 0
		if state_wizcoinMiningState not in stateMap[str(guild.id)][member.name]:
			stateMap[str(guild.id)][member.name][state_wizcoinMiningState] = state_wizcoinMiningState_notStarted
		if state_buyingTrinket not in stateMap[str(guild.id)][member.name]:
			stateMap[str(guild.id)][member.name][state_buyingTrinket] = state_buyingTrinket_notStarted
		if data_trinkets not in stateMap[str(guild.id)][member.name]:
			stateMap[str(guild.id)][member.name][data_trinkets] = {}
		if state_auctioningTrinket not in stateMap[str(guild.id)][member.name]:
			stateMap[str(guild.id)][member.name][state_auctioningTrinket] = state_auctioningTrinket_notStarted
		if state_auctioningTrinketName not in stateMap[str(guild.id)][member.name]:
			stateMap[str(guild.id)][member.name][state_auctioningTrinketName] = ''
		if data_gameCollection not in stateMap[str(guild.id)][member.name]:
			stateMap[str(guild.id)][member.name][data_gameCollection] = {}
		if data_wicoinMiningGoblins not in stateMap[str(guild.id)][member.name]:
			stateMap[str(guild.id)][member.name][data_wicoinMiningGoblins] = 0
		if data_randomGameBookmarks not in stateMap[str(guild.id)][member.name]:
			stateMap[str(guild.id)][member.name][data_randomGameBookmarks] = []
		if data_artifacts not in stateMap[str(guild.id)][member.name]:
			stateMap[str(guild.id)][member.name][data_artifacts] = []


		if data_adventure not in stateMap[str(guild.id)][member.name]:
			stateMap[str(guild.id)][member.name][data_adventure] = {}
		if data_completedDungeons not in stateMap[str(guild.id)][member.name][data_adventure]:
			stateMap[str(guild.id)][member.name][data_adventure][data_completedDungeons] = []
		if data_adventureCard not in stateMap[str(guild.id)][member.name][data_adventure]:
			card = copy.deepcopy(adventurerCardTemplate)
			card['name'] = member.name
			card['guildID'] = str(guild.id)
			stateMap[str(guild.id)][member.name][data_adventure][data_adventureCard] = card

		if data_prefs not in stateMap[str(guild.id)][member.name]:
			stateMap[str(guild.id)][member.name][data_prefs] = {}
		if prefs_imgIdgbOnly not in stateMap[str(guild.id)][member.name][data_prefs]:
			stateMap[str(guild.id)][member.name][data_prefs][prefs_imgIdgbOnly] = False 
		if prefs_idgbBlockMobile not in stateMap[str(guild.id)][member.name][data_prefs]:
			stateMap[str(guild.id)][member.name][data_prefs][prefs_idgbBlockMobile] = False 
		

def getNameFromAlias(message, alias):
	aliasLower = alias.lower();
	aliasMap = stateMap[str(message.guild.id)][data_aliasMap]

	if alias in aliasMap:
		return aliasMap[alias];
	if aliasLower in aliasMap:
		return aliasMap[aliasLower];
	if alias in stateMap[str(message.guild.id)]:
		return alias;
	return 'ALIAS NOT FOUND'

def readibleTimeFromStamp(sec):
	mins, secs = divmod(sec, 60) 
	hours, mins = divmod(mins, 60)
	days, hours = divmod(hours, 24)
	
	days = int(days)
	hours = int(hours)
	mins = int(mins)

	finalMsg = ''
	if days > 0:
		finalMsg += str(days) + ' Days, '
	if hours > 0:
		finalMsg += str(hours) + ' Hours, '
	finalMsg += str(mins) + ' Minutes'

	return finalMsg

gamestopFilepath = 'gamestopSelection_0.json'
def fetchGamestopSelection():
	try:
		with open(gamestopFilepath) as file:
			print('correctly opened and read gamestopSelection')
			selection = json.load(file)

			#debug
			for platKey in selection:
					for game in selection[platKey]:
						if game['name'] == 'NHL Stanley Cup':
							print('FUCK tried to read in bad data!! from fetchGamestopSelection ')
							

			return selection
	except Exception as e:
		print("ERROR trying to fetch gamestop selection")
		print(e)
		return {}

def writeOutGamestopSelection(selection):

	#debug
	for platKey in selection:
			for game in selection[platKey]:
				if game['name'] == 'NHL Stanley Cup':
					print('FUCK tried to write out bad data!! from writeOutGamestopSelection')
					return


	with open(gamestopFilepath, 'w') as file:
		json.dump(selection, file)
		print('wrote out updated gamestop selection map')

# commands
commands = []
async def command_commands(message, author, msgString):
	#await message.channel.send("Apologies, master. Due to a disturbance I am unable to list my commands for a while")
	#return

	msg = 'I live to serve, master ' + author + '. To summon me, type \"wizbot, \" followed by one of the commands I know:\n'
	msg += '```'
	for cmd in commands:
		#msg += "\"" + cmd['name'] + "\" : " + cmd['description'] + "\n"
		msg += "\"" + cmd['name'] + "\"\n"
	msg += '```'
	await message.channel.send(msg)
commands.append({
	'name' : 'commands',
	'description' : 'List all comands that Wizbot knows (except for the secret commands!)',
	'func' : command_commands})
# hail
async def command_hail(message, author, msgString):
	await message.channel.send('Hello master ' + message.author.name + ', I feel strong magicks brewing..')
commands.append({
	'name' : 'hail',
	'description' : 'Hail Wizbot',
	'func' : command_hail})
# wizcoin award
async def command_wizcoinAward(message, author, msgString):
	awardedUser = msgString.split("wizbot, award ", 1)[1] 
	userID = getNameFromAlias(message, awardedUser)

	if userID == 'ALIAS NOT FOUND' or userID not in stateMap[str(message.guild.id)]:
		await message.channel.send('My deepest apologies, ' + author + ', I\'m not familiar with that particular Wizard. Perhaps you\'ve type\'d it wrongly?')
	else:
		if userID == author:
			stateMap[str(message.guild.id)][userID][state_wizcoinCount] = stateMap[str(message.guild.id)][userID][state_wizcoinCount] - 5
			await message.channel.send('What tomfoolery is this?? I\'m afriad I\'ll have to deduct 5 Wizcoin from you for attempting this trickery. Please think about your actions.')
		else:
			secondsSinceEpoch = datetime.datetime.now().timestamp()
			timeSinceLastAward = secondsSinceEpoch - stateMap[str(message.guild.id)][author][state_wizcoinLastAward]
			secondsBetweenAwards = 3600

			if timeSinceLastAward >= secondsBetweenAwards:
				stateMap[str(message.guild.id)][author][state_wizcoinLastAward] = secondsSinceEpoch;
				stateMap[str(message.guild.id)][userID][state_wizcoinCount] = stateMap[str(message.guild.id)][userID][state_wizcoinCount] + 1
				if userID == 'wizbot':
					await message.channel.send('This is... an honor of incredible magnitude. I thank ye, master ' + author + ', from the bottomest of my heart.')
				else:
					await message.channel.send('Behold! ' + userID + ' has on this day been granted a shining Wizcoin, for a total of ' + str(stateMap[str(message.guild.id)][userID][state_wizcoinCount]) + '! Splendid.')
			else:
				remainingTime = (secondsBetweenAwards - timeSinceLastAward) / 60;
				remainingTimeStr = "{0:.4g}".format(remainingTime)
				await message.channel.send('Is your wallet so deep, master ' + author + ', that you can give Wizcoin out so freely? I think not. Perhaps you should wait another ' + remainingTimeStr + ' minutes and try again?')
commands.append({
	'name' : 'award',
	'description' : 'Give the precious gift of Wizcoin to a guild member. Write their name after \"award \"',
	'func' : command_wizcoinAward})
# wizcoin check
async def command_wizcoinMyCoin(message, author, msgString):
	await message.channel.send('You have ' + str(stateMap[str(message.guild.id)][author][state_wizcoinCount]) + ' Wizcoin deposited into your Merlin Credit Union account. Don\'t spend it all in one place!')
commands.append({
	'name' : 'my wizcoin',
	'description' : 'See how much Wizcoin you currently own',
	'func' : command_wizcoinMyCoin})
async def command_wizcoinStandings(message, author, msgString):
	wizcoinAmnts = {}
	for user in stateMap[str(message.guild.id)]:
		#stupidly have to check if state_wizcoinCount exists here because this map holds all state, not just users
		#ideally we have a sub map called users inside of the state map for the guild
		if state_wizcoinCount in stateMap[str(message.guild.id)][user]:
			wizcoinAmnts[user] = stateMap[str(message.guild.id)][user][state_wizcoinCount]
	sortedCoinList = sorted(wizcoinAmnts.items(), key=lambda x: x[1], reverse=True)
	newMsg = 'Here are the current standings: \n'
	for entry in sortedCoinList: 
		newMsg += str(entry[0]) + ': ' + '{:.3f}'.format(float(entry[1])) + 'μ\n'
	await message.channel.send(newMsg)
commands.append({
	'name' : 'wizcoin standings',
	'description' : 'See how much Wizcoin everybody in the guild has. Who is the Top Wizard?',
	'func' : command_wizcoinStandings})	
# wizcoin mining
async def command_wizcoinMining(message, author, msgString):
	totalTimeToMinePerCoin = wizcoinSecondsToMinePerCoin
	numGoblins = stateMap[str(message.guild.id)][author][data_wicoinMiningGoblins]
	totalTimeToMinePerCoin -= wizcoinGoblinMiningReductionSeconds * numGoblins
	totalTimeToMinePerCoin /= 60

	if totalTimeToMinePerCoin < 0.001:
		totalTimeToMinePerCoin = 0.001

	#delete this
	totalTimeToMinePerCoin = wizcoinSecondsToMinePerCoin

	if stateMap[str(message.guild.id)][author][state_wizcoinMiningState] == state_wizcoinMiningState_notStarted:
		stateMap[str(message.guild.id)][author][state_wizcoinMiningState] = state_wizcoinMiningState_waitingForAnswer

		if numGoblins > 0:
			await message.channel.send('I\'ll get that started for you, master ' + author + '. Wizcoin takes ' + str(wizcoinSecondsToMinePerCoin / 60) + ' minutes to mine per coin, but with your '+str(numGoblins)+' Goblins, it\'ll only take '+str(totalTimeToMinePerCoin)+'. How much would you like to mine, sire?')
		else:
			await message.channel.send('I\'ll get that started for you, master ' + author + '. Wizcoin takes ' + str(wizcoinSecondsToMinePerCoin / 60) + ' minutes to mine per coin. How much would you like to mine, sire?')
	elif stateMap[str(message.guild.id)][author][state_wizcoinMiningState] == state_wizcoinMiningState_waitingForAnswer:
		await message.channel.send('Of course, master ' + author + '. Just tell me how much by saying \'wizbot, <number>\'')
	elif stateMap[str(message.guild.id)][author][state_wizcoinMiningState] == state_wizcoinMiningState_waitingForMining:
		secondsSinceEpoch = datetime.datetime.now().timestamp()
		timeMining = secondsSinceEpoch - stateMap[str(message.guild.id)][author][state_wizcoinStartedMiningTime]
		totalTime = totalTimeToMinePerCoin * stateMap[str(message.guild.id)][author][state_wizcoinMiningAmountInPorgress]
		if timeMining < totalTime:
			timeLeft = totalTime - timeMining
			timeReadible = readibleTimeFromStamp(timeLeft)
			await message.channel.send('My apologies, master ' + author + ', I am already in the process of mining ' + str(stateMap[str(message.guild.id)][author][state_wizcoinMiningAmountInPorgress]) + ' Wizcoin for you, which will be completed in: ' + timeReadible + '. Use command \'wizbot, abort mining\' to cancel.')
		else:
			await message.channel.send('I already have ' + str(stateMap[str(message.guild.id)][author][state_wizcoinMiningAmountInPorgress]) + ' Wizcoin mined and ready for you to collect. Use command \'wizbot, collect wizcoin\'') 
commands.append({
	'name' : 'mine wizcoin',
	'description' : 'Head to the ancient mines and mine that which is most prized, Wizcoin',
	'func' : command_wizcoinMining})
async def command_hireGoblins(message, author, msgString):
	return
	resetState(message, author)

	numGoblins = stateMap[str(message.guild.id)][author][data_wicoinMiningGoblins]
	if stateMap[str(message.guild.id)][author][state_wizcoinCount] < 1:
		await message.channel.send('Terribly sorry, master ' + author + ', you can\'t afford to hire any Goblins.')
		return
	stateMap[str(message.guild.id)][author][state_wizcoinMiningState] = state_wizcoinMiningState_waitingForGoblins
	await message.channel.send('Certainly, I\'ll call them out from their den. Every Goblin under your care reduces the time to mine a Wizcoin by ' + str(wizcoinGoblinMiningReductionSeconds) + ' seconds and costs 1 Wizcoin to hire. You currently have ' + str(numGoblins) + ' working for you. How many more would you like to hire?')

commands.append({
	'name' : 'hire goblins',
	'description' : 'Mine Wizcoin faster, with the help of Goblin Contractors',
	'func' : command_hireGoblins})
async def command_wizcoinMiningAbort(message, author, msgString):
	if stateMap[str(message.guild.id)][author][state_wizcoinMiningState] == state_wizcoinMiningState_waitingForMining:
		stateMap[str(message.guild.id)][author][state_wizcoinMiningState] = state_wizcoinMiningState_notStarted
		stateMap[str(message.guild.id)][author][state_wizcoinMiningAmountInPorgress] = 0
		await message.channel.send('As you wish, master ' + author + ', I will call the Goblin Miners back to their dens.')
	else:
		await message.channel.send('I\'m not aware of any mining expeditions to abort, master ' + author + '.')
commands.append({
	'name' : 'abort mining',
	'description' : 'Abort a previously-started job to mine Wizcoin.',
	'func' : command_wizcoinMiningAbort})
async def command_wizcoinMiningCollect(message, author, msgString):
	if stateMap[str(message.guild.id)][author][state_wizcoinMiningState] == state_wizcoinMiningState_waitingForMining:
		secondsSinceEpoch = datetime.datetime.now().timestamp()
		timeMining = secondsSinceEpoch - stateMap[str(message.guild.id)][author][state_wizcoinStartedMiningTime]
		


		totalTime = wizcoinSecondsToMinePerCoin * stateMap[str(message.guild.id)][author][state_wizcoinMiningAmountInPorgress]
		if timeMining < totalTime:
			timeLeft = totalTime - timeMining
			timeReadible = readibleTimeFromStamp(timeLeft)
			await message.channel.send('My apologies, master ' + author + ', Your Wizcoin is still being mined. ' + str(stateMap[str(message.guild.id)][author][state_wizcoinMiningAmountInPorgress]) + ' Wizcoin is being mined, which will be completed in: ' + timeReadible + '. Use command \'wizbot, abort mining\' to cancel.')
		else:
			amountGot = stateMap[str(message.guild.id)][author][state_wizcoinMiningAmountInPorgress]
			stateMap[str(message.guild.id)][author][state_wizcoinCount] += amountGot
			stateMap[str(message.guild.id)][author][state_wizcoinMiningAmountInPorgress] = 0
			stateMap[str(message.guild.id)][author][state_wizcoinMiningState] = state_wizcoinMiningState_notStarted
			readibleTime = readibleTimeFromStamp(wizcoinSecondsToMinePerCoin * amountGot)

			embed = discord.Embed(title="Wizcoin") 
			embed.set_image(url='https://c.tenor.com/pPYpISB14vwAAAAM/coin.gif')
			coal = amountGot * 7.2
			newMsg = 'Glorious! ' + str(amountGot) + ' sparkling Wizcoin have been added to your account at the Merlin Credit Union, master ' + author + '.\n It took ' + readibleTime + ' to mine, expending ' + str(coal) + ' tonnes of coal.'
			await message.channel.send(content=newMsg, embed=embed)
	else:
		await message.channel.send('I\'m not aware of any mining expeditions in progres, master ' + author + '.')
	
commands.append({
	'name' : 'collect wizcoin',
	'description' : 'Obtain Wizcoin generated after the completion of a Wizcoin mining job.',
	'func' : command_wizcoinMiningCollect})
# fact
async def command_randomFact(message, author, msgString):
	newMsg = 'Certainly, master ' + author + '. Did you know: ' + randfacts.get_fact()
	await message.channel.send(newMsg)
commands.append({
	'name' : 'tell me a fact',
	'description' : 'I posess a deep pool of knowledge. Would you like to know a fun fact?',
	'func' : command_randomFact})	
# guessing game
async def command_guessingGame(message, author, msgString):
	stateMap[str(message.guild.id)][author][state_guessingGame] = state_guessingGame()
	await message.channel.send('I\'m thinking of a number between 1 and 5, master ' + author + '. Please respond with \'wizbot, <number>\'')
	stateMap[str(message.guild.id)][author][state_guessingGame].state = gamestate_waiting
	stateMap[str(message.guild.id)][author][state_guessingGame].correctNum = random.randrange(1, 5)
commands.append({
	'name' : 'i\'m feeling lucky',
	'description' : 'Would you like to play a stimulating game of logic and wit?',
	'func' : command_guessingGame})	
# aliases
async def command_addAlias(message, author, msgString):
	personToAlias = msgString.split("wizbot, alias ", 1)[1] 
	personToAlias = getNameFromAlias(message, personToAlias)
	if personToAlias in stateMap[str(message.guild.id)]:
		await message.channel.send('What alias would you like to add for master ' + personToAlias + '?')
		stateMap[str(message.guild.id)][author][state_addingAlias] = True
		stateMap[str(message.guild.id)][author][state_addingAliasName] = personToAlias
	else:
		await message.channel.send('My deepest apologies, ' + author + ', I\'m not familiar with that particular Wizard. Perhaps you\'ve type\'d it wrongly?')
		print('tried to find [' + personToAlias + ']  [' + msgString.split("wizbot, alias ", 1)[1]  + ']')
commands.append({
	'name' : 'alias',
	'description' : 'I can recognize as many nicknames as you like.',
	'func' : command_addAlias})	
# igdb
async def command_randGameFromYear(message, author, msgString):
	global lastRandomGame

	gameYear = msgString.split("wizbot, random game from ", 1)[1] 
	if gameYear.isnumeric():
		requireScreenshot = stateMap[str(message.guild.id)][author][data_prefs][prefs_imgIdgbOnly]
		blockMobile = stateMap[str(message.guild.id)][author][data_prefs][prefs_idgbBlockMobile]
		gameMsg = igdbReader.getGameData(int(gameYear), requireScreenshot, blockMobile)
		lastRandomGame = copy.deepcopy(gameMsg)

		if 'screenshot' in gameMsg:
			embed = discord.Embed(title="Screenshot") 
			embed.set_image(url=gameMsg['screenshot'])
			await message.channel.send(content=gameMsg['gameMessage'], embed=embed)
		else:
			await message.channel.send(content=gameMsg['gameMessage'])
	else:
		await message.channel.send('My deepest apologies, I wasn\'t able to parse that year')
commands.append({
	'name' : 'random game ',
	'description' : 'Tell me a random year and I shall search the archives for a game from that year. Format=\'random game from <year>\'',
	'func' : command_randGameFromYear})	

async def bookmark_random_game(message, author, msgString):
	global lastRandomGame

	if lastRandomGame != {}:
		stateMap[str(message.guild.id)][author][data_randomGameBookmarks].append(copy.deepcopy(lastRandomGame))
		await message.channel.send('As you wish, master ' + author + '. Use \"wizbot, view game bookmarks\" to see your bookmarks.')
	else:
		await message.channel.send('I don\'t believe I\'ve pulled a game recently. Try \"wizbot, random game from <year>\"')
commands.append({
	'name' : 'bookmark that',
	'description' : 'Enjoying the Random Game feature and want to remember something for later? Try a bookmark. Works on the last pulled random game.',
	'func' : bookmark_random_game})	
async def view_game_bookmarks(message, author, msgString):
	if len(stateMap[str(message.guild.id)][author][data_randomGameBookmarks]) > 0:
		msg = '```'
		print(stateMap[str(message.guild.id)][author][data_randomGameBookmarks])
		for entry in stateMap[str(message.guild.id)][author][data_randomGameBookmarks]:
			msg += entry['name'] + '\n'
		msg += '```'
		await message.channel.send(msg)
	else:
		await message.channel.send('You don\'t have any bookmarks yet! Use \"wizbot, bookmark that\" to add a bookmark.')
commands.append({
	'name' : 'view game bookmarks',
	'description' : 'View all your bookmarks of random games.',
	'func' : view_game_bookmarks})	
# trinkets 
async def command_buyTrinket(message, author, msgString):
	stateMap[str(message.guild.id)][author][state_buyingTrinket] = state_buyingTrinket_waiting
	newMsg = 'Certainly, master ' + author + '. I\'ll warm up the Capsule Toy Machine.\n'
	newMsg += '```If you insert Wizcoin, you will get a randomly selected trinket out of a possible pool of ' + str(len(trinketMap)) + ', with a rarity of 1 ('+rarityMap[2]+') to 3 ('+rarityMap[0]+').\n'
	newMsg += 'The minimum cost to buy-in is ' + str(minCapsuleToyCost) + ' Wizcoin, which gives you a purely random selection.\n'
	newMsg += 'You may insert more Wizcoin to increase the chance of a rare trinket, up to ' + str(maxCapsuleToyCost) + ', which garuntees a maxium rarity trinket.\n'
	newMsg += '\'wizbot, <number>\' to buy a trinket, or \'wizbot, cancel\' to change your mind.\n'
	newMsg += 'Full Trinket list at: https://shenmue.fandom.com/wiki/Shenmue/Collection_Datasheets ```\n'
	newMsg += 'How much Wizcoin would you like to spend on a trinket?'
	await message.channel.send(newMsg)
commands.append({
	'name' : 'buy trinket',
	'description' : 'Spend your Wizcoin on one of my prized trinkets from the Capsule Toy Machine',
	'func' : command_buyTrinket})	
async def command_viewFullTrinkets(message, author, msgString):
	if len(stateMap[str(message.guild.id)][author][data_trinkets]) > 0:

		ownedTrinks = 0
		for trinketKey in stateMap[str(message.guild.id)][author][data_trinkets]:
			if stateMap[str(message.guild.id)][author][data_trinkets][trinketKey] > 0:
				ownedTrinks += 1
				break
		if ownedTrinks == 0:
			await message.channel.send('You don\'t have any Trinkets, master ' + author + '. Spend some Wizcoin and \"Collect Every One!\"' )
			return

		totalTrinkets = 0
		uniqueTrinkets = 0
		rarity0 = 0
		rarity1 = 0
		rarity2 = 0
		for trinketKey in stateMap[str(message.guild.id)][author][data_trinkets]:
			if stateMap[str(message.guild.id)][author][data_trinkets][trinketKey] > 0:
				totalTrinkets += stateMap[str(message.guild.id)][author][data_trinkets][trinketKey]
				uniqueTrinkets += 1
				rarity = trinketMap[trinketKey]['rarity']
				if rarity == 0:
					rarity0 += 1
				elif rarity == 1:
					rarity1 += 1
				elif rarity == 2:
					rarity2 += 1

		newMsg = 'Master ' + author + ', here is some information about your Trinket collection\n'
		newMsg += '```Total Trinkets: ' + str(totalTrinkets) + '. Unique Trinkets: ' + str(uniqueTrinkets) + '.\n'
		newMsg += rarityMap[2] + ' Trinkets: ' + str(rarity2) + '. ' + rarityMap[1] + ' Trinkets: ' + str(rarity1) + '. ' + rarityMap[0] + ' Trinkets: ' + str(rarity0) + '.```'
		sentMessage = await message.channel.send(newMsg)

		trinketThread = await sentMessage.create_thread(name=str(author + '\'s Trinkets'), auto_archive_duration=60)

		for trinketKey in stateMap[str(message.guild.id)][author][data_trinkets]:
			if stateMap[str(message.guild.id)][author][data_trinkets][trinketKey] > 0:
				miniMsg = trinketKey + '\n'
				miniMsg += '```Owned: ' + str(stateMap[str(message.guild.id)][author][data_trinkets][trinketKey]) + '\n'
				miniMsg += 'Rarity: ' + rarityMap[trinketMap[trinketKey]['rarity']] + '```'

				embed = discord.Embed() 
				embed.set_image(url=trinketMap[trinketKey]['img'])
				await trinketThread.send(content=miniMsg, embed=embed)

	else:
		await message.channel.send('You don\'t have any Trinkets, master ' + author + '. Spend some Wizcoin and \"Collect Every One!\"' )
commands.append({
	'name' : 'view full trinkets',
	'description' : 'View a detailed list of your Trinket collection, including images and rarity info.',
	'func' : command_viewFullTrinkets})	
async def command_viewTrinkets(message, author, msgString):
	if len(stateMap[str(message.guild.id)][author][data_trinkets]) > 0:
		ownedTrinks = 0
		for trinketKey in stateMap[str(message.guild.id)][author][data_trinkets]:
			if stateMap[str(message.guild.id)][author][data_trinkets][trinketKey] > 0:
				ownedTrinks += 1
				break
		if ownedTrinks == 0:
			await message.channel.send('You don\'t have any Trinkets, master ' + author + '. Spend some Wizcoin and \"Collect Every One!\"' )
			return

		newMsg = 'Here are the Trinkets you own, master ' + author + '. Remember to use the command \'wizbot, view full trinkets\' to see images and rarity details.\n'
		newMsg += '```'
		for trinketKey in stateMap[str(message.guild.id)][author][data_trinkets]:
			if stateMap[str(message.guild.id)][author][data_trinkets][trinketKey] > 0:
				newMsg += '\"' + trinketKey + '\" : ' + str(stateMap[str(message.guild.id)][author][data_trinkets][trinketKey]) + '\n'
		newMsg += '```'
		await message.channel.send(newMsg)
	else:
		await message.channel.send('You don\'t have any Trinkets, master ' + author + '. Spend some Wizcoin and \"Collect Every One!\"' )
commands.append({
	'name' : 'view trinkets',
	'description' : 'View a summarized list of your Trinket collection.',
	'func' : command_viewTrinkets})	
async def command_sellTrinket(message, author, msgString):
	if len(stateMap[str(message.guild.id)][author][data_trinkets]) > 0:
		if msgString.lower() == 'wizbot, sell trinket':
			await message.channel.send('Please type the command again with the name of the tinket you would like to sell. Like this: \"wizbot, sell trinket <name>\"')
		else:
			trinketStr = msgString.split("sell trinket ", 1)[1]
			if trinketStr in stateMap[str(message.guild.id)][author][data_trinkets] and stateMap[str(message.guild.id)][author][data_trinkets][trinketStr] > 0:
				stateMap[str(message.guild.id)][author][data_trinkets][trinketStr] -= 1
				value = trinketMarketState[trinketStr][trinketMarketValue]
				stateMap[str(message.guild.id)][author][state_wizcoinCount] += value
				if value == 0:
					await message.channel.send('The current market value for that Trinket is 0 Wizcoin. I\'ve gone ahead and thrown ' + trinketStr + ' into the trash for you, master ' + author + '.')
				else:
					await message.channel.send('Understood. I\'ve sold ' + trinketStr + ' at current market value (' + '{:.6}'.format(float(value)) + 'μ) to Wizbay user ' + getRandomName() +'.')
			else:
				await message.channel.send('My deepest apologies, master ' + author + ', I don\'t see that Trinket in your collection. Perhaps you\'ve type\'d it wrongly?')
	else:
		await message.channel.send('You don\'t have any Trinkets, master ' + author + '. Spend some Wizcoin and \"Collect Every One!\"' )
commands.append({
	'name' : 'sell trinket',
	'description' : 'Sell a trinket at current market value.',
	'func' : command_sellTrinket})	
async def command_marketDigest(message, author, msgString):
	if trinketMarketState == {}:
		await message.channel.send('Oh dear, I\'m having trouble getting in touch with the stock exchange...')
	else:
		path15DayVal = get15DayImgPath()
		file = discord.File(path15DayVal, filename="15DayVal.png")
		embed = discord.Embed() 
		embed.set_image(url="attachment://15DayVal.png")

		digestMsg = getMarketDigest()

		sentMessage = await message.channel.send(content=digestMsg, embed=embed, file=file)
		digestThread = await sentMessage.create_thread(name='Market Digest')

		path50DayVal = get50DayImgPath()
		file = discord.File(path50DayVal, filename="50DayVal.png")
		embed = discord.Embed() 
		embed.set_image(url="attachment://50DayVal.png")
		await digestThread.send(content='', embed=embed, file=file)

		pathAvgVal = getAvgImgPath()
		file = discord.File(pathAvgVal, filename="pathAvgVal.png")
		embed = discord.Embed() 
		embed.set_image(url="attachment://pathAvgVal.png")
		await digestThread.send(content='', embed=embed, file=file)

		currMarkStateMsgs = getCurrentMarketState()
		for totalsMsg in currMarkStateMsgs:
			await digestThread.send('```'+totalsMsg+'```')
commands.append({
	'name' : 'market digest',
	'description' : 'View the current status of the Trinket Market.',
	'func' : command_marketDigest})	
# auction
async def command_auction(message, author, msgString):
	if len(stateMap[str(message.guild.id)][author][data_trinkets]) > 0:
		stateMap[str(message.guild.id)][author][state_auctioningTrinket] = state_auctioningTrinket_notStarted
		if msgString.lower() == 'wizbot, auction':
			await message.channel.send('Please type the command again with the item you would like to auction, master ' + author + '. Like this: \"wizbot, auction <Trinket>\"')
		else:
			trinketStr = msgString.split("auction ", 1)[1]
			if trinketStr in stateMap[str(message.guild.id)][data_auctionHouseItems]:
				await message.channel.send('My deepest apologies, master ' + author + ', There is already a \"' + trinketStr + '\" up for sale. No duplicates allowed I\'m afriad. Supply and demand, and all that.')
			else:
				if trinketStr in stateMap[str(message.guild.id)][author][data_trinkets]:
					stateMap[str(message.guild.id)][author][state_auctioningTrinket] = state_auctioningTrinket_selecting
					stateMap[str(message.guild.id)][author][state_auctioningTrinketName] = trinketStr
					await message.channel.send('Excellent. And how much Wizcoin would you be auctioning this item for, master ' + author + '?')
				else:
					await message.channel.send('My deepest apologies, master ' + author + ', I don\'t see that Trinket in your collection. Perhaps you\'ve type\'d it wrongly?')
		return
	else:
		await message.channel.send('You don\'t have any Trinkets that you can auction, master ' + author + '.' )
commands.append({
	'name' : 'auction',
	'description' : 'Put a trinket up for acution, where another Wizard may pay you Wizcoin for it. \"auction <Trinket>\"',
	'func' : command_auction})	
async def command_auctionVisit(message, author, msgString):
	if len(stateMap[str(message.guild.id)][data_auctionHouseItems]) > 0:
		sentMessage = await message.channel.send('Welcome to the auction house!\n```If something catches your fancy you can use the command \"wizbot, buy auction item <Trinket>\". And remember if you want to remove your own item from the acution you can use \"wizbot, unauction <Trinket>\"```')
		auctionThread = await sentMessage.create_thread(name='Auction House')

		for trinketKey in stateMap[str(message.guild.id)][data_auctionHouseItems]:
			item = stateMap[str(message.guild.id)][data_auctionHouseItems][trinketKey]
			miniMsg = trinketKey + '\n'
			miniMsg += '```'
			miniMsg += 'Price: ' + str(item[data_auctionItemCost]) + ' Wizcoin\n'
			miniMsg += 'Seller: ' + item[data_auctionItemUsername] + '\n'
			miniMsg += 'Rarity: ' + rarityMap[trinketMap[trinketKey]['rarity']]
			miniMsg += '```'

			embed = discord.Embed() 
			embed.set_image(url=trinketMap[trinketKey]['img'])
			await auctionThread.send(content=miniMsg, embed=embed)

	else:
		await message.channel.send('Currently there\'s nothing up for auction, master ' + author + ', I\'m very sorry to say.' )
commands.append({
	'name' : 'visit auction',
	'description' : 'Visit the storied Wizard Auction House, where many items can be found or put up for sale.',
	'func' : command_auctionVisit})	
async def command_unauction(message, author, msgString):
	if len(stateMap[str(message.guild.id)][data_auctionHouseItems]) > 0:
		if msgString.lower() == 'wizbot, unauction':
			await message.channel.send('Please type the command again with the item you would like to take back, master ' + author + '. Like this: \"wizbot, unauction <Trinket>\"')
		else:
			trinketStr = msgString.split("unauction ", 1)[1]
			if trinketStr not in trinketMap:
				await message.channel.send('My deepest apologies, master ' + author + ', But \"' + trinketStr + '\" isn\'t a Trinket that I\'m familiar with.')
			elif trinketStr not in stateMap[str(message.guild.id)][data_auctionHouseItems]:
				await message.channel.send('My deepest apologies, master ' + author + ', I can\'t find a \"' + trinketStr + '\" up for sale.')
			elif stateMap[str(message.guild.id)][data_auctionHouseItems][trinketStr][data_auctionItemUsername] != author:
				await message.channel.send('The trinket \"' + trinketStr + '\" belongs to master \"' + stateMap[str(message.guild.id)][data_auctionHouseItems][trinketStr][data_auctionItemUsername] + '\" I\'m afraid. It cannot be unauctioned by you, sire.')
			else:
				stateMap[str(message.guild.id)][data_auctionHouseItems].pop(trinketStr)
				if trinketStr not in stateMap[str(message.guild.id)][author][data_trinkets]:
					print('returning auctioned trinket but it wasnnt in the user map. this should be impossible')
					stateMap[str(message.guild.id)][author][data_trinkets][trinketStr] = 1
				else:
					stateMap[str(message.guild.id)][author][data_trinkets][trinketStr] += 1
				await message.channel.send('\"' + trinketStr + '\" was returned to your collection, master ' + author + '.')
	else:
		await message.channel.send('Currently there\'s nothing up for auction, master ' + author + '.' )
commands.append({
	'name' : 'unauction',
	'description' : 'Seller\'s remorse? Take back what is yours with \"wizbot, unaction <Trinket>\".',
	'func' : command_unauction})
async def command_auctionBuyTrinket(message, author, msgString):
	if len(stateMap[str(message.guild.id)][data_auctionHouseItems]) > 0:
		if msgString.lower() == 'wizbot, buy auction item':
			await message.channel.send('Please type the command again with the item you would like to take back, master ' + author + '. Like this: \"wizbot, unauction <Trinket>\"')
		else:
			trinketStr = msgString.split("buy auction item ", 1)[1]
			if trinketStr not in trinketMap:
				await message.channel.send('My deepest apologies, master ' + author + ', But \"' + trinketStr + '\" isn\'t a Trinket that I\m familiar with.')
			elif trinketStr not in stateMap[str(message.guild.id)][data_auctionHouseItems]:
				await message.channel.send('My deepest apologies, master ' + author + ', I can\'t find a \"' + trinketStr + '\" up for sale.')
			elif stateMap[str(message.guild.id)][data_auctionHouseItems][trinketStr][data_auctionItemUsername] == author and author != 'Duncan':
				await message.channel.send('But master ' + author + ', \"' + trinketStr + '\" was put up to auction by you, sire! Use  \"wizbot, unauction <Trinket>\" to retrieve it.')
			else:
				amnt = stateMap[str(message.guild.id)][data_auctionHouseItems][trinketStr][data_auctionItemCost]
				if stateMap[str(message.guild.id)][author][state_wizcoinCount] < amnt:
					await message.channel.send('My deepest apologies, master ' + author + ', But you can\'t afford this Trinket.')
				else:

					userNameCache = stateMap[str(message.guild.id)][data_auctionHouseItems][trinketStr][data_auctionItemUsername]
					stateMap[str(message.guild.id)][author][state_wizcoinCount] -= amnt
					stateMap[str(message.guild.id)][stateMap[str(message.guild.id)][data_auctionHouseItems][trinketStr][data_auctionItemUsername]][state_wizcoinCount] += amnt
					stateMap[str(message.guild.id)][data_auctionHouseItems].pop(trinketStr)
					if trinketStr not in stateMap[str(message.guild.id)][author][data_trinkets]:
						stateMap[str(message.guild.id)][author][data_trinkets][trinketStr] = 1
					else:
						stateMap[str(message.guild.id)][author][data_trinkets][trinketStr] += 1


					await message.channel.send('Sold! Master ' + userNameCache + '\'s \"' + trinketStr + '\" has been sold to master ' + author + ' for the price of ' + str(amnt) + ' Wizcoin. Your accounts and collections have been updated accordingly.')				
	else:
		await message.channel.send('Currently there\'s nothing up for auction, master ' + author + '.' )
commands.append({
	'name' : 'buy auction item',
	'description' : 'Something catch you\'r eye at the auction house? Use the command \"wizbot, buy auction item <Trinket>\"',
	'func' : command_auctionBuyTrinket})	
# gamestop
async def command_visitGamestop(message, author, msgString):
	cachedGamestopMap = fetchGamestopSelection()
	if cachedGamestopMap == {}:
		await message.channel.send('Terribly sorry, master ' + author + ', GameStop appears to be temporarily closed')
		return

	summaryMsg = 'Here\'s what\'s in stock today. Selection resets daily at Midnight PST\n```\n'
	for platKey in cachedGamestopMap:
		if len(cachedGamestopMap[platKey]) > 0:
			summaryMsg += platKey + '\n'
			for game in cachedGamestopMap[platKey]:
				summaryMsg += '\t' + game['name'] + '\n'
	summaryMsg += '```'

	sentMessage = await message.channel.send(summaryMsg)
	gamestopThread = await sentMessage.create_thread(name=str('GameStop Selection'), auto_archive_duration=60)
	
	for platKey in cachedGamestopMap:
			for game in cachedGamestopMap[platKey]:
				gameMsg = 'Name: \"' + game['name'] + '\"\n'
				gameMsg += '```'
				gameMsg += 'Platform: \"' + game['platform'] + '\"\n'
				gameMsg += 'Price: \"' + str(game['price']) + 'μ\"\n'
				gameMsg += 'Summary: \"' + game['summary'] + '```'

				if game['screenshot'] != '':
					embed = discord.Embed() 
					embed.set_image(url=game['screenshot'])
					await gamestopThread.send(content=gameMsg, embed=embed)
				else:
					await gamestopThread.send(gameMsg)

commands.append({
	'name' : 'visit gamestop',
	'description' : 'Why not stroll down to the local game store and pick something up?',
	'func' : command_visitGamestop})	
async def command_buyvideogame(message, author, msgString):
	cachedGamestopMap = fetchGamestopSelection()
	if cachedGamestopMap == {}:
		await message.channel.send('Terribly sorry, master ' + author + ', GameStop appears to be temporarily closed')
		return
	if msgString == 'wizbot, buy game':
		await message.channel.send('Please enter the game you wish to buy like this: \"wizbot, buy game <Game Name>\"')
		return

	gamesList = {}
	for platKey in cachedGamestopMap:
			for game in cachedGamestopMap[platKey]:
				gamesList[game['name']] = game

	toBuy = msgString.split("buy game ", 1)[1]
	if toBuy not in gamesList:
		await message.channel.send('I\'m afriad I don\'t see that game at the store. Make sure you\'ve spelled it correctly, master' + author + '.')
		return
	if toBuy in stateMap[str(message.guild.id)][author][data_gameCollection]:
		await message.channel.send('You already own this game, master' + author + '.')
		return

	gameToBuy = gamesList[toBuy]
	price = gameToBuy['price']
	coinOnHand = stateMap[str(message.guild.id)][author][state_wizcoinCount]

	if coinOnHand < price:
		await message.channel.send('So so sorry, master' + author + ', this game appears to be out of your price range.')
		return

	cachedGamestopMap[gameToBuy['platform']].remove(gameToBuy)
	stateMap[str(message.guild.id)][author][state_wizcoinCount] -= price
	stateMap[str(message.guild.id)][author][data_gameCollection][toBuy] = gameToBuy

	await message.channel.send('I\'d be happy to make that transaction for you, master ' + author + '. \"' + toBuy + ' has been added to your game library. Use \"wizbot, view game library\" to see your games.')

	writeOutGamestopSelection(cachedGamestopMap)

commands.append({
	'name' : 'buy game',
	'description' : 'See something you like at GameStop? buy with\"wizbot, buy game <Game Name>\"',
	'func' : command_buyvideogame})	
async def command_viewgames(message, author, msgString):
	gamesCnt = len(stateMap[str(message.guild.id)][author][data_gameCollection])
	if gamesCnt > 0:
		newMsg = 'You have ' + str(gamesCnt) + ' games, sire.'
		if gamesCnt == 1:
			newMsg = 'You have 1 game, sire.'

		sentMessage = await message.channel.send(newMsg)
		gamelibraryThread = await sentMessage.create_thread(name=str('Game Library'), auto_archive_duration=60)
		for gameKey in  stateMap[str(message.guild.id)][author][data_gameCollection]:
			game = stateMap[str(message.guild.id)][author][data_gameCollection][gameKey]
			gameMsg = 'Name: \"' + game['name'] + '\"\n'
			gameMsg += '```'
			gameMsg += 'Platform: \"' + game['platform'] + '\"\n'
			gameMsg += 'Bought for: \"' + str(game['price']) + 'μ\"\n'
			gameMsg += 'Summary: \"' + game['summary'] + '```'

			if game['screenshot'] != '':
				embed = discord.Embed() 
				embed.set_image(url=game['screenshot'])
				await gamelibraryThread.send(content=gameMsg, embed=embed)
			else:
				await gamelibraryThread.send(gameMsg)

	else:
		await message.channel.send('You don\'t have any games, master ' + author + '. Head to the GameStop and buy some, if you\'re asking my opinion!')
commands.append({
	'name' : 'view game library',
	'description' : 'See all games in your collection',
	'func' : command_viewgames})	
async def command_viewArtifacts(message, author, msgString):
	if len(stateMap[str(message.guild.id)][author][data_artifacts]) == 0:
		await message.channel.send('You don\'t seem to have any Artifacts, master ' + author)
	else:
		newMsg = '```'
		for artifact in stateMap[str(message.guild.id)][author][data_artifacts]:
			newMsg += artifact + '\n'
		newMsg += '```'
		await message.channel.send(newMsg)

commands.append({
	'name' : 'view artifacts',
	'description' : 'See all of your mysterious Artifacts',
	'func' : command_viewArtifacts})	
# prefs
async def command_viewPrefs(message, author, msgString):
	newMsg = 'This is the full list of customizable preferences, master ' + author + '. To change one, use command \'wizbot, set pref <number> to <true/false>\'.\n'
	newMsg += '```'
	idx = 0
	for prefName in prefsIdxMap:
		newMsg += str(idx) + ': ' + prefsDescrMap[idx] + '. CURRENT_VALUE=[' + str(stateMap[str(message.guild.id)][author][data_prefs][prefName]) + ']\n'
		idx += 1
	newMsg += '```'
	await message.channel.send(newMsg)
commands.append({
	'name' : 'view prefs',
	'description' : 'So many variables, so many permutations. I am fully customizable, master.',
	'func' : command_viewPrefs})	
async def command_setPref(message, author, msgString):
	prefValStr = msgString.split("to ", 1)[1].replace(' ', '')
	prefNumStr = msgString.split("to ", 1)[0].split('pref ', 1)[1].replace(' ', '')

	if not prefNumStr.isnumeric():
		print('bad pref formatting on numeral: ')
		print(prefValStr + ' ' + prefNumStr + '\n')
		await message.channel.send('Imporperly Formatted')
		return
	if prefValStr.lower() != 'true' and prefValStr.lower() != 'false':
		print('bad pref formatting on value: ')
		print(prefValStr + ' ' + prefNumStr + '\n')
		await message.channel.send('Imporperly Formatted')
		return
	prefNum = int(prefNumStr)
	if prefNum < 0 or prefNum >= len(prefsIdxMap):
		await message.channel.send('Out of Range')
		return

	prefVal = False
	if prefValStr.lower() == 'true':
		prefVal = True

	prefName = prefsIdxMap[prefNum]
	stateMap[str(message.guild.id)][author][data_prefs][prefName] = prefVal

	await message.channel.send('I have set pref number ' + str(prefNum) + ' (' + prefName + ') for user ' + author + ' to [' + str(prefVal) + ']')
commands.append({
	'name' : 'set pref',
	'description' : 'Set a pref value. Use \'view prefs\' to see the full list and syntax.',
	'func' : command_setPref})	





@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))

async def parseMessage(message):
	author = message.author.name
	msgString = message.content

	msgString = msgString.replace('’', '\'')

	if stateMap == {}:
		if msgString.lower().startswith('wizbot, reboot'):
			try:
				loadStatemap()
			except:
				print('fatal error: failed to load state map')
			if stateMap == {}:
				return await message.channel.send("It's no good...")
			else:
				return await message.channel.send("Back at your service, sire")
		elif msgString.lower().startswith('wizbot, '):
			return await message.channel.send("I\'m currently trapped in the Realm of Darkness... type \"wizbot, reboot\" to try and help me escape.")


	for cmd in commands:
		if msgString.lower().startswith('wizbot, ' + cmd['name']):
			await cmd['func'](message, author, msgString)
			return

	msgString = msgString.replace('Wizbot', 'wizbot')

	# continued state from already-started commands
	if msgString.startswith('wizbot, '):
		# guessing game
		if state_guessingGame in stateMap[str(message.guild.id)][author] and stateMap[str(message.guild.id)][author][state_guessingGame] == gamestate_waiting:
			userGuess = msgString.split("wizbot, ", 1)[1] 

			if not userGuess.isnumeric():
				await message.channel.send('Master ' + message.author.name + ', please take this seriously.')
				stateMap[str(message.guild.id)][author][state_guessingGame].state = gamestate_notStarted
			else:
				gamebleNumber = int(userGuess)

				if gamebleNumber < 1 or gamebleNumber > 5:
					await message.channel.send('Master ' + message.author.name + ', that\'s far off the mark.')
					stateMap[str(message.guild.id)][author][state_guessingGame].state = gamestate_notStarted
				elif stateMap[str(message.guild.id)][author][state_guessingGame].correctNum == gamebleNumber or author == 'Duncan':
					await message.channel.send('Incredible guess ' + message.author.name + '! You\'ve done it! You\'re a real Wizard!')
					stateMap[str(message.guild.id)][author][state_guessingGame].state = gamestate_notStarted
				else:
					await message.channel.send('I win this time, master ' + message.author.name + '. Try again... if you dare!')
					stateMap[str(message.guild.id)][author][state_guessingGame].state = gamestate_notStarted
			return
		# alias
		if state_addingAlias in stateMap[str(message.guild.id)][author] and stateMap[str(message.guild.id)][author][state_addingAlias] == True:
			stateMap[str(message.guild.id)][author][state_addingAlias] = False
			newName = msgString.split("wizbot, ", 1)[1] 
			if newName in stateMap[str(message.guild.id)][data_aliasMap]:
				await message.channel.send('My dearest apologies, master ' + message.author.name + ', This alias appears to already be in use.')
			else:
				forUser = stateMap[str(message.guild.id)][author][state_addingAliasName] 
				stateMap[str(message.guild.id)][data_aliasMap][newName] = forUser
				await message.channel.send('Understood. Henceforth I shall recognize ' + newName + ' as ' + forUser )
			return
		# wizcoin
		if stateMap[str(message.guild.id)][author][state_wizcoinMiningState] == state_wizcoinMiningState_waitingForAnswer:
			amount = msgString.split("wizbot, ", 1)[1] 
			if not amount.isnumeric():
				await message.channel.send('Apologies, master ' + message.author.name + ', I can\'t understand that. Please enter in a standard numeral.')
				return
			if int(amount) > 1000000:
				await message.channel.send('Deepest apologies, master ' + message.author.name + ', that number is too long to fit into the Wizcoin Ledger. Please try something smaller.')
				return
			if int(amount) == 0:
				await message.channel.send('Modest request, master ' + message.author.name + ', but perhaps you\'d like to aim your sights higher?')
				return
			if int(amount) < 0:
				await message.channel.send('Please, master ' + message.author.name + ', you\'re wasting my time and yours with these childish requests.')
				return
			timeToMine = wizcoinSecondsToMinePerCoin * int(amount)
			timeReadible = readibleTimeFromStamp(timeToMine)

			stateMap[str(message.guild.id)][author][state_wizcoinStartedMiningTime] = datetime.datetime.now().timestamp()
			stateMap[str(message.guild.id)][author][state_wizcoinMiningAmountInPorgress] = int(amount)
			stateMap[str(message.guild.id)][author][state_wizcoinMiningState] = state_wizcoinMiningState_waitingForMining
			newMsg = 'As you wish, master ' + message.author.name + ', I\'ll get that started right away. That much Wizcoin will take this long to mine: ' + timeReadible + '.\n'
			newMsg += 'After the time has elapsed you can use the command  \'wizbot, collect wizcoin\' to retrieve it. \nOr you can use command \'wizbot, abort mining\' if you wish to cancel.'
			await message.channel.send(newMsg)
			return
		if stateMap[str(message.guild.id)][author][state_wizcoinMiningState] == state_wizcoinMiningState_waitingForGoblins:
			if msgString.startswith('wizbot, cancel'):
				stateMap[str(message.guild.id)][author][state_wizcoinMiningState] = state_wizcoinMiningState_notStarted
				await message.channel.send('Understood, master ' + author + '. Perhaps another time.')
				return
			amount = msgString.split("wizbot, ", 1)[1] 
			if not amount.isnumeric():
				await message.channel.send('Apologies, master ' + message.author.name + ', I can\'t understand that. Please enter in a standard numeral.')
				return
			if int(amount) > 1000000:
				await message.channel.send('Deepest apologies, master ' + message.author.name + ', that number is too long to fit into the Wizcoin Ledger. Please try something smaller.')
				return
			if int(amount) == 0:
				await message.channel.send('Modest request, master ' + message.author.name + ', but perhaps you\'d like to hire more?')
				return
			if int(amount) < 0:
				await message.channel.send('Please, master ' + message.author.name + ', you\'re wasting my time and yours with these childish requests.')
				return

			cost = amount # 1 coin for 1 gob
			if stateMap[str(message.guild.id)][author][state_wizcoinCount] < int(amount):
				await message.channel.send('I don\'t how to say this, master ' + author + ', but your pockets are not quite, ahem, deep enough to make that transaction')
				resetState(message, author)

			stateMap[str(message.guild.id)][author][state_wizcoinCount] -= cost
			stateMap[str(message.guild.id)][author][data_wicoinMiningGoblins] += amount
			await message.channel.send('Excellent choice, master '+ author +'. With that, ' + str(stateMap[str(message.guild.id)][author][data_wicoinMiningGoblins]) + ' Goblins are now working for you.')

		# trinket
		if stateMap[str(message.guild.id)][author][state_buyingTrinket] == state_buyingTrinket_waiting:
			if msgString.startswith('wizbot, cancel'):
				stateMap[str(message.guild.id)][author][state_buyingTrinket] = state_buyingTrinket_notStarted
				await message.channel.send('Understood, master ' + author + '. Perhaps another time.')
				return
			amount = msgString.split("wizbot, ", 1)[1] 
			if not amount.isnumeric():
				await message.channel.send('Apologies, master ' + message.author.name + ', I didn\'t quite get that.')
				return
			if int(amount) > stateMap[str(message.guild.id)][author][state_wizcoinCount]:
				await message.channel.send('I don\'t how to say this, master ' + author + ', but your pockets are not quite, ahem, deep enough to make that transaction')
				stateMap[str(message.guild.id)][author][state_buyingTrinket] = state_buyingTrinket_notStarted
				return
			if int(amount) < 0:
				stateMap[str(message.guild.id)][author][state_buyingTrinket] = state_buyingTrinket_notStarted
				await message.channel.send('I suppose you must think me a simple fool, master ' + message.author.name + '. Your Wizard Tricks hold no power over me')
				return
			elif int(amount) == 0:
				stateMap[str(message.guild.id)][author][state_buyingTrinket] = state_buyingTrinket_notStarted
				await message.channel.send('Nothing in life is free, master ' + message.author.name + '. But especially not my finest trinkets')
				return
			elif int(amount) < minCapsuleToyCost:
				await message.channel.send('Beg your pardon, master ' + message.author.name + ', the minimum cost of a trinket is ' + str(minCapsuleToyCost) + ' Wizcoin.')
				return
			elif int(amount) > maxCapsuleToyCost:
				await message.channel.send('Beg your pardon, master ' + message.author.name + ', the maxium amount you can spend on a trinket is ' + str(maxCapsuleToyCost) + ' Wizcoin.')
				return
			else:
				stateMap[str(message.guild.id)][author][state_buyingTrinket] = state_buyingTrinket_notStarted
				seed = (float(amount) - minCapsuleToyCost) / (float(maxCapsuleToyCost) - minCapsuleToyCost)
				trinketKey = ''
				while True:
					trinketIdx = random.randrange(0, len(trinketMap))
					trinketKey = list(trinketMap)[trinketIdx]
					trinketVal = list(trinketMap.values())[trinketIdx]

					randVal = random.random()
					if seed < 0.25: # arbitrary threshhold over which your seed will garuntee you a rare or ultra rare trinket
						if (trinketVal['rarity'] <= 1 and randVal < seed) or randVal > seed:
							break
					else:
						if (trinketVal['rarity'] == 0 and randVal < seed) or randVal > seed:
							break


				newMsg = 'You got... '
				if trinketKey in stateMap[str(message.guild.id)][author][data_trinkets] and stateMap[str(message.guild.id)][author][data_trinkets][trinketKey] > 0:
					newMsg = 'You got a duplicate of '
				elif trinketMap[trinketKey]['rarity'] == 1:
					newMsg = 'Nice one. You got '
				elif trinketMap[trinketKey]['rarity'] == 0:
					newMsg = 'Oh my word! You got '
				newMsg += trinketKey + '\n'
				newMsg += '```'
				newMsg += 'Rarity: ' + rarityMap[trinketMap[trinketKey]['rarity']] 

				if trinketKey in stateMap[str(message.guild.id)][author][data_trinkets] and stateMap[str(message.guild.id)][author][data_trinkets][trinketKey] > 0:
					newMsg += ', Owned: ' + str(1 + stateMap[str(message.guild.id)][author][data_trinkets][trinketKey])
				newMsg += '\nUse command \'wizbot, view trinkets\' to see your full collection```'

				if trinketKey not in stateMap[str(message.guild.id)][author][data_trinkets]:
					stateMap[str(message.guild.id)][author][data_trinkets][trinketKey] = 0
				stateMap[str(message.guild.id)][author][data_trinkets][trinketKey] = stateMap[str(message.guild.id)][author][data_trinkets][trinketKey] + 1
				stateMap[str(message.guild.id)][author][state_wizcoinCount] = stateMap[str(message.guild.id)][author][state_wizcoinCount] - int(amount)

				embed = discord.Embed() 
				embed.set_image(url=trinketMap[trinketKey]['img'])
				await message.channel.send(content=newMsg, embed=embed)

				# check for all trinkets
				hasAll = True
				for trinketName in trinketMap:
					if trinketName in stateMap[str(message.guild.id)][author][data_trinkets] and stateMap[str(message.guild.id)][author][data_trinkets][trinketName] == 0:
						hasAll = False
						break
					if trinketName not in stateMap[str(message.guild.id)][author][data_trinkets]:
						hasAll = False
						break

				if hasAll and artifact_trinketKey not in stateMap[str(message.guild.id)][author][data_artifacts]:
					stateMap[str(message.guild.id)][author][data_artifacts].append(artifact_trinketKey)
					await message.channel.send('What... What\'s this!?! A blinding bright light emerges from ' + author + '\'s Trinket Sack! I don\'t believe it! It\'s a legendary Trinket Key! You are a Keeper of the Key!!!')

					newMsg = 'Master ' + author + ' got Artifact: \"Trinket Key\". Use command \"wizbot, view artifacts\" to see all Artifacts'
					embed = discord.Embed() 
					embed.set_image(url='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSvAa0dm_gglrK2-WmnpDn5GWj_nCVJmDH3Qw')
					await message.channel.send(content=newMsg, embed=embed)

				return
		# auction (selling)
		if stateMap[str(message.guild.id)][author][state_auctioningTrinket] == state_auctioningTrinket_selecting:
			if msgString.startswith('wizbot, cancel'):
				stateMap[str(message.guild.id)][author][state_auctioningTrinket] = state_auctioningTrinket_notStarted
				await message.channel.send('Understood, master ' + author + '. Perhaps another time.')
				return
			amount = msgString.split("wizbot, ", 1)[1] 
			if not amount.isnumeric() or int(amount) < 0:
				await message.channel.send('Apologies, master ' + message.author.name + ', I didn\'t quite get that.')
				return
			if int(amount) > 1000000000:
				stateMap[str(message.guild.id)][author][state_auctioningTrinket] = state_auctioningTrinket_notStarted
				await message.channel.send('Deeply sorry, master ' + message.author.name + '. That sum is too large to fit into Wizard Ledger.')
				return

			trinketName = stateMap[str(message.guild.id)][author][state_auctioningTrinketName]
			stateMap[str(message.guild.id)][author][state_auctioningTrinketName] = ''

			auctionItem = {}
			auctionItem[data_auctionItemUsername] = author
			auctionItem[data_auctionItemTrinketName] = trinketName
			auctionItem[data_auctionItemCost] = int(amount)

			stateMap[str(message.guild.id)][data_auctionHouseItems][trinketName] = auctionItem
			stateMap[str(message.guild.id)][author][data_trinkets][trinketName] -= 1

			stateMap[str(message.guild.id)][author][state_auctioningTrinket] == state_auctioningTrinket_notStarted

			await message.channel.send('It is done. \"' + trinketName + '\" has been removed from your Trinket collection and placed into the auction house.\n```Use \"wizbot, unauction <Trinket>\" if you change your mind or \"wizbot, visit auction\" to peruse items for sale.```')
			return
	# secret
	

	if msgString.startswith('wizbot, who is the greatest wizard'):
		mostCoinUser = 'Nobody'
		maxCoin = 0
		for user in stateMap[str(message.guild.id)]:
			if state_wizcoinCount in stateMap[str(message.guild.id)]:
				if stateMap[str(message.guild.id)][user][state_wizcoinCount] > maxCoin:
					mostCoinUser = user
					maxCoin = stateMap[str(message.guild.id)][user][state_wizcoinCount]
		numWithMaxCoin = 0
		for user in stateMap[str(message.guild.id)]:
			if state_wizcoinCount in stateMap[str(message.guild.id)]:
				if stateMap[str(message.guild.id)][user][state_wizcoinCount] == maxCoin:
					numWithMaxCoin = numWithMaxCoin + 1
					if numWithMaxCoin >= 2:
						await message.channel.send('Intriguing inquiry! I shall mull over this question in my study, master ' + author + '...')
						return
		await message.channel.send('The greatest Wizard is they who hold the most Wizcoin, naturally. Currently that would be master ' + mostCoinUser + ' with ' + str(maxCoin) + ' Wizcoin to their name.')
	

	if msgString.startswith('wizbot, i love you'):
		await message.channel.send('I love you too, master ' + author + '.')
		return
	if msgString.startswith('computer, load up celery man'):
		embed = discord.Embed() 
		embed.set_image(url='https://i.imgur.com/5m2WSZX.gif')
		await message.channel.send(content='', embed=embed)
		return
	if msgString.startswith('wizbot, load up celery man'):
		embed = discord.Embed() 
		embed.set_image(url='https://i.imgur.com/5m2WSZX.gif')
		await message.channel.send(content='', embed=embed)
		return
	if msgString.startswith('wizbot, could you kick up the 4d3d3d3'):
		embed = discord.Embed() 
		embed.set_image(url='https://i.gifer.com/4sJP.gif')
		await message.channel.send(content='', embed=embed)
		return
	if msgString.startswith('wizbot, give me a printout of oyster smiling'):
		embed = discord.Embed() 
		embed.set_image(url='https://thumbs.gfycat.com/ShamefulConsciousHogget-size_restricted.gif')
		await message.channel.send(content='', embed=embed)
		return
	if msgString.startswith('wizbot, could i see a hat wobble'):
		embed = discord.Embed() 
		embed.set_image(url='https://thumbs.gfycat.com/KlutzyAcrobaticFrigatebird-size_restricted.gif')
		await message.channel.send(content='', embed=embed)
		return
	if msgString.startswith('wizbot, and a flarhgunnstow'):
		embed = discord.Embed() 
		embed.set_image(url='https://i.gifer.com/origin/65/6585e9efe82e6ba0c6d11f20d2fc04ac_w200.gif')
		await message.channel.send(content='', embed=embed)
		return
	if msgString.startswith('wizbot, flip coin'):
		val  = random.random()
		if val > 0.5:
			await message.channel.send('Heads')
		else:
			await message.channel.send('Tails')
		return

	if 'dune' in msgString:
		if 'state_dune' not in stateMap[str(message.guild.id)][author]:
			await message.channel.send('A fantastic film, ' + message.author.name + ', I agree hardily! Ah, the romance of the silver screen!')
			stateMap[str(message.guild.id)][author]['state_dune'] = 'done'
			return

	if msgString.startswith('wizbot, where do you live'):
		await message.channel.send('I\'m currently hosted on bitbucket at https://bitbucket.org/duncankeller/wizbot\nI have everything here that a Wizard needs to be happy.')
		return

	# debug
	if msgString.startswith('wizbot, debug set wizcoin '):
		if author == 'Duncan':
			amount = msgString.split("wizbot, debug set wizcoin ", 1)[1] 
			stateMap[str(message.guild.id)][author][state_wizcoinCount] = int(amount)
			return
	if msgString.startswith('wizbot, debug dungeon'):
		if author == 'Duncan':

			sentMessage = await message.channel.send('Starting dungeon...')
			dungeonThread = await sentMessage.create_thread(name=str('Test Explore Dungeon'), auto_archive_duration=60)
			await debugExploreWithDiscord(sentMessage)

			return
	if msgString.startswith('wizbot, debug clear trinkets'):
		if author == 'Duncan':
			stateMap[str(message.guild.id)][author][data_trinkets] = {}
			return
	if msgString.startswith('wizbot, debug clear games'):
		if author == 'Duncan':
			stateMap[str(message.guild.id)][author][data_gameCollection] = {}
			return
	if msgString.startswith('wizbot, debug force gamestop update'):
		if author == 'Duncan':
			doWriteGamestop()
			return
	if msgString.startswith('wizbot, debug erase bookmarks'):
		if author == 'Duncan':
			stateMap[str(message.guild.id)][author][data_randomGameBookmarks] = []
			return
	if msgString.startswith('wizbot, debug regen market images'):
		regenImages()
		return
	if msgString.startswith('wizbot, debug abort'):
		if author == 'Duncan':
			sys.exit(0)
	if msgString.startswith('wizbot, debug give') and 'wizcoin' in msgString:
		if author == 'Duncan':
			splitString = msgString.split('wizbot, debug give ')[1]
			splitString = splitString.split('wizcoin')[0]
			userStr = splitString.split(' ')[0]
			moneyStr = splitString.split(' ')[1]
			userID = getNameFromAlias(message, userStr)
			stateMap[str(message.guild.id)][userID][state_wizcoinCount] += int(moneyStr)
			stateMap[str(message.guild.id)][userID][state_wizcoinMiningState] = state_wizcoinMiningState_notStarted
			stateMap[str(message.guild.id)][userID][state_wizcoinMiningAmountInPorgress] = 0

			return

	# fallback
	if msgString.startswith('wizbot, '):
		await message.channel.send('I\'m afriad I didn\'t understand that command, master ' + author + '. My deepest apologies.')


@client.event
async def on_message(message):
	global didInit

	if message.author == client.user:
		return

	if not didInit:
		setPostCallback(sendMsgToThread)
		init(message.guild)
		didInit = True

	if message.author.name not in stateMap[str(message.guild.id)]:
		registerUser(str(message.guild.id), message.author.name)

	await parseMessage(message)

	try:
		with open(wizstateMapFilepathTmp, 'w') as file:
			outState = {}
			outState['stateMap'] = stateMap
			json.dump(outState, file)
			
			shutil.move(wizstateMapFilepathTmp, wizstateMapFilepath)
	except Exception as e:
		print('failed to save state!')
		print(e)



 


client.run(token)