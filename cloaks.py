from itemCommon import * 

low = 4
mid = 4
high = 4

allCloaks = [
#------------------------------------------------------------- 0 ---------------------------------------------------------------
{
	'name' : 'Smelly Rags',
	'def' : 0,
	'resist' : [],
	'negate' : [],
	'price' : 200,
	'level' : 0,
	'hp-up': 0,
	'mp-up': 0,
	'descr' : 'Better than going naked.'
},
#------------------------------------------------------------- 1 ---------------------------------------------------------------
{
	'name' : 'Journeyman\'s Cloak',
	'def' : 1,
	'resist' : [],
	'negate' : [],
	'price' : high,
	'level' : 1,
	'hp-up': 0,
	'mp-up': 0,
	'descr' : 'A sturdy, well-made navy blue cloak. Not flashy but gets the job done.'
},
{
	'name' : 'Fur Coat',
	'def' : 0,
	'resist' : [Element.Ice],
	'negate' : [],
	'price' : low,
	'level' : 1,
	'hp-up': 0,
	'mp-up': 0,
	'descr' : 'This Ice-Resistant (take 50\% less damage) poofy coat is perfect for Wizards who live in colder climates. Fake fur, of course.'
},
#------------------------------------------------------------- 2 ---------------------------------------------------------------
{
	'name' : 'Spellcaster\'s Robes',
	'def' : 2,
	'resist' : [],
	'negate' : [],
	'price' : high,
	'level' : 2,
	'hp-up': 0,
	'mp-up': 10,
	'descr' : 'Long, elegent robes, imbued with magic-enhancing powers that increase your Max MP by 10.'
},
{
	'name' : 'Flame Retardant Jacket',
	'def' : 2,
	'resist' : [],
	'negate' : [Element.Fire, StatusEffect.Burn],
	'price' : low,
	'level' : 2,
	'hp-up': 0,
	'mp-up': 0,
	'descr' : 'Used by mages and firefighters alike. Negates all Fire damage.'
},
#------------------------------------------------------------- 3 ---------------------------------------------------------------
{
	'name' : 'Buff Baby Boy Robes',
	'def' : 3,
	'resist' : [],
	'negate' : [],
	'price' : low,
	'level' : 3,
	'hp-up': 30,
	'mp-up': 0,
	'descr' : 'This cloak adds 30 to max HP, perfect for all wizards aspiring to be Buff Baby Boys.'
},
{
	'name' : 'Protecc Jaket',
	'def' : 3,
	'resist' : [StatusEffect.Poison, StatusEffect.Burn, StatusEffect.Paralisis, StatusEffect.Freeze],
	'negate' : [],
	'price' : low,
	'level' : 3,
	'hp-up': 0,
	'mp-up': 0,
	'descr' : 'Reduces the chances of being inflected with Poison, Burn, Freeze, and Paralisis by half.'
},
#------------------------------------------------------------- 4 ---------------------------------------------------------------
{
	'name' : 'Drake\'s Robes',
	'def' : 7,
	'resist' : [],
	'negate' : [],
	'price' : low,
	'level' : 4,
	'hp-up': 0,
	'mp-up': 0,
	'descr' : 'Robes woven with fibers made from the claw of a drake. High defense.'
},
{
	'name' : 'Poncho',
	'def' : 4,
	'resist' : [],
	'negate' : [Element.Water],
	'price' : low,
	'level' : 4,
	'hp-up': 0,
	'mp-up': 0,
	'element-boost': Element.Water,
	'descr' : 'A standard store-bought poncho. Completley negates all Water damage, and gives you a boost to Water attacks'
},
#------------------------------------------------------------- 5 ---------------------------------------------------------------
{
	'name' : 'Casting Cloak',
	'def' : 6,
	'resist' : [StatusEffect.Silence],
	'negate' : [],
	'price' : low,
	'level' : 5,
	'hp-up': 0,
	'mp-up': 50,
	'descr' : 'This cloak, targeted towards heavy mana users, grants +50 to Max MP and reduces the chances of being inflected by Silence.'
},
{
	'name' : 'Groovin\' Cloak',
	'def' : 6,
	'resist' : [Element.Earth, Element.Wind, Element.Fire],
	'negate' : [],
	'price' : low,
	'level' : 5,
	'hp-up': 15,
	'mp-up': 15,
	'descr' : 'Reduces damage from Earth, Wind, or Fire attacks. Increase max HP and MP by 15'
},
#------------------------------------------------------------- 6 ---------------------------------------------------------------
{
	'name' : 'Plastic Cloak',
	'def' : 8,
	'resist' : [],
	'negate' : [Element.Electric],
	'price' : low,
	'level' : 6,
	'hp-up': 0,
	'mp-up': 0,
	'descr' : 'Completley negates all Electric damage.'
},
{
	'name' : 'Claric\'s Jacket',
	'def' : 7,
	'resist' : [],
	'negate' : [],
	'price' : low,
	'level' : 6,
	'hp-up': 50,
	'mp-up': 0,
	'rest-up' : True,
	'descr' : 'Plus 50 to Max HP and doubles the effect of restoritave spells.'
},
#------------------------------------------------------------- 7 ---------------------------------------------------------------
{
	'name' : 'Prismatic Robes',
	'def' : 12,
	'resist' : [Element.Void, Element.Light],
	'negate' : [],
	'price' : low,
	'level' : 7,
	'hp-up': 0,
	'mp-up': 0,
	'descr' : 'A cloak that reflects ten thousand different colors. Reduces Void and Light damage by half.'
},
{
	'name' : 'Cloak of Clarity',
	'def' : 10,
	'resist' : [],
	'negate' : [StatusEffect.Sleep, StatusEffect.Confusion],
	'price' : low,
	'level' : 7,
	'hp-up': 0,
	'mp-up': 0,
	'descr' : 'Worn by the sages of long past era. Completely negates the effects of Confusion and Sleep.'
},


]