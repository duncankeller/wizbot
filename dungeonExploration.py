from rpgIncludes import * 
import time
import asyncio

debugMode = False
dungeonThreadLogs = {}

def temp(a, b):
	pass

postCallback = temp

def setPostCallback(callback):
	global postCallback

	postCallback = callback

def findEnemy(enemyName):
	for enemy in fullEnemyList:
		if enemy['name'] == enemyName:
			return enemy
	print('ERROR, enemy not found: ' + enemyName)
	return {}

# ⏤ ⎹ ⎸  │ ▀ ▄ ░ ▒ ▓ ▔▁ ▏ ▕ ▢ ◢ ◣ ◤ ◥  ▐ ▌ ▖ ▗ ▘ ▝  █ 
async def renderDungeonUpdate(player, dungeonThread, pos, seq):
	global postCallback
	global debugMode

	prtintSeq  = ['','','']

	prtintSeq[0] += '▄'
	prtintSeq[1] += '█'
	prtintSeq[2] += '▀'

	cur = 0
	for step in seq:
		prtintSeq[0] += '▄▄'
		prtintSeq[2] += '▀▀'

		spacing = '⬛️'

		if pos == cur:
			if player['hp'] > 0:
				prtintSeq[1] += '🧙' if not debugMode else 'o'
			else:
				prtintSeq[1] += '⚰'  if not debugMode else 'x'
		elif step == '':
			prtintSeq[1] += spacing  if not debugMode else '|'
		elif pos < cur - 1:
			prtintSeq[1] += spacing 
		elif step == 'chest':
			prtintSeq[1] += '💰' if not debugMode else 'g'
		else :
			if pos > cur:
				prtintSeq[1] += '☠'  if not debugMode else 'X'
			else:
				enemy = findEnemy(step)
				prtintSeq[1] += enemy['icon']  if not debugMode else 'e'

		cur += 1

	prtintSeq[1] += '█'

	hpPercent = float(player['hp'] / player['maxHp'])
	mpPercent = float(player['mp'] / player['maxMp'])
	barSize = 12
	hpBar = ''
	mpBar = ''
	hpBar += '▓' * int(barSize * hpPercent)
	hpBar += '▒' * int(barSize - (barSize * hpPercent))
	mpBar += '▓' * int(barSize * mpPercent)
	mpBar += '▒' * int(barSize - (barSize * mpPercent))

	finalMsg = ''
	finalMsg += ('HP: ' + str(player['hp']) + '/' + str(player['maxHp'])) + '\n'
	finalMsg += ('MP: ' + str(player['mp']) + '/' + str(player['maxMp'])) + '\n'

	finalMsg += (prtintSeq[0]) + '\n'
	finalMsg += (prtintSeq[1]) + '\n'
	finalMsg += (prtintSeq[2])

	if not debugMode:
		finalMsg += '\n'
		remaining = 5
		for subMsg in dungeonThreadLogs[dungeonThread.id]:
			finalMsg += '> ' + subMsg + '\n'
			remaining -= 1
		for index in range(remaining):
			finalMsg += '> ' + '\n'

		await postCallback(dungeonThread, finalMsg)
	else:
		print(finalMsg)


async def dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, notif):
	global postCallback
	global debugMode
	
	if not debugMode:
		dungeonThreadLogs[dungeonThread.id].append(notif);
		if len(dungeonThreadLogs[dungeonThread.id]) > 5:
			dungeonThreadLogs[dungeonThread.id].pop(0)

		await renderDungeonUpdate(player, dungeonThread, dungeonPos, dungeonSeq)
		time.sleep(2)
	else:
		print(notif)
		time.sleep(0.2)

def addTrinket(player, trinketKey):
	pass
	#global stateMap
	#global debugMode
	#
	#if not debugMode:
	#	author = player['name']
	#	guild = player['guildID']
	#	if trinketKey not in stateMap[guild][author][data_trinkets]:
	#		stateMap[guild][author][data_trinkets][trinketKey] = 0
	#	stateMap[guild][author][data_trinkets][trinketKey] = stateMap[guild][author][data_trinkets][trinketKey] + 1

def addWizcoin(player, amnt):
	player['coin'] += amnt

def addWizcoinToWallet(player, amnt):
	pass
	#global stateMap
	#global debugMode
	#
	#if not debugMode:
	#	author = player['name']
	#	guild = player['guildID']
	#	stateMap[guild][author][state_wizcoinCount] = stateMap[guild][author][state_wizcoinCount] + int(amount)

def addDungeonToCompletedList(player, dungeon):
	pass
	#global stateMap
	#global debugMode

	#if not debugMode:
	#	author = player['name']
	#	guild = player['guildID']
	#	alreadyAdded = dungeon in stateMap[guild][author][data_adventure][data_completedDungeons]

	#	if not alreadyAdded:
	#		stateMap[guild][author][data_adventure][data_completedDungeons].append(dungeon)
	#	return alreadyAdded


async def executeTurn(player, dungeonThread, dungeonPos, dungeonSeq, enemy):
	global debugMode

	# check player status effects
	playerStatus = player['status']
	canAttack = True
	canCastMagic = True

	if playerStatus == StatusEffect.Burn:
		dmgAmnt = int(player['maxHp'] / 18)
		dmgAmnt = 1 if dmgAmnt == 0 else dmgAmnt
		if random.randrange(0, 100) < 50:
			player['hp'] -= dmgAmnt
			pointsStr = 'points' if dmgAmnt > 1 else 'point'
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' took ' + str(dmgAmnt) + ' ' + pointsStr + ' of dmg due to Burn')
	elif playerStatus == StatusEffect.Poison:
		dmgAmnt = int(player['maxHp'] / 40)
		dmgAmnt = 1 if dmgAmnt == 0 else dmgAmnt
		player['hp'] -= dmgAmnt
		pointsStr = 'points' if dmgAmnt > 1 else 'point'
		await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' took ' + str(dmgAmnt) + ' ' + pointsStr + ' of dmg due to Poison')
	elif playerStatus == StatusEffect.Sleep:
		if random.randrange(0, 100) < 40:
			player['status'] = StatusEffect.None_
			player['status-time'] = 0
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' woke up from their slumber')
		else:
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' is fast asleep')
			canAttack = False
	elif playerStatus == StatusEffect.Paralisis:
		if random.randrange(0, 100) < 50 and player['status-time'] > 1:
			canAttack = False
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' was restricted due to Paralisis')
	elif playerStatus == StatusEffect.Confusion:
		await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' is Confused')
		randnum = random.randrange(0, 100)
		if randnum < 20:
			dmgAmnt = int(player['maxHp'] / 16)
			dmgAmnt = 1 if dmgAmnt == 0 else dmgAmnt
			player['hp'] -= dmgAmnt
			pointsStr = 'points' if dmgAmnt > 1 else 'point'
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' hit themselves for ' + str(dmgAmnt) + ' ' + pointsStr + ' of dmg')
			canAttack = False
		elif randnum < 30:
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' is trapped in a daze') 
			canAttack = False
	elif playerStatus == StatusEffect.Freeze:
		randnum = random.randrange(0, 100)
		if randnum < 30:
			dmgAmnt = int(player['maxHp'] / 24)
			dmgAmnt = 1 if dmgAmnt == 0 else dmgAmnt
			player['hp'] -= dmgAmnt
			pointsStr = 'points' if dmgAmnt > 1 else 'point'
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' took ' + str(dmgAmnt) + ' ' + pointsStr + ' of dmg due to Freeze')
		if randnum < 60:
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' is Frozen shock still')
			canAttack = False
	elif playerStatus == StatusEffect.Freeze:
		canCastMagic = False
		await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' is Silenced and unable to use MP!')

	# check death
	if player['hp'] <= 0:
		bestRevive = {}
		for item in player['inv']:
			if 'revive' in item:
				if bestRevive == {}:
					bestRevive = item
				elif item['revive'] > bestRevive['revive']:
					bestRevive = item
		if bestRevive != {}:
			amnt = float(bestRevive['revive'])  * 0.01 # human readible percentage to multiplicative percentage
			player['hp'] = int(player['maxHp'] * amnt)
			player['status'] = StatusEffect.None_
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' was almost felled by ' + statusEffectStr[playerStatus] + '...')
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, 'But ' + player['name'] + ' was healed by a revive stone!')
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, 'Revive stone shattered.')
			player['inv'] -= bestRevive
		else:
			player['hp'] = 0
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' was felled by ' + statusEffectStr[playerStatus])
			return

	# check status effect over
	if playerStatus != StatusEffect.None_:
		player['status-time'] -= 1
		if player['status-time'] == 0:
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, statusEffectStr[playerStatus] + ' wore off')
			player['status'] = StatusEffect.None_
			playerStatus = StatusEffect.None_

	# figre out what spells can be used
	enemyWeaknesses = enemy['weak'] if 'weak' in enemy else []
	enemyResistances = enemy['resist'] if 'resist' in enemy else []
	wand = player['wand']
	cloak = player['cloak']
	mpReduction = 0
	if 'mp-red' in wand['mods']:
		mpReduction = wand['mods']['mp-red']

	spellToUse = findSpell(player, 'Plonk')
	itemToUse = {}

	ignoreResist = False
	if 'ignore-resist' in wand['mods']:
		if random.randrange(0, 100) < wand['mods']['ignore-resist']:
			ignoreResist = True

	effectiveDamagingSpells = []
	effectiveStatusEffectSpells = []
	effectiveSpells = []
	standardSpells = []
	inffectiveSpells = []
	bestHealSpell = {}
	dispellSpell = {}

	bestHealingItem = {}
	bestMpItem = {}

	# figure out usable items
	for item in player['inv']:
		if 'heal-hp' in item:
			if player['hp'] + item['heal-hp'] <= player['maxHp']:
				if bestHealingItem == {}:
					bestHealingItem = item
				else:
					if item['heal-hp'] > bestHealingItem['heal-hp']:
						bestHealingItem = item
		if 'heal-mp' in item:
			if player['mp'] + item['heal-mp'] <= player['maxMp']:
				if bestMpItem == {}:
					bestMpItem = item
				else:
					if item['heal-mp'] > bestMpItem['heal-mp']:
						bestMpItem = item

	# figure out usable spells
	if canCastMagic:
		for weakness in enemyWeaknesses:
			if 'inflict' in spell and enemy['status'] != StatusEffect.None_:
				continue

			spell = findSpellElement(player, weakness) 
			if spell != {} and spell['mp-cost'] - mpReduction <= player['mp'] and spell['attack'] == True:
				effectiveSpells.append(spell)
				effectiveDamagingSpells.append(spell)
			spell = findSpellStatusEffect(player, weakness)
			if spell != {} and spell['mp-cost'] - mpReduction <= player['mp'] and spell['attack'] == True:
				effectiveSpells.append(spell)
				effectiveStatusEffectSpells.append(spell)

		if not ignoreResist:
			for resistance in enemyResistances:
				spell = findSpellElement(player, resistance) 
				if spell != {} and spell['mp-cost'] - mpReduction <= player['mp'] and spell['attack'] == True:
					inffectiveSpells.append(spell)
				spell = findSpellStatusEffect(player, resistance) 
				if spell != {} and spell['mp-cost'] - mpReduction <= player['mp'] and spell['attack'] == True:
					inffectiveSpells.append(spell)

		for spell in player['spells']:
			if spell['mp-cost'] - mpReduction <= player['mp'] and spell['attack'] == True:
				for weakness in enemyWeaknesses:
					if spell['element'] == weakness:
						continue
					if 'inflict' in spell and spell['inflict'][0] == weakness:
						continue
				if not ignoreResist:
					for resistance in enemyResistances:
						if spell['element'] == resistance:
							continue
						if 'inflict' in spell and spell['inflict'][0] == resistance:
							continue
					if 'inflict' in spell and enemy['status'] != StatusEffect.None_:
						continue
				standardSpells.append(spell)

		healBonus = 1
		if 'rest-up' in spell:
			healBonus = 2

		if player['hp'] < player['maxHp']:
			for spell in player['spells']:
				if spell['mp-cost'] - mpReduction <= player['mp'] and 'heal-hp' in spell:
					if bestHealSpell != {}:
						if spell['heal-hp'] > bestHealSpell['heal-hp'] and player['hp'] + spell['heal-hp'] * healBonus <= player['maxHp']:
							bestHealSpell = spell
					else:
						if player['hp'] + spell['heal-hp'] * healBonus <= player['maxHp']:
							bestHealSpell = spell

		if playerStatus != StatusEffect.None_:
			for spell in player['spells']:
				if spell['mp-cost'] - mpReduction <= player['mp']:
					if 'dispell' in spell and spell['dispell'] == True:
						dispellSpell = spell
						break

		# decide attack based on personality
		personality = player['personality']
		if personality == PlayerPersonalitiy.Cautious:
			if bestHealSpell != {}:
				spellToUse = bestHealSpell
			elif bestHealingItem != {}:
				itemToUse = bestHealingItem
			elif dispellSpell != {}:
				spellToUse = dispellSpell
			elif bestMpItem != {}:
				itemToUse = bestMpItem
			elif player['mp'] > player['maxMp'] * 0.75 and len(effectiveSpells) > 0:
				spellToUse = effectiveSpells[random.randrange(0, len(effectiveSpells))]
		elif personality == PlayerPersonalitiy.Agressive:
			if player['mp'] < player['maxMp'] * 0.2 and bestMpItem != {}:
				itemToUse = bestMpItem
			elif  len(effectiveDamagingSpells) > 0:
				spellToUse = effectiveDamagingSpells[random.randrange(0, len(effectiveDamagingSpells))]
			elif len(effectiveStatusEffectSpells) > 0:
				spellToUse = effectiveStatusEffectSpells[random.randrange(0, len(effectiveStatusEffectSpells))]
			elif player['hp'] < player['maxHp'] * 0.2 and bestHealSpell != {}:
				spellToUse = bestHealSpell
			elif player['hp'] < player['maxHp'] * 0.2 and bestHealingItem != {}:
				itemToUse = bestHealingItem
			elif len(standardSpells) > 0:
				spellToUse = standardSpells[random.randrange(0, len(standardSpells))]
		elif personality == PlayerPersonalitiy.Devious:
			if bestHealSpell != {} and player['hp'] < player['maxHp'] * 0.4:
				spellToUse = bestHealSpell
			elif bestHealingItem != {} and player['hp'] < player['maxHp'] * 0.4:
				itemToUse = bestHealingItem
			elif dispellSpell != {}:
				spellToUse = dispellSpell
			elif player['mp'] < player['maxMp'] * 0.4 and bestMpItem != {}:
				itemToUse = bestMpItem
			elif  len(effectiveStatusEffectSpells) > 0:
				spellToUse = effectiveStatusEffectSpells[random.randrange(0, len(effectiveStatusEffectSpells))]
			elif  len(effectiveDamagingSpells) > 0 and player['mp'] > player['maxMp'] * 0.5:
				spellToUse = effectiveDamagingSpells[random.randrange(0, len(effectiveDamagingSpells))]
			elif  len(standardSpells) > 0 and player['mp'] > player['maxMp'] * 0.8:
				spellToUse = standardSpells[random.randrange(0, len(standardSpells))]
		elif personality == PlayerPersonalitiy.Conservative:
			if bestHealSpell != {} and player['hp'] < player['maxHp'] * 0.2:
				spellToUse = bestHealSpell
			elif dispellSpell != {} and player['mp'] > player['maxMp'] * 0.35:
				spellToUse = dispellSpell
			elif len(effectiveSpells) > 0 and player['mp'] > player['maxMp'] * 0.6:
				spellToUse = effectiveSpells[random.randrange(0, len(effectiveSpells))] 
		elif personality == PlayerPersonalitiy.Balanced:
			if bestHealSpell != {} and player['hp'] < player['maxHp'] * 0.4:
				spellToUse = bestHealSpell
			elif bestHealingItem != {} and player['hp'] < player['maxHp'] * 0.4:
				itemToUse = bestHealingItem
			elif dispellSpell != {}:
				spellToUse = dispellSpell
			elif len(effectiveSpells) > 0 and player['mp'] > player['maxMp'] * 0.4:
				spellToUse = effectiveSpells[random.randrange(0, len(effectiveSpells))] 
			elif player['mp'] < player['maxMp'] * 0.3 and bestMpItem != {}:
				itemToUse = bestMpItem
			elif len(standardSpells) > 0 and player['mp'] > player['maxMp'] * 0.6:
				spellToUse = standardSpells[random.randrange(0, len(standardSpells))]

		# do action (either item or spell)
		if itemToUse != {}:
			player['inv'] -= itemToUse
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' used ' + itemToUse['name'])

			if 'heal-hp' in itemToUse:
				player['hp'] += itemToUse['heal-hp']
				if player['hp'] > player['maxHp']:
					player['hp'] = player['maxHp']
				await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' recovered ' + str(itemToUse['heal-hp']) + ' HP!')
			if 'heal-mp' in itemToUse:
				player['mp'] += itemToUse['heal-mp']
				if player['mp'] > player['maxMp']:
					player['mp'] = player['maxMp']
				await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' recovered ' + str(itemToUse['heal-mp']) + ' MP!')
		else:
			spell = spellToUse

			statusEffectBoost = 0
			if 'status-up' in player['wand']:
				statusEffectBoost = player['wand']['status-up']

			# apply spell effects
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' casts ' + spell['name'])
			player['mp'] -= (spell['mp-cost'] - mpReduction)

			if 'heal-hp' in spell:
				player['hp'] += spell['heal-hp'] * healBonus
				await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' recovered ' + str(spell['heal-hp'] * healBonus) + ' HP!')

			if 'dispell' in spell and spell['dispell'] == True:
				player['status-time'] = 0
				player['status'] = StatusEffect.None_
				await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, playerStatus + ' was dispelled')


			missed = False
			if 'evade' in enemy:
				if random.randrange(0, 100) > enemy['evade']:
					missed = True
					await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' missed!')

			if not missed:
				if 'silence' in spell and spell['silence'] == True:
					enemy['status'] = StatusEffect.Silence
					enemy['status-time'] = 3
					await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' was silenced')


				statusToInflict = StatusEffect.None_
				statusToInflictChance = 0
				statusToInflictTurns = 0

				if 'inflict' in spell:
					statusToInflict = spell['inflict'][0]
					statusToInflictChance = spell['inflict'][1]
					statusToInflictTurns = spell['inflict'][2]
				elif enemy['status'] == StatusEffect.None_:
					if 'inflict-chance' in wand['mods']:
						statusToInflict = wand['mods']['inflict-chance'][0]
						statusToInflictChance = wand['mods']['inflict-chance'][1]
						statusToInflictTurns = wand['mods']['inflict-chance'][2]

				if statusToInflict != StatusEffect.None_:
					if 'status-up-on' in player['wand']:
						for statItr in player['wand']['status-up-on']:
							if statItr[0] == statusToInflict:
								statusEffectBoost += statItr[1]

					if statusToInflict in enemyResistances and not ignoreResist:
						await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, 'but ' + enemy['name'] + ' resisted it')
					elif statusToInflict in enemyWeaknesses:
						await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, 'and ' + enemy['name'] + ' is weak to it...')
						if random.randrange(0, 100) < (statusToInflictChance * 2) + statusEffectBoost:
							enemy['status'] = statusToInflict
							enemy['status-time'] = statusToInflictTurns
							await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' was ' + statusEffectPastTense[statusToInflict] + '!')
						else:
							await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, 'but it didn\'t work')
					elif random.randrange(0, 100) < statusToInflictChance + statusEffectBoost:
						enemy['status'] = statusToInflict
						enemy['status-time'] = statusToInflictTurns
						await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' was ' + statusEffectPastTense[statusToInflict] + '!')
				


				# apply damage
				if 'does-dmg' in spell and spell['does-dmg'] == True:
					element = spell['element']
					damageMin = wand['dmg'][0]
					damageMax = wand['dmg'][1] + 1
					damageTimes = wand['dmg'][2]

					effective = False
					weak = False

					if element in enemyWeaknesses:
						effective = True

					if element in enemyResistances and not ignoreResist:
						weak = True

					if 'atk-up' in player['wand']:
						damageMin += player['wand']['atk-up-amnt'][0]
						if damageMin > damageMax:
							damageMin = damageMax

						if random.randrange(0, 100) < player['wand']['atk-up-amnt'][1]:
							player['wand']['atk-up-amnt'] += 1

					for i in range(damageTimes):
						dmg = random.randrange(damageMin, damageMax)

						if effective:
							bonus = dmg / 2
							dmg += bonus

						if weak:
							dmg /= 2


						if 'element-boost' in cloak and cloak['element-boost'] == element:
							damage += int(damage / 3)


						if 'element-boost' in wand['mods'] and dmg > 0:
							for boostedElement in wand['mods']['element-boost']:
								if element == boostedElement[0]:
									dmg += boostedElement[1]


						if random.randrange(0, 100) < wand['crit'] and not weak and dmg > 0:
							dmg *= 2
							await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, 'Critical Hit!')


							if 'fx-on-crit' in wand['mods']:
								fx = wand['mods']['fx-on-crit']
								if fx[0] == 'status-fx':

									if 'status-up-on' in player['wand']:
										for statItr in player['wand']['status-up-on']:
											if statItr[0] == statusToInflict:
												statusEffectBoost += statItr[1]

									if random.randrange(0, 100) < fx[2] + statusEffectBoost:
										enemy['status'] = statusToInflict
										enemy['status-time'] = forTurns
										await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' was ' + statusEffectPastTense[statusToInflict] + '!')
								elif fx[0] == 'mp-plus':
									player['mp'] += fx[1]
									if player['mp'] > player['maxMp']:
										player['mp'] = player['maxMp']
									await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' recovered ' + str(fx[1]) + ' MP!')
								elif fx[0] == 'hp-plus':
									player['hp'] += fx[1]
									if player['hp'] > player['maxHp']:
										player['hp'] = player['maxHp']
									await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' recovered ' + str(fx[1]) + ' HP!')


						enemy['hp'] -= dmg
						if enemy['hp'] < 0:
							enemy['hp'] = 0

						await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' took ' + str(dmg) + ' points of dmg')

					if effective == True:
						await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, 'It was Super Effective!')
					elif weak == True:
						await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, 'It wasn\'t very effective')


	# check enemy health
	if enemy['hp'] <= 0:
		return

	# check enemy status effects
	enemyStatus = enemy['status']
	canAttack = True
	canCastMagic = True

	if enemyStatus == StatusEffect.Burn:
		dmgAmnt = int(enemy['maxHp'] / 18)
		dmgAmnt = 1 if dmgAmnt == 0 else dmgAmnt
		if random.randrange(0, 100) < 50:
			enemy['hp'] -= dmgAmnt
			pointsStr = 'points' if dmgAmnt > 1 else 'point'
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' took ' + str(dmgAmnt) + ' ' + pointsStr + ' of dmg due to Burn')
	elif enemyStatus == StatusEffect.Poison:
		dmgAmnt = int(enemy['maxHp'] / 40)
		dmgAmnt = 1 if dmgAmnt == 0 else dmgAmnt
		enemy['hp'] -= dmgAmnt
		pointsStr = 'points' if dmgAmnt > 1 else 'point'
		await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' took ' + str(dmgAmnt) + ' ' + pointsStr + ' of dmg due to Poison')
	elif enemyStatus == StatusEffect.Sleep:
		if random.randrange(0, 100) < 40:
			enemy['status'] = StatusEffect.None_
			enemy['status-time'] = 0
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' woke up from their slumber')
		else:
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' is fast asleep')
			canAttack = False
	elif enemyStatus == StatusEffect.Paralisis:
		if random.randrange(0, 100) < 50 and enemy['status-time'] > 1:
			canAttack = False
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' was restricted due to Paralisis')
	elif enemyStatus == StatusEffect.Confusion:
		await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' is Confused')
		randnum = random.randrange(0, 100)
		if randnum < 20:
			dmgAmnt = int(enemy['maxHp'] / 16)
			dmgAmnt = 1 if dmgAmnt == 0 else dmgAmnt
			enemy['hp'] -= dmgAmnt
			pointsStr = 'points' if dmgAmnt > 1 else 'point'
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' hit themselves for ' + str(dmgAmnt) + ' ' + pointsStr + ' of dmg')
			canAttack = False
		elif randnum < 30:
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' is trapped in a daze') 
			canAttack = False
	elif enemyStatus == StatusEffect.Freeze:
		randnum = random.randrange(0, 100)
		if randnum < 30:
			dmgAmnt = int(enemy['maxHp'] / 24)
			dmgAmnt = 1 if dmgAmnt == 0 else dmgAmnt
			enemy['hp'] -= dmgAmnt
			pointsStr = 'points' if dmgAmnt > 1 else 'point'
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' took ' + str(dmgAmnt) + ' ' + pointsStr + ' of dmg due to Freeze')
		if randnum < 60:
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' is Frozen shock still')
			canAttack = False
	elif enemyStatus == StatusEffect.Freeze:
		canCastMagic = False
		await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' is Silenced and unable to use MP!')

	# check death
	if enemy['hp'] <= 0:
		enemy['hp'] = 0
		await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' was felled by ' + statusEffectStr[playerStatus])
		return

	# check status effect over
	if enemyStatus != StatusEffect.None_:
		enemy['status-time'] -= 1
		if enemy['status-time'] == 0:
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, statusEffectStr[playerStatus] + ' wore off for ' + enemy['name'])
			enemy['status'] = StatusEffect.None_
			enemyStatus = StatusEffect.None_

	# do enemy attack
	attacks = enemy['attacks']

	possible = []
	for attack in attacks:
		for i in range(attack['chance']):
			possible.append(attack)

	attack = possible[random.randrange(0, len(possible))]

	if 'element' not in attack:
		attack['element'] = Element.None_

	doAttack = False


	if 'mp-cost' in attack and 'mp' in enemy:
		if enemy['status'] == StatusEffect.Silence:
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' tried to cast magic but was Silenced')
		elif enemy['mp'] >= attack['mp-cost']:
			enemy['mp'] -= attack['mp-cost']
			doAttack = True
		else:
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' tried to cast magic but didn\'t have enough MP')
	else:
		doAttack = True

	if doAttack == True and canAttack == True:
		await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' ' + attack['descr'])

		if 'self-effect' in attack:
			selfEffect = attack['self-effect'][0]
			chance = attack['self-effect'][1]
			if random.randrange(0, 100) < chance:
				await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' was ' + statusEffectPastTense[selfEffect])
			else:
				await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, 'it was not effective')

		didDamage = False
		canDoDamage = False
		if 'dmg' in attack:
			canDoDamage = True
			damageMin = attack['dmg'][0]
			damageMax = attack['dmg'][1]
			damageTimes = attack['dmg'][2]

			for i in range(damageTimes):
				damage = random.randrange(damageMin, damageMax + 1)

				if 'boosted-atk' in attack:
					damage += attack['boosted-atk']

				if 'armor' in enemy:
					damage -= enemy['armor']
					damage = 0 if damage < 0 else damage

				if 'negate' in cloak and attack['element'] in cloak['negate']:
					damage = 0
					await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + '\'s cloak negated the attack')
					break


				if damage > 0:
					didDamage = True
					ogDamag = damage

					if player['status'] == StatusEffect.Protect:
						damage /= 2
					elif player['status'] == StatusEffect.Weaken:
						damage *= 2

					if attack['element'] in cloak['resist']:
						damage /= 2

					dmgReduce = cloak['def']
					damage -= dmgReduce
					if damage < 0:
						damage = 0

					if player['item'] != {}:
						if 'dmg-reduction' in player['item']:
							damage -= player['item']['dmg-reduction'] 
							if damage < 0:
								damage = 0

					player['hp'] -= damage
					if player['hp'] < 0:
						player['hp'] = 0

					await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' was hit for ' + str(damage) + ' points of dmg')


					if player['status'] == StatusEffect.Protect:
						await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, 'dmg was reduced by ' + str(ogDamag - damage) + ' due to Protect')
					elif player['status'] == StatusEffect.Weaken:
						await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, 'dmg was increased by ' + str(damage) + ' due to Weaken')			
					if attack['element'] in cloak['resist']:
						await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + '\'s cloak weakened the attack')

					if 'drain' in attack:
						if attack['drain'] == 'hp':
							drainAmnt = damage
							enemy['hp'] += drainAmnt
							enemy['hp'] = enemy['maxHp'] if enemy['hp'] > enemy['maxHp'] else enemy['hp']
							await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' drained ' + str(amnt) + ' HP from ' + player['name'])

		if 'raise-atk' in attack:
			amnt = attack['raise-atk']
			if 'boosted-atk' not in attack:
				attack['boosted-atk'] = 0
			attack['boosted-atk'] += amnt
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' boosted it\'s attack by ' + str(amnt) + ' points.')

		if 'drain-mp' in attack:
			minAmnt = attack['drain-mp'][0]
			maxAmnt = attack['drain-mp'][1] + 1
			drainAmnt = random.randrange(minAmnt, maxAmnt)
			if drainAmnt > 0:
				enemy['mp'] += drainAmnt
				player['mp'] -= drainAmnt
				enemy['mp'] = enemy['maxMp'] if enemy['mp'] > enemy['maxMp'] else enemy['mp']
				player['mp']  = 0 if player['mp'] < 0 else player['mp'] 
				await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' drained ' + str(amnt) + ' MP from ' + player['name'])
			else:
				await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, '... but nothing happened.')


		if 'hp-up' in attack:
			if enemy['hp'] < enemy['maxHp']:
				amnt = enemy['hp-up'] 
				enemy['hp'] += amnt
				if enemy['hp'] > enemy['maxHp']:
					enemy['hp'] = enemy['maxHp']
				await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' recovered ' + str(amnt) + ' HP')

		if 'armor-up' in attack:
			if 'armor' not in enemy:
				enemy['armor'] = 0
			enemy['armor'] += enemy['armor-up']
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' raised its defenses by ' + str(enemy['armor-up']) + ' points')

		if 'effect' in attack:
			canDoDamage = True
			statusEffectType = attack['effect'][0]
			statusEffectChance = attack['effect'][1]
			resisted = False
			effectChance = 100

			if statusEffectType in cloak['negate']:
				resisted = True
			elif statusEffectType in cloak['resist']:
				effectChance = 50
				await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' negated ' + statusEffectStr[statusEffectType] + ' with their cloak')

			if player['item'] != {}:
				if 'resist' in player['item'] and player['item']['resist'] == statusEffectType:
					resisted = True
					await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' negated ' + statusEffectStr[statusEffectType] + ' by using ' + player['item']['name'])

			if random.randrange(0, effectChance) < statusEffectChance and not resisted:
				if not playerCanResist(player, statusEffectType):
					playerGivenStatusEffect(player, statusEffectType)
					player['status-time'] = getStatusTime(statusEffectType)
					await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' inflicted with ' + statusEffectStr[statusEffectType])
					didDamage = True

		if not didDamage and canDoDamage:
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' missed')

	# check death
	if player['hp'] <= 0:
		bestRevive = {}
		for item in player['inv']:
			if 'revive' in item:
				if bestRevive == {}:
					bestRevive = item
				elif item['revive'] > bestRevive['revive']:
					bestRevive = item
		if bestRevive != {}:
			amnt = float(bestRevive['revive'])  * 0.01 # human readible percentage to multiplicative percentage
			player['hp'] = int(player['maxHp'] * amnt)
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' was almost felled by ' + enemy['name'] + '...')
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, 'But ' + player['name'] + ' was healed by a revive stone!')
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, 'Revive stone shattered.')
			player['inv'] -= bestRevive
		else:
			player['hp'] = 0
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' was felled by ' + enemy['name'])
			return

async def doExploreDungeon(playerTemplate, dungeonTemplate, dungeonThread):
	global dungeonThreadLogs
	global debugMode

	dungeonThreadLogs[dungeonThread.id] = []


	dungeon = copy.deepcopy(dungeonTemplate)
	player = copy.deepcopy(playerTemplate)

	dungeonSeq = dungeonTemplate['sequence']

	player['wand']['atk-up-amnt'] = 0

	player['maxHp'] += player['cloak']['hp-up']
	player['maxMp'] += player['cloak']['mp-up']

	dungeonPos = 0
	for stepNum in range(len(dungeonSeq) - 1):
		step = dungeonSeq[stepNum + 1]

		await renderDungeonUpdate(player, dungeonThread, dungeonPos, dungeonSeq)

		if step == dungeonSeq[len(dungeonSeq) - 1]:
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, 'The dungeon boss lurks just ahead')

		if step == '':
			time.sleep(1)
		elif step == 'chest':
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, 'Found a chest!')

			itemChance = 20
			if random.randrange(0, 100) < itemChance:
				itemName = dungeon['item-rewards'][random.randrange(0, len(dungeon['item-rewards']))]
				item = getItemByName(itemName)
				player['inv'].append(item)
				await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' got item: ' + itemName)
			else:
				amnt = dungeon['chest-wizcoin'][random.randrange(0, len(dungeon['chest-wizcoin']))]
				addWizcoin(player, amnt)
				await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' got Wizcoin. (' + str(amnt) + ')')
		else:
			enemy = copy.deepcopy(findEnemy(step))
			enemy['status'] = StatusEffect.None_
			enemy['status-time'] = 0
			enemy['maxHp'] = enemy['hp']
			enemy['maxMp'] = enemy['mp']

			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, 'Encountered a ' + enemy['name'] )

			if enemy not in player['seen-enemies']:
				player['seen-enemies'].append(enemy)

			done = False
			while not done:
				await executeTurn(player, dungeonThread, dungeonPos, dungeonSeq, enemy)
				await renderDungeonUpdate(player, dungeonThread, dungeonPos, dungeonSeq)
				time.sleep(0.5)

				if player['hp'] <= 0:
					done = True
				elif enemy['hp'] <= 0:
					done = True
			if player['hp'] <= 0:
				break
			else:
				await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' was defeated!')
				randNum = random.randrange(0, 100)
				if randNum < 15:
					await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' dropped something...')
					trinkname = list(trinketMap.keys())[random.randrange(0, len(trinketMap))]
					addTrinket(player, trinkname)
					await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, 'Got Trinket! (' + trinkname + ')')
				elif randNum < 70:
					await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, enemy['name'] + ' dropped something...') 
					amnt = random.randrange(dungeon['enemy-wizcoin'][0], dungeon['enemy-wizcoin'][1])
					addWizcoin(player, amnt)
					await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, 'Got Wizcoin! (' + str(amnt) + ')')

				expGet = enemy['exp']
				player['exp'] += expGet
				while player['exp'] > requiredExp(player['lvl']):
					expReq = requiredExp(player['lvl'])
					player['exp'] -= expReq
					prevHp = player['maxHp'] 
					prevMp = player['maxMp'] 
					player['lvl'] += 1
					player['maxHp'] += 3
					player['maxMp'] += 2
					player = healAdventureCard(player)
					await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' leveled up!')
					await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' is now level ' + str(player['lvl']) + '. HP: ' + str(prevHp) + '->' + str(player['maxHp']) + '. MP: ' + str(prevMp) + '->' + str(player['maxMp'])) 

		dungeonPos += 1

	coinGet = player['coin']
	if player['hp'] <= 0:
		await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' collapsed in exhaustion.')
		await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' seems like they\'re done for...')
		await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, '... But was carried out of the dungeon at the last minute')

		if coinGet > 0:
			coinGet /= 3
			coinGet = int(coinGet)
			await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, str(coinGet) + ' Wizcoin fell out of ' + player['name'] + '\'s pockets on the way out')
			addWizcoinToWallet(player, coinGet)

		return


	await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' defeated the dungeon boss!')

	if not addDungeonToCompletedList(player, dungeon['name']):
		player['wizgems'] += 1
		await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' found a coveted Wizgem!')

	if 'boss-reward-wizcoin' in dungeon:
		amnt = dungeon['boss-reward-wizcoin']
		addWizcoin(player, amnt)
		await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' recived ' + str(amnt) + ' Wizcoin from beating the boss!')

	coinGet = player['coin']
	player['coin'] = 0

	if coinGet > 0:
		await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' got ' + str(coinGet) + ' Wizcoin overall.')
		addWizcoinToWallet(player, coinGet)


	await dungeonNotify(player, dungeonThread, dungeonPos, dungeonSeq, player['name'] + ' safely exited the dungeon, prizes in tow.')


#def exploreDungeon(user, dungeon):
#	global stateMap
#
#	stateMap[guild][author][state_wizcoinCount] = stateMap[guild][author][state_wizcoinCount] + int(amount)

class DummyThread:
	id = 0

def debugExplore():
	global debugMode

	debugMode = True
	player = genearateDebugAdventuerer(10)
	player['exp'] = 10
	dungeon = dungeonList[0]

	dummy = DummyThread()
	asyncio.run(doExploreDungeon(player, dungeon, dummy))

async def debugExploreWithDiscord(dungeonThread):
	global debugMode

	debugMode = False
	player = genearateDebugAdventuerer(10)
	player['exp'] = 10
	dungeon = dungeonList[0]
	await doExploreDungeon(player, dungeon, dungeonThread)


#debugExplore()