from igdb.igdbapi_pb2 import GameResult
from igdb.igdbapi_pb2 import ScreenshotResult
from igdb.igdbapi_pb2 import PlatformResult
from igdb.wrapper import IGDBWrapper
import json
import requests
import random


igdbClientID = 'ixxdbb076ddfrhbnsjwi3jnk230dnm'
igdbToken = 'NOTREAD'
with open('../igdbtoken.text', 'r') as file:
    token = file.read().replace('\n', '')

def isMobilePlat(platforms):
	for plat in platforms:
		if plat.name.lower() == 'ios' or plat.name.lower() == 'android' or plat.name.lower() == 'airconsole':
			return True
	return False


def getGameSetData():
	r = requests.post("https://id.twitch.tv/oauth2/token?client_id=ixxdbb076ddfrhbnsjwi3jnk230dnm&client_secret=naqega54d5nenrcx6mz9bnhz9zm4qu&grant_type=client_credentials")
	accessToken = json.loads(r._content)['access_token']

	wrapper = IGDBWrapper(igdbClientID, accessToken)

	count = 0
	countResult = wrapper.api_request(
        'platforms/count', 
        'limit 1;')
	countResultJson = json.loads(countResult)
	count = countResultJson['count']
	print(str(count) + ' platform results parsed from igdb')

	addedConsoles = []

	platforms = []
	for i in range(4):

		while True:
			randIdx = random.randrange(0, count)
			offset = randIdx
			byteArr = wrapper.api_request(
				'platforms.pb', 
				'fields id, name; offset ' + str(offset) + '; limit 1;'
			)
			platform_message = PlatformResult()
			platform_message.ParseFromString(byteArr)

			print(platform_message.platforms)
			countResult = wrapper.api_request(
		        'games/count', 
		        'where platforms=' + str(platform_message.platforms[0].id) + ';')
			countResultJson = json.loads(countResult)
			gamecount = countResultJson['count']

			if gamecount > 3 and platform_message.platforms[0].id not in addedConsoles:
				platforms.append((platform_message.platforms[0].id, platform_message.platforms[0].name))
				addedConsoles.append(platform_message.platforms[0].id)
				break

	platformResults = {}
	addedGames = []
	
	for plat in platforms:
		count = 0
		countResult = wrapper.api_request(
	        'games/count', 
	        'where platforms=' + str(plat[0]) + ';')
		countResultJson = json.loads(countResult)
		count = countResultJson['count']

		games = []
		for i in range(3):

			while True:
				randIdx = random.randrange(0, count)
				offset = randIdx

				byteArr = wrapper.api_request(
					'games.pb', 
					'fields id, name, summary, platforms.name, screenshots; offset ' + str(offset) + '; limit 1; where platforms=' + str(plat[0]) + ';'
				)
				games_message = GameResult()
				games_message.ParseFromString(byteArr)

				gameReqest = games_message.games[0]

				print(gameReqest.name)

				if gameReqest.name not in addedGames:
					break

			#screenshot
			byteArr = wrapper.api_request(
				'screenshots.pb', 
				'fields url, width; limit 20; where game=' + str(gameReqest.id) + ';'
			)
			screenshot_message = ScreenshotResult()
			screenshot_message.ParseFromString(byteArr)

			screenshotURL = ''
			if screenshot_message.ByteSize() > 0:
				screenshotURL = screenshot_message.screenshots[0].url
				screenshotURL = screenshotURL.replace('t_thumb', 't_original')
				screenshotURL = 'https:' + screenshotURL

			addedGames.append(gameReqest.name)
			game = {}
			game['name'] = gameReqest.name
			summary = gameReqest.summary
			if len(summary) > 1400:
				summary = summary[:1400] + '...'
			game['summary'] = summary
			game['platform'] = plat[1]
			game['screenshot'] = screenshotURL
			game['price'] = random.randrange(20, 200)
			games.append(game)
		platformResults[plat[1]] = games

	return platformResults


def getGameData(date, requireImg, blockMobile):

	r = requests.post("https://id.twitch.tv/oauth2/token?client_id=ixxdbb076ddfrhbnsjwi3jnk230dnm&client_secret=naqega54d5nenrcx6mz9bnhz9zm4qu&grant_type=client_credentials")
	accessToken = json.loads(r._content)['access_token']

	wrapper = IGDBWrapper(igdbClientID, accessToken)

	count = 0
	countResult = wrapper.api_request(
        'games/count', 
        'where release_dates.y=' + str(date) + ';'
      )
	countResultJson = json.loads(countResult)
	count = countResultJson['count']
	print(str(count) + ' results parsed from igdb')

	while True:
		randIdx = random.randrange(0, count)
		offset = randIdx

		byteArr = wrapper.api_request(
			'games.pb', 
			'fields id, name, summary, platforms.name, screenshots; offset ' + str(offset) + '; limit 1; where release_dates.y=' + str(date) + ';'
		)
		games_message = GameResult()
		games_message.ParseFromString(byteArr)

		if games_message.ByteSize() == 0: # this shouldn't happen
			print('failed to get a came message, trying again with a different game. failed id: ' + str(randIdx))
			continue


		game = games_message.games[0]

		if blockMobile == True:
			if isMobilePlat(game.platforms):
				continue

		if len(game.screenshots) > 0 or requireImg == False:
			break



	#screenshot
	byteArr = wrapper.api_request(
		'screenshots.pb', 
		'fields url, width; limit 20; where game=' + str(game.id) + ';'
	)
	screenshot_message = ScreenshotResult()
	screenshot_message.ParseFromString(byteArr)

	screenshotURL = ''
	if screenshot_message.ByteSize() > 0:
		screenshotURL = screenshot_message.screenshots[0].url
		screenshotURL = screenshotURL.replace('t_thumb', 't_original')
		screenshotURL = 'https:' + screenshotURL

	plats = ''
	gameMessage = game.name + ', released in ' + str(date) + ' for these platforms:'
	for plat in game.platforms:
		gameMessage += ' ' + plat.name
		plats += ' ' + plat.name
	gameMessage += '\n'
	summary = game.summary
	if len(summary) > 1400:
		summary = summary[:1400] + '...'
	gameMessage += summary

	gameMessageStruct = {}
	gameMessageStruct['gameMessage'] = gameMessage
	gameMessageStruct['name'] = str(game.name)
	gameMessageStruct['platforms'] = plats
	gameMessageStruct['date'] = date

	if screenshotURL != '':
		gameMessageStruct['screenshot'] = screenshotURL

	return gameMessageStruct
