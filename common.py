import json
import os


trinketMap = {}
shenmueDataFilepath = 'trinkets.json'
if os.path.isfile(shenmueDataFilepath):
	with open(shenmueDataFilepath) as file:
		trinketMap = json.load(file)

		
rarityMap = {2 : 'Common', 1 : 'Rare', 0 : 'Ultra Rare'}

