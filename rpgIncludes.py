from itemCommon import * 
from enemies import * 
from dungeons import * 
from wands import * 
from spells import * 
from items import * 
from cloaks import * 
from common import *
import random

data_adventure = 'data_adventure'
data_adventureCard = 'data_adventureCard'
data_completedDungeons = 'data_completedDungeons'

def playerCanResist(player, statusEffect):
	return False

def playerGivenStatusEffect(player, statusEffect):
	player['status'] = statusEffect

def findSpell(player, name):
	for spell in player['spells']:
		if spell['name'] == name:
			return spell
	print('ERROR: couldnt find spell named ' + name)

def findSpellElement(player, element):
	for spell in player['spells']:
		if spell['element'] == element:
			return spell
	return {}

def findSpellStatusEffect(player, effect):
	for spell in player['spells']:
		if 'inflict' in spell and spell['inflict'][0] == effect:
			return spell
	return {}


def getStatusTime(effect):
	if effect == StatusEffect.Burn:
		return random.randrange(1, 4)
	elif effect == StatusEffect.Poison:
		return random.randrange(3, 7)
	elif effect == StatusEffect.Sleep:
		return random.randrange(1, 4)
	elif effect == StatusEffect.Paralisis:
		return random.randrange(2, 6)
	elif effect == StatusEffect.Confusion:
		return random.randrange(1, 4)
	elif effect == StatusEffect.Protect:
		return random.randrange(2, 6)
	elif effect == StatusEffect.Weaken:
		return random.randrange(2, 6)
	elif effect == StatusEffect.Freeze:
		return random.randrange(1, 5)
	elif effect == StatusEffect.Silence:
		return random.randrange(2, 7)

def getBestiaryMessage(enemy):
	msg = enemy['name'] + ' ' + enemy['icon'] + '\n```'
	msg += 'HP: ' + enemy['hp'] + '\n'
	msg += 'MP: ' + enemy['mp'] + '\n'
	msg += 'Weaknesses: '

	if 'weak' in enemy:
		for weakness in enemy['weak']:
			msg += str(weakness) + ' '
	else:
		msg += 'None'

	msg += '\n'
	msg += 'Resistances: '

	if 'resist' in enemy:
		for resistance in enemy['resist']:
			msg += str(resistance) + ' '
	else:
		msg += 'None'

	msg += '\n'

	allStatus = [e.value for e in StatusEffect]
	statusfx = []
	for attack in enemy['attacks']:
		if 'effect' in attack and attack['effect'] in allStatus:
			statusfx += attack['effect']

	if len(statusfx) > 0:
		msg += 'Watch out for: '
		for fx in statusfx:
			msg += fx + ' ' 
	msg += '```'
			

def genearateDebugAdventuerer(level):
	card = copy.deepcopy(adventurerCardTemplate)

	card['name'] = 'Biscuit'
	card['lvl'] = level
	card['hp'] += 3 * level
	card['mp'] += 2 * level
	card['maxHp'] += 3 * level
	card['maxMp'] += 2 * level

	card['wand'] = allWands[0]
	card['cloak'] = allCloaks[0]
	card['spells'].append(allSpells[0])

	card['spells'].append(allSpells[1])
	card['spells'].append(allSpells[4])

	return card