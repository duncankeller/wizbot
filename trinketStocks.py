import json
import random
import datetime
import os
import shutil
import copy
from enum import Enum
import matplotlib.pyplot as plt

from common import *

trinketMarketState = {}

trinketMarketStateHistoric = []

trinketMarketCurrentTotals = []


trinketMarketDataFolder = 'trinketMarketData'
trinketMarketValPath = 'trinketsOverDays0_'
trinketMarketAvgPath = 'trinketsAvg0'
trinketMarketVolPath = 'trinketsVol0'
trinketMarketCurrentDataPath = 'trinketsCurrData0'
trinketMarketHistoricDataPath = 'trinketsHistData0'

marketDataFullPath = trinketMarketDataFolder + '/' + trinketMarketCurrentDataPath + '.json'
marketDataTmpFullPath = trinketMarketDataFolder + '/' + trinketMarketCurrentDataPath + '_tmp.json'
marketHistDataFullPath = trinketMarketDataFolder + '/' + trinketMarketHistoricDataPath + '.json'
marketHistDataTmpFullPath = trinketMarketDataFolder + '/' + trinketMarketHistoricDataPath + '_tmp.json'


marketDataOldFullPath = trinketMarketDataFolder + '/' + 'trinketsCurrData' + '.json'
marketHistDataOldFullPath = trinketMarketDataFolder + '/' + 'trinketsHistData' + '.json'

def get15DayImgPath():
	return trinketMarketDataFolder + '/' + trinketMarketValPath + '15' + '.png'
def get50DayImgPath():
	return trinketMarketDataFolder + '/' + trinketMarketValPath + '50' + '.png'
def get30DayImgPath():
	return trinketMarketDataFolder + '/' + trinketMarketValPath + '30' + '.png'
def getAvgImgPath():
	return trinketMarketDataFolder + '/' + trinketMarketAvgPath + '.png'
def getVolImgPath():
	return trinketMarketDataFolder + '/' + trinketMarketVolPath + '.png'

class TrinketMarketState(str, Enum):
	Steady = 'Steady',
	Appreciating = 'Appreciating',
	Depreciating = 'Depreciating',
	Surging = 'Surging',
	Crashing = 'Crashing',
	Volitile = 'Volitile'


trinketStateToStringMap = {}
trinketStateToStringMap[TrinketMarketState.Steady] = 'Steady'
trinketStateToStringMap[TrinketMarketState.Appreciating] = 'Appreciating'
trinketStateToStringMap[TrinketMarketState.Depreciating] = 'Depreciating'
trinketStateToStringMap[TrinketMarketState.Surging] = 'Surging'
trinketStateToStringMap[TrinketMarketState.Crashing] = 'Crashing'
trinketStateToStringMap[TrinketMarketState.Volitile] = 'Volitile'

def trinketStateToString(state):
	return trinketStateToStringMap[state]

trinketMarketDaysInState = 'trinketMarketDaysInState'
trinketMarketCurrentState = 'trinketMarketCurrentState'
trinketMarketCurrentValueMod = 'trinketMarketCurrentValueMod' # stores the amount the trinket is surging by, depreciating by, etc
trinketMarketValue = 'trinketMarketValue'

maxDaysInTrinketState = {}
maxDaysInTrinketState[TrinketMarketState.Steady] = 6.0
maxDaysInTrinketState[TrinketMarketState.Appreciating] = 5.0
maxDaysInTrinketState[TrinketMarketState.Depreciating] = 5.0
maxDaysInTrinketState[TrinketMarketState.Surging] = 4.0
maxDaysInTrinketState[TrinketMarketState.Crashing] = 6.0
maxDaysInTrinketState[TrinketMarketState.Volitile] = 6.0

trinketCommon = 2
trinketRare = 1
trinketUltraRare = 0

trinketBasePriceRanges = {}
trinketBasePriceRanges[trinketCommon] =    (0.01, 7)
trinketBasePriceRanges[trinketRare] =      (1, 9)
trinketBasePriceRanges[trinketUltraRare] = (5, 10)

minMaxAppreciationAmnt = (0.05, 10)
minMaxAppreciationChance = {}
minMaxAppreciationChance[trinketCommon] =    0.2
minMaxAppreciationChance[trinketRare] =      0.4
minMaxAppreciationChance[trinketUltraRare] = 0.8

minMaxDepreciationAmnt = (0.05, 40)
minMaxDepreciationChance = {}
minMaxDepreciationChance[trinketCommon] =    0.55
minMaxDepreciationChance[trinketRare] =      0.65
minMaxDepreciationChance[trinketUltraRare] = 0.8

minMaxSurgeAmnt  =  (1.1, 1.8)
minMaxSurgeChance = {}
minMaxSurgeChance[trinketCommon] =    0.1
minMaxSurgeChance[trinketRare] =      0.2
minMaxSurgeChance[trinketUltraRare] = 0.6

minMaxCrashAmnt  =  (0.3, 0.86)
minMaxCrashChance = {}
minMaxCrashChance[trinketCommon] =    0.8
minMaxCrashChance[trinketRare] =      0.8
minMaxCrashChance[trinketUltraRare] = 0.8

minMaxVolitilityAmnt = {}
minMaxVolitilityAmnt[trinketCommon] =    (1, 40)
minMaxVolitilityAmnt[trinketRare] =      (3, 60)
minMaxVolitilityAmnt[trinketUltraRare] = (5, 75)

startingTrinketMarketValues = {}
startingTrinketMarketValues[trinketCommon] =    5
startingTrinketMarketValues[trinketRare] =      20
startingTrinketMarketValues[trinketUltraRare] = 50

trinketMarketCap = 1000

def getWeightedPercentage(weight):
	val = random.random()
	weight += 0.000001
	gravity = 0.0
	if val < weight:
		gravity = ((-1*(val - weight)**2) / (weight**2)) + 1
		result = weight * gravity
	else:
		val = 1 - val
		weight = 1 - weight
		gravity = (((-1*(val - weight)**2) / (weight**2)) + 1)
		result = weight * gravity
		result = 1 - result

	if result < 0:
		result = 0
	elif result > 1:
		result = 1

	return result

def interpRange(weight, range):
	randVal = getWeightedPercentage(weight)
	val = (range[1] - range[0]) / range[1]
	val *= randVal
	val *= range[1]
	val += range[0]
	return val

def initMarketMap():
	global trinketMarketState
	global trinketMarketStateHistoric

	try:
		with open(marketDataFullPath) as file:
			print('correctly opened and read previous market state')
			trinketMarketState = json.load(file)
		with open(marketHistDataFullPath) as file:
			print('correctly opened and read previous market state')
			trinketMarketStateHistoric = json.load(file)
	except:
		print('fatal error: failed to read trinket market maps')

	print('test: ' + str(trinketMarketState['Wooden Man']))

	for trinketName in trinketMap:
		if trinketName not in trinketMarketState:
			trinketMarketState[trinketName] = {}
			trinketMarketState[trinketName][trinketMarketValue] = startingTrinketMarketValues[trinketMap[trinketName]['rarity']]
			trinketMarketState[trinketName][trinketMarketCurrentState] = TrinketMarketState.Steady
			trinketMarketState[trinketName][trinketMarketDaysInState] = 0
			trinketMarketState[trinketName][trinketMarketCurrentValueMod] = 0


def advanceTrinketMarket():
	global trinketMarketState

	trinketMarketStateHistoric.append(copy.deepcopy(trinketMarketState))

	numOver100 = 0
	for trinketName in trinketMarketState:
		currentValue = trinketMarketState[trinketName][trinketMarketValue]
		if currentValue > 100:
			numOver100 += 1

	for trinketName in trinketMarketState:
		# see if we should shift into a different state
		currentValue = trinketMarketState[trinketName][trinketMarketValue]
		prevState = trinketMarketState[trinketName][trinketMarketCurrentState]
		days = trinketMarketState[trinketName][trinketMarketDaysInState]
		maxDays = maxDaysInTrinketState[prevState]
		rarity =  trinketMap[trinketName]['rarity']

		kidsModA = 1 if 'kids' not in trinketName.lower() else 10000
		kidsModB = 0 if 'kids' not in trinketName.lower() else 4

		weight = days / maxDays
		shouldChangeState = random.random() < weight
		if currentValue > trinketMarketCap or (prevState == TrinketMarketState.Surging and currentValue * trinketMarketState[trinketName][trinketMarketCurrentValueMod] > trinketMarketCap):
			shouldChangeState = True

		if shouldChangeState:
			# create array of potential states to shift into
			possibleStates = []
			if currentValue > trinketMarketCap:
				possibleStates.extend([TrinketMarketState.Crashing])
			elif prevState == TrinketMarketState.Steady:
				possibleStates.extend([TrinketMarketState.Appreciating] * (10))
				possibleStates.extend([TrinketMarketState.Depreciating] * (10 + numOver100 + kidsModA))
				possibleStates.extend([TrinketMarketState.Crashing] * (6 + numOver100 + kidsModA * 10))
				possibleStates.extend([TrinketMarketState.Surging] * 6)
				possibleStates.extend([TrinketMarketState.Volitile] * (4))

				if rarity == trinketRare and currentValue < 5:
					possibleStates.extend([TrinketMarketState.Appreciating] * 10)
				elif rarity == trinketUltraRare and currentValue < 5:
					possibleStates.extend([TrinketMarketState.Appreciating] * 20)
			elif prevState == TrinketMarketState.Appreciating:
				possibleStates.extend([TrinketMarketState.Steady] * 6)
				possibleStates.extend([TrinketMarketState.Depreciating] * (10 + numOver100 + kidsModA))
				possibleStates.extend([TrinketMarketState.Crashing] * (6 + numOver100 + kidsModA * 10))
				possibleStates.extend([TrinketMarketState.Surging] * 3)
				possibleStates.extend([TrinketMarketState.Volitile] * 3)
			elif prevState == TrinketMarketState.Depreciating:
				possibleStates.extend([TrinketMarketState.Steady] * (6 + kidsModB))
				possibleStates.extend([TrinketMarketState.Appreciating] * 10)
				possibleStates.extend([TrinketMarketState.Crashing] * (3 + kidsModB * 5))
				possibleStates.extend([TrinketMarketState.Surging] * 5)
				possibleStates.extend([TrinketMarketState.Volitile] * (3))

				if rarity == trinketRare and currentValue < 5:
					possibleStates.extend([TrinketMarketState.Appreciating] * 10)
				elif rarity == trinketUltraRare and currentValue < 5:
					possibleStates.extend([TrinketMarketState.Appreciating] * 20)
			elif prevState == TrinketMarketState.Surging:
				possibleStates.extend([TrinketMarketState.Depreciating] * 3)
				possibleStates.extend([TrinketMarketState.Crashing] * 10)
			elif prevState == TrinketMarketState.Crashing:
				possibleStates.extend([TrinketMarketState.Steady] * (10 + kidsModB))
				possibleStates.extend([TrinketMarketState.Appreciating] * (14))
				possibleStates.extend([TrinketMarketState.Depreciating] * (7 + numOver100 + (kidsModB * 4) ))
				possibleStates.extend([TrinketMarketState.Surging] * 5)
				possibleStates.extend([TrinketMarketState.Volitile] * (5))

				if rarity == trinketRare and currentValue < 5:
					possibleStates.extend([TrinketMarketState.Appreciating] * 10)
				elif rarity == trinketUltraRare and currentValue < 5:
					possibleStates.extend([TrinketMarketState.Appreciating] * 20)
			elif prevState == TrinketMarketState.Volitile:
				possibleStates.extend([TrinketMarketState.Steady] * 10)
				possibleStates.extend([TrinketMarketState.Appreciating] * 10)
				possibleStates.extend([TrinketMarketState.Depreciating] * (10 + numOver100 + kidsModA))
			else:
				print("ERROR: in unexpected trinket market state \"" + str(TrinketMarketState.Volitile) + "\"")
				possibleStates.extend([TrinketMarketState.Steady])

			# pick state out of the possible ones generated
			randIdx = random.randrange(0, len(possibleStates))
			newState = possibleStates[randIdx]
			trinketMarketState[trinketName][trinketMarketCurrentState] = newState

			# set params on new state
			trinketMarketState[trinketName][trinketMarketDaysInState] = 0

			if newState == TrinketMarketState.Appreciating:
				trinketMarketState[trinketName][trinketMarketCurrentValueMod] = interpRange(minMaxAppreciationChance[rarity], minMaxAppreciationAmnt)
			elif newState == TrinketMarketState.Depreciating:
				trinketMarketState[trinketName][trinketMarketCurrentValueMod] = interpRange(minMaxDepreciationChance[rarity], minMaxDepreciationAmnt)
			elif newState == TrinketMarketState.Surging:
				trinketMarketState[trinketName][trinketMarketCurrentValueMod] = interpRange(minMaxSurgeChance[rarity], minMaxSurgeAmnt)
			elif newState == TrinketMarketState.Crashing:
				trinketMarketState[trinketName][trinketMarketCurrentValueMod] = interpRange(minMaxCrashChance[rarity], minMaxCrashAmnt)
			elif newState == TrinketMarketState.Volitile:
				possiblities = list(range(minMaxVolitilityAmnt[rarity][0], minMaxVolitilityAmnt[rarity][1]))
				possiblities.extend(list(range(-minMaxVolitilityAmnt[rarity][0], -minMaxVolitilityAmnt[rarity][1])))
				trinketMarketState[trinketName][trinketMarketCurrentValueMod] = possiblities[random.randrange(0, len(possiblities))]
		else:
			# not changing state
			trinketMarketState[trinketName][trinketMarketDaysInState] += 1


		# now do the value calcualtion based on the state
		newState = trinketMarketState[trinketName][trinketMarketCurrentState]
		valMod = trinketMarketState[trinketName][trinketMarketCurrentValueMod]
		if newState == TrinketMarketState.Appreciating:
			trinketMarketState[trinketName][trinketMarketValue] += valMod
		elif newState == TrinketMarketState.Depreciating:
			trinketMarketState[trinketName][trinketMarketValue] -= valMod
		elif newState == TrinketMarketState.Surging:
			trinketMarketState[trinketName][trinketMarketValue] *= valMod
		elif newState == TrinketMarketState.Crashing:
			trinketMarketState[trinketName][trinketMarketValue] *= valMod
		elif newState == TrinketMarketState.Volitile:
			if random.randrange(0, 100) > 50:
				trinketMarketState[trinketName][trinketMarketValue] += random.randrange(minMaxVolitilityAmnt[rarity][0], minMaxVolitilityAmnt[rarity][1])
			else:
				trinketMarketState[trinketName][trinketMarketValue] -= random.randrange(minMaxVolitilityAmnt[rarity][0], minMaxVolitilityAmnt[rarity][1])

		if trinketMarketState[trinketName][trinketMarketValue] < 0:
			trinketMarketState[trinketName][trinketMarketValue] = 0

def printDebugMarketData():
	for trinketName in trinketMarketState:
		print(trinketName + ': Value: ' + str(trinketMarketState[trinketName][trinketMarketValue]) + ', State: ' +  trinketStateToString(trinketMarketState[trinketName][trinketMarketCurrentState]) + ', Days: ' + str(trinketMarketState[trinketName][trinketMarketDaysInState]) + ' Mod: ' + str(trinketMarketState[trinketName][trinketMarketCurrentValueMod]))

def printDebugMarketData(name):
	print(name + ': Value: ' + str(trinketMarketState[name][trinketMarketValue]) + ', State: ' +  trinketStateToString(trinketMarketState[name][trinketMarketCurrentState]) + ', Days: ' + str(trinketMarketState[name][trinketMarketDaysInState]) + ' Mod: ' + str(trinketMarketState[name][trinketMarketCurrentValueMod]))



def plotData(days):
	global trinketMarketState
	plt.clf()

	finalPicks = []
	topPicks = []
	for trinketName in trinketMarketState:
		topPicks.append((trinketName, trinketMarketState[trinketName][trinketMarketValue]))
	topPicks = sorted(topPicks, key=lambda x: x[1], reverse=True)
	for item in topPicks:
		finalPicks.append(item[0])
	
	finalPicks = finalPicks[:12]

	copiedState = copy.deepcopy(trinketMarketState)
	copiedState = sorted(copiedState.items(), key=lambda x: x[1][trinketMarketValue], reverse=True)

	for trinketName in finalPicks:
		pointsX = []
		pointsY = []
		for i in range(days):
			hist = trinketMarketStateHistoric[(len(trinketMarketStateHistoric) - days) + i]
			pointsX.append(i)
			pointsY.append(hist[trinketName][trinketMarketValue])

		pointsX.append(days)
		pointsY.append(trinketMarketState[trinketName][trinketMarketValue])

		lbl = trinketName + ' (' + '{:.5}'.format(float(trinketMarketState[trinketName][trinketMarketValue])) + 'μ)'

		plt.plot(pointsX, pointsY, label = lbl)

	plt.xlabel('Days')
	plt.ylabel('Value')
	plt.title('Trinket Market Over ' + str(days) + ' Days')
	plt.legend(loc="center right", bbox_to_anchor=(1.49, 0.5), ncol= 1)
	#plt.show()
	plt.savefig(trinketMarketDataFolder+'/'+trinketMarketValPath+str(days)+'.png', bbox_inches='tight')



def plotDataAvg(days):
	global trinketMarketState
	plt.clf()

	for rarity in range(3):
		pointsY = []
		pointsX = []
		for i in range(days + 1):
			num = 0
			val = 0
			for trinketName in trinketMarketState:
				if trinketMap[trinketName]['rarity'] == rarity:
					if i == days:
						val += trinketMarketState[trinketName][trinketMarketValue]
					else:
						hist = trinketMarketStateHistoric[(len(trinketMarketStateHistoric) - days) + i]
						val += hist[trinketName][trinketMarketValue]
					num += 1

			if num > 0:
				pointsX.append(i)
				pointsY.append(val / num)

		lbl = rarityMap[rarity]
		plt.plot(pointsX, pointsY, label = lbl)

	plt.xlabel('Days')
	plt.ylabel('Value')
	plt.title('Average Value Over ' + str(days) + ' Days')
	plt.legend(loc="center right", bbox_to_anchor=(1.29, 0.5), ncol= 1)
	#plt.show()
	plt.savefig(trinketMarketDataFolder+'/'+trinketMarketAvgPath+'.png', bbox_inches='tight')


def plotVolatility(days):
	global trinketMarketState
	plt.clf()

	for rarity in range(3):
		pointsY = []
		pointsX = []
		for i in range(days + 1):
			num = 0
			val = 0
			for trinketName in trinketMarketState:
				curVal = 0
				prevVal = 0
				if trinketMap[trinketName]['rarity'] == rarity:
					if i == days:
						curVal = trinketMarketState[trinketName][trinketMarketValue]
						histB = trinketMarketStateHistoric[(len(trinketMarketStateHistoric) - (days + 1)) + i]
						prevVal = histB[trinketName][trinketMarketValue]
					else:
						histA = trinketMarketStateHistoric[(len(trinketMarketStateHistoric) - days) + i]
						histB = trinketMarketStateHistoric[(len(trinketMarketStateHistoric) - (days + 1)) + i]
						curVal = histA[trinketName][trinketMarketValue]
						prevVal = histB[trinketName][trinketMarketValue]
					val += abs(curVal - prevVal)
					num += 1

			if num > 0:
				pointsX.append(i)
				pointsY.append(val / num)

		lbl = rarityMap[rarity]
		plt.plot(pointsX, pointsY, label = lbl)

	plt.xlabel('Days')
	plt.ylabel('Value')
	plt.title('Volatility Over ' + str(days) + ' Days')
	plt.legend(loc="center right", bbox_to_anchor=(1.29, 0.5), ncol= 1)
	#plt.show()
	plt.savefig(trinketMarketDataFolder+'/'+trinketMarketVolPath+'.png', bbox_inches='tight')

def generateMarketInsights(num):
	insights = []

	numOver100 = 0
	for trinketName in trinketMarketState:
		currentValue = trinketMarketState[trinketName][trinketMarketValue]
		if currentValue > 100:
			numOver100 += 1

	for trinketName in trinketMarketState:
		value = trinketMarketState[trinketName][trinketMarketValue]
		state = trinketMarketState[trinketName][trinketMarketCurrentState]
		days = trinketMarketState[trinketName][trinketMarketDaysInState]
		maxDays = maxDaysInTrinketState[state]
		mod = trinketMarketState[trinketName][trinketMarketCurrentValueMod]
		rarity =  trinketMap[trinketName]['rarity']
		kids = True if 'kids' in trinketName.lower() else False

		if kids and value > 100 and maxDays - days < 5:
			insights.append( 'I\'m not sure how long ' + trinketName + ' will be able to hold its value. May be wise to sell soon.')
		if kids and value < 10 and state != TrinketMarketState.Surging and days < 3:
			insights.append('There\'s no demand for  ' + trinketName + '. Don\'t count too highly on a price surge.')
		if value > trinketMarketCap - 50:
			insights.append( trinketName + ' is seeing incredible success on the market, but for how long can it sustain this price?')
		if value > 20 and state == TrinketMarketState.Surging and maxDays - days < 2:
			insights.append( 'I predict that the price surge on ' + trinketName + ' will be ending soon')
		if value > 100 and numOver100 > 5 and state != TrinketMarketState.Crashing and state != TrinketMarketState.Depreciating and maxDays - days > 2:
			insights.append( 'The market can\'t sustain such a level of growth. Trinkets like ' + trinketName + ' may see a loss in value soon.')
		if rarity == trinketRare or rarity == trinketUltraRare:
			if value < 5 and maxDays - days > 3:
				insights.append( 'Prices for ' + trinketName + ' are destined to rebound soon.')
		if state == TrinketMarketState.Volitile and days == maxDays:
			insights.append( trinketName + ' has undergone a longer than usual period of volatility and is expected to stabalize in short order.')
		if state == TrinketMarketState.Steady and maxDays - days < 3 and value > 5:
			insights.append( trinketName + ' is a Trinket to look out for. It may be undergoing a dramatic change in value soon.')
		if state == TrinketMarketState.Steady and days == 0 and value < trinketMarketCap - 100 and value > 5:
			insights.append('I predict that ' + trinketName + ' will hold its value for a few days, absent of other market fluctuations.')
		if state == TrinketMarketState.Surging and days == 0 and value > 5 and mod > 1.5 and not kids and value < trinketMarketCap - 100:
			insights.append( trinketName + ' is beginning a price surge that may be very fruitful.')
		if state == TrinketMarketState.Crashing and days == 0 and value > 20 and mod < 0.57:
			insights.append( trinketName + ' is expected to tank hard. Please deinvest in this Trinket while you can, sirs!')
		if state == TrinketMarketState.Appreciating and days <= 2 and mod > 7 and value > 20:
			insights.append( trinketName + ' is undergoing a period of unusually high appreciation. Selling or waiting for a higher value are both valid approaches.')
		if state == TrinketMarketState.Depreciating and days <= 1 and mod > 7 and value > 30:
			insights.append( 'This is a bad time to be invested in ' + trinketName + ', its depreciation is unusually severe.')

	results = ''
	for i in range(num):
		result = insights[random.randrange(0, len(insights))]
		insights.remove(result)
		results += '\t- ' + result + '\n'
	return results

def generateCurrentMarketState():
	global trinketMarketState

	copiedState = copy.deepcopy(trinketMarketState)
	copiedState = sorted(copiedState.items(), key=lambda x: x[1][trinketMarketValue], reverse=True)
	msgs = []
	msg = ''

	cnt = 0

	for entry in copiedState:
		trinketName = entry[0]
		price = trinketMarketState[trinketName][trinketMarketValue]

		item = (trinketName, 
			price,
			trinketStateToString(trinketMarketState[trinketName][trinketMarketCurrentState]),
			'for ' + str(trinketMarketState[trinketName][trinketMarketDaysInState]) + ' days' if trinketMarketState[trinketName][trinketMarketDaysInState] > 0 else '')
		msg += '{0:<18} {1:<8.1f} {2:<13} {3:>3}'.format(*item) + '\n'

		cnt += 1
		if cnt > 25:
			cnt = 0
			msgs.append(msg)
			msg = ''

	if len(msg) > 0:
		msgs.append(msg)

	return msgs


def getCurrentMarketState():
	#global trinketMarketCurrentTotals

	#if len(trinketMarketCurrentTotals) == 0:
	#	trinketMarketCurrentTotals = generateCurrentMarketState()
	return generateCurrentMarketState()

def getMarketDigest():
	topPicks = []
	for trinketName in trinketMarketState:
		topPicks.append((trinketName, trinketMarketState[trinketName][trinketMarketValue]))
	topPicks = sorted(topPicks, key=lambda x: x[1], reverse=True)

	insights = generateMarketInsights(3)

	msg = 'Here\'s what\'s happening in the Trinket Market today.\n\n'
	msg += 'The highest valued Trinkets right now are: ' + topPicks[0][0] + ', ' + topPicks[1][0] + ' and ' + topPicks[2][0] + '.\n\n'  
	msg += 'Market Insights:\n```'  
	msg += insights + '\n```'
	return msg

def regenImages():
	plotData(15)
	plotData(50)
	plotDataAvg(50)
	plotVolatility(15)

def updateMarketForTheDay():
	if not os.path.exists(trinketMarketDataFolder):
		os.makedirs(trinketMarketDataFolder)

	advanceTrinketMarket()
	plotData(15)
	plotData(50)
	plotDataAvg(50)
	plotVolatility(15)

	trinketMarketCurrentTotals = generateCurrentMarketState()

	try:
		with open(marketDataTmpFullPath, 'w') as file:
			json.dump(trinketMarketState, file)
		shutil.move(marketDataTmpFullPath, marketDataFullPath)
	except Exception as e:
		print('failed to save market data!')
		print(e)

	try:
		with open(marketHistDataTmpFullPath, 'w') as file:
			json.dump(trinketMarketStateHistoric, file)
		shutil.move(marketHistDataTmpFullPath, marketHistDataFullPath)
	except Exception as e:
		print('failed to save market data!')
		print(e)


#for x in range(100):
#	val = random.random()
#	result = getWeightedPercentage(val)
#	print("weight: " + str('{:.3}'.format(val)) + ": " + str(('{:.3}'.format(result))))

#for x in range(100):
#	val = interpRange(minMaxAppreciationChance[2], minMaxAppreciationAmnt)
#	print("weight: " + str('{:.3}'.format(val)))

initMarketMap()


for x in range(200):
	#printDebugMarketData('Kids Jacky 1')
	#printDebugMarketData('Metal Sonic')
	advanceTrinketMarket()


#plotData(15)
#plotData(30)
#plotDataAvg(50)
#plotVolatility(15)
#print(generateCurrentMarketState())

#updateMarketForTheDay()


#print(generateMarketInsights(8))