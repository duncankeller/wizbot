from itemCommon import * 

allDungeonItems = [
#------------------------------------------------------------- 0 ---------------------------------------------------------------
{
	'name' : 'Lesser Health Potion',
	'price' : 1000,
	'consumable' : True,
	'level' : 0,
	'descr' : 'Heals 10 hp.',
	'heal-hp' : 10
},
{
	'name' : 'Lesser Mana Potion',
	'price' : 1100,
	'consumable' : True,
	'level' : 0,
	'descr' : 'Heals 12 mp.',
	'heal-mp' : 12
},
{
	'name' : 'Lesser Revive Stone',
	'price' : 9200,
	'consumable' : True,
	'level' : 0,
	'descr' : 'Revives you to 10\% HP when you are felled. Breaks upon use.',
	'revive' : 10
},
#------------------------------------------------------------- 1 ---------------------------------------------------------------
{
	'name' : 'Lesser Antitoxin Circlet',
	'price' : 10300,
	'consumable' : False,
	'level' : 1,
	'descr' : 'Resists Poison. (-50\% chance of inflicting)',
	'resist' : StatusEffect.Poison
},
{
	'name' : 'Lesser Soothing Circlet',
	'price' : 10400,
	'consumable' : False,
	'level' : 1,
	'descr' : 'Resists Burn. (-50\% chance of inflicting)',
	'resist' : StatusEffect.Burn
},
#------------------------------------------------------------- 2 ---------------------------------------------------------------
{
	'name' : 'Lesser Warming Circlet',
	'price' : 10400,
	'consumable' : False,
	'level' : 2,
	'descr' : 'Resists Freeze. (-50\% chance of inflicting)',
	'resist' : StatusEffect.Freeze
},
{
	'name' : 'Lesser Alertness Circlet',
	'price' : 11200,
	'consumable' : False,
	'level' : 2,
	'descr' : 'Resists Sleep. (-50\% chance of inflicting)',
	'resist' : StatusEffect.Sleep
},
#------------------------------------------------------------- 3 ---------------------------------------------------------------
{
	'name' : 'Lesser Protection Charm',
	'price' : 15400,
	'consumable' : False,
	'level' : 3,
	'descr' : 'Reduces Damage by 1.',
	'dmg-reduction' : 1
},
{
	'name' : 'Lesser Mana Charm',
	'price' : 13400,
	'consumable' : False,
	'level' : 3,
	'descr' : 'Reduces cost of spells by 3.',
	'mp-reduction' : 3
},
#------------------------------------------------------------- 4 ---------------------------------------------------------------
{
	'name' : 'Lesser Limber Circlet',
	'price' : 10500,
	'consumable' : False,
	'level' : 4,
	'descr' : 'Resists Paralisis. (-50\% chance of inflicting)',
	'resist' : StatusEffect.Paralisis
},
{
	'name' : 'Lesser Clarity Circlet',
	'price' : 11700,
	'consumable' : False,
	'level' : 4,
	'descr' : 'Resists Confusion. (-50\% chance of inflicting)',
	'resist' : StatusEffect.Confusion
},
{
	'name' : 'Lesser Communication Circlet',
	'price' : 11700,
	'consumable' : False,
	'level' : 4,
	'descr' : 'Resists Silence. (-50\% chance of inflicting)',
	'resist' : StatusEffect.Silence
},
#------------------------------------------------------------- 5 ---------------------------------------------------------------
{
	'name' : 'Health Potion',
	'price' : 21000,
	'consumable' : True,
	'level' : 5,
	'descr' : 'Heals 40 hp.',
	'heal-hp' : 40
},
{
	'name' : 'Mana Potion',
	'price' : 21600,
	'consumable' : True,
	'level' : 5,
	'descr' : 'Heals 50 mp.',
	'heal-mp' : 50
},
{
	'name' : 'Revive Stone',
	'price' : 39200,
	'consumable' : True,
	'level' : 5,
	'descr' : 'Revives you to 50\% HP when you are felled. Breaks upon use.',
	'revive' : 50
},
#------------------------------------------------------------- 6 ---------------------------------------------------------------
{
	'name' : 'Antitoxin Circlet',
	'price' : 53500,
	'consumable' : False,
	'level' : 6,
	'descr' : 'Negates Poison.',
	'negate' : StatusEffect.Poison
},
{
	'name' : 'Soothing Circlet',
	'price' : 54200,
	'consumable' : False,
	'level' : 6,
	'descr' : 'Negates Burn.',
	'negate' : StatusEffect.Burn
},
#------------------------------------------------------------- 7 ---------------------------------------------------------------
{
	'name' : 'Warming Circlet',
	'price' : 54400,
	'consumable' : False,
	'level' : 7,
	'descr' : 'Negates Freeze.',
	'negate' : StatusEffect.Freeze
},
{
	'name' : 'Alertness Circlet',
	'price' : 57200,
	'consumable' : False,
	'level' : 7,
	'descr' : 'Negates Sleep.',
	'negate' : StatusEffect.Sleep
},
#------------------------------------------------------------- 8 ---------------------------------------------------------------
{
	'name' : 'Lesser Limber Circlet',
	'price' : 59600,
	'consumable' : False,
	'level' : 8,
	'descr' : 'Negates Paralisis.',
	'negate' : StatusEffect.Paralisis
},
{
	'name' : 'Lesser Clarity Circlet',
	'price' : 62100,
	'consumable' : False,
	'level' : 8,
	'descr' : 'Negates Confusion.',
	'negate' : StatusEffect.Confusion
},
#------------------------------------------------------------- 9 ---------------------------------------------------------------
{
	'name' : 'Lesser Communication Circlet',
	'price' : 74700,
	'consumable' : False,
	'level' : 9,
	'descr' : 'Negates Silence.',
	'negate' : StatusEffect.Silence
},
#------------------------------------------------------------- 10 ---------------------------------------------------------------
{
	'name' : 'Ultra Revive Stone',
	'price' : 128900,
	'consumable' : True,
	'level' : 10,
	'descr' : 'Revives you to 100\% HP when you are felled. Breaks upon use.',
	'revive' : 100
}

]


def getItemByName(name):
	for item in allDungeonItems:
		if item['name'] == name:
			return item
	return {}